<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\UserMenu;
use App\Models\UserRole;
use App\Models\UserSubMenu;
use App\Models\UserAccessMenu;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $userRole = ['Administrator', 'User'];
        $userMenu = ['Main Menu', 'Management', 'Keuangan', 'Laporan', 'CMS', 'Profile', 'Setting'];
        $userSubMenu = [
            [
                'menu_id' => 1,
                'name' => 'Home',
                'url' => 'home',
                'icon' => 'fas fa-home'
            ],
            [
                'menu_id' => 2,
                'name' => 'Pegawai',
                'url' => 'employees',
                'icon' => 'bi bi-person-check'
            ],
            [
                'menu_id' => 2,
                'name' => 'Client',
                'url' => 'clients',
                'icon' => 'bi bi-people'
            ],
            [
                'menu_id' => 2,
                'name' => 'Aplikasi',
                'url' => 'products',
                'icon' => 'bi bi-laptop'
            ],
            [
                'menu_id' => 3,
                'name' => 'COA',
                'url' => 'finance-coas',
                'icon' => 'bi bi-bank'
            ],
            [
                'menu_id' => 3,
                'name' => 'Saldo Awal',
                'url' => 'balance-beginnings',
                'icon' => 'bi bi-piggy-bank'
            ],
            [
                'menu_id' => 3,
                'name' => 'Anggaran',
                'url' => 'finance-budgets',
                'icon' => 'bi bi-wallet2'
            ],
            [
                'menu_id' => 3,
                'name' => 'Transaksi',
                'url' => 'finance-transactions',
                'icon' => 'bi bi-currency-dollar'
            ],
            [
                'menu_id' => 3,
                'name' => 'Gaji',
                'url' => 'finance-salaries',
                'icon' => 'bi bi-cash-coin'
            ],
            [
                'menu_id' => 4,
                'name' => 'Neraca',
                'url' => 'report-balances',
                'icon' => 'bi bi-file-earmark-pdf'
            ],
            [
                'menu_id' => 4,
                'name' => 'Buku Besar',
                'url' => 'report-ledgers',
                'icon' => 'bi bi-file-earmark-pdf'
            ],
            [
                'menu_id' => 5,
                'name' => 'Content',
                'url' => 'cms-contents',
                'icon' => 'far fa-image'
            ],
            [
                'menu_id' => 6,
                'name' => 'Manage Profile',
                'url' => 'profiles',
                'icon' => 'far fa-user-circle'
            ],
            [
                'menu_id' => 6,
                'name' => 'Change Password',
                'url' => 'profiles/change-password',
                'icon' => 'bi bi-key-fill'
            ],
            [
                'menu_id' => 7,
                'name' => 'Menu Management',
                'url' => 'menus',
                'icon' => 'bi bi-menu-button-wide'
            ],
            [
                'menu_id' => 7,
                'name' => 'Sub Menu Management',
                'url' => 'submenus',
                'icon' => 'bi bi-menu-button'
            ],
            [
                'menu_id' => 7,
                'name' => 'Role Management',
                'url' => 'roles',
                'icon' => 'bi bi-person-bounding-box'
            ],
            [
                'menu_id' => 7,
                'name' => 'User Management',
                'url' => 'users',
                'icon' => 'bi bi-person-check-fill'
            ],
        ];
        $userAccessMenu = [
            [
                'role_id' => 1,
                'menu_id' => 1,
            ],
            [
                'role_id' => 1,
                'menu_id' => 2,
            ],
            [
                'role_id' => 1,
                'menu_id' => 3,
            ],
            [
                'role_id' => 1,
                'menu_id' => 4,
            ],
            [
                'role_id' => 1,
                'menu_id' => 5,
            ],
            [
                'role_id' => 1,
                'menu_id' => 6,
            ],
            [
                'role_id' => 1,
                'menu_id' => 7,
            ],
            [
                'role_id' => 2,
                'menu_id' => 1,
            ],
            [
                'role_id' => 2,
                'menu_id' => 6,
            ],
        ];

        User::factory()->create();
        // UserRole::factory()->create();

        // user role
        foreach ($userRole as $key => $role) {
            UserRole::create([
                'name' => $role
            ]);
        }

        // user menu
        $order = 0;
        foreach ($userMenu as $key => $menu) {
            $order++;
            $um = UserMenu::create([
                'name' => $menu,
                'order' => $order
            ]);

            // user sub menu
            $orderSub = 0;
            foreach ($userSubMenu as $key => $subMenu) {
                if ($um->id == $subMenu['menu_id']) {
                    $orderSub++;
                    UserSubMenu::create([
                        'menu_id' => $subMenu['menu_id'],
                        'name' => $subMenu['name'],
                        'url' => $subMenu['url'],
                        'icon' => $subMenu['icon'],
                        'is_active' => 1,
                        'order' => $orderSub
                    ]);
                }
            }
        }

        // // user sub menu
        // $order = 0;
        // foreach ($userSubMenu as $key => $subMenu){
        //     $order ++;
        //     UserSubMenu::create([
        //         'menu_id' => $subMenu['menu_id'],
        //         'name' => $subMenu['name'],
        //         'url' => $subMenu['url'],
        //         'is_active' => 1,
        //         'order' => $order
        //     ]);
        // }

        // user access menu
        foreach ($userAccessMenu as $key => $access) {
            UserAccessMenu::create([
                'role_id' => $access['role_id'],
                'menu_id' => $access['menu_id'],
            ]);
        }
    }
}
