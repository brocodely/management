<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBalanceRemainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balance_remainings', function (Blueprint $table) {
            $table->id();
            $table->string('nomor_perkiraan', '50');
            $table->string('nama_akun', '100');
            $table->decimal('saldo_awal', '15', '2');
            $table->decimal('saldo_akhir', '15', '2');
            $table->date('tanggal');
            $table->string('type', '50');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balance_remainings');
    }
}
