<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinanceCoasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finance_coas', function (Blueprint $table) {
            $table->id();
            $table->string('nomor_perkiraan', '50');
            $table->string('nama_akun', '100');
            $table->string('header', '50');
            $table->string('bagian', '50');
            $table->string('status', '1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finance_coas');
    }
}
