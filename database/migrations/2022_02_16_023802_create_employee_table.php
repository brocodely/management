<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->longText('image');
            $table->string('email', '255')->unique();
            $table->string('position', '100');
            $table->string('name', '100');
            $table->string('place_of_birth', '100');
            $table->date('date_of_birth');
            $table->string('phone', '20');
            $table->longText('address');
            $table->date('join_date');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
