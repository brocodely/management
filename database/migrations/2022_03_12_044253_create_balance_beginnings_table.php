<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBalanceBeginningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balance_beginnings', function (Blueprint $table) {
            $table->id();
            $table->string('nomor_perkiraan', '50');
            $table->string('nama_akun', '100');
            $table->decimal('debit', '15', '2');
            $table->decimal('kredit', '15', '2');
            $table->date('tanggal');
            $table->string('status', '1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balance_beginnings');
    }
}
