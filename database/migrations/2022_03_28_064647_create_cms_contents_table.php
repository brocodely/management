<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCmsContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_contents', function (Blueprint $table) {
            $table->id();
            $table->longText('logo');
            $table->longText('headline');
            $table->longText('tagline');
            $table->longText('about_title');
            $table->longText('about_content');
            $table->longText('service_image');
            $table->longText('service_title');
            $table->longText('service_content');
            $table->longText('element_image');
            $table->longText('element_title');
            $table->longText('element_content');
            $table->longText('excellence_title');
            $table->longText('excellence_content');
            $table->longText('cta_title');
            $table->longText('cta_content');
            $table->longText('portfolio_title');
            $table->longText('portfolio_content');
            $table->longText('team_title');
            $table->longText('team_content');
            $table->longText('faq_title');
            $table->longText('faq_content');
            $table->longText('contact_title');
            $table->longText('contact_content');
            $table->longText('socmed_title');
            $table->longText('socmed_content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_contents');
    }
}
