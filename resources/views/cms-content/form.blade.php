<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group mb-3">
            {{ Form::label('logo') }}
            {{ Form::text('logo', $cmsContent->logo, ['class' => 'form-control' . ($errors->has('logo') ? ' is-invalid' : ''), 'placeholder' => 'Logo']) }}
            {!! $errors->first('logo', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('headline') }}
            {{ Form::text('headline', $cmsContent->headline, ['class' => 'form-control' . ($errors->has('headline') ? ' is-invalid' : ''), 'placeholder' => 'Headline']) }}
            {!! $errors->first('headline', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('tagline') }}
            {{ Form::text('tagline', $cmsContent->tagline, ['class' => 'form-control' . ($errors->has('tagline') ? ' is-invalid' : ''), 'placeholder' => 'Tagline']) }}
            {!! $errors->first('tagline', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('about_title') }}
            {{ Form::text('about_title', $cmsContent->about_title, ['class' => 'form-control' . ($errors->has('about_title') ? ' is-invalid' : ''), 'placeholder' => 'About Title']) }}
            {!! $errors->first('about_title', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('about_content') }}
            {{ Form::text('about_content', $cmsContent->about_content, ['class' => 'form-control' . ($errors->has('about_content') ? ' is-invalid' : ''), 'placeholder' => 'About Content']) }}
            {!! $errors->first('about_content', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('about_detail_code') }}
            {{ Form::text('about_detail_code', $cmsContent->about_detail_code, ['class' => 'form-control' . ($errors->has('about_detail_code') ? ' is-invalid' : ''), 'placeholder' => 'About Detail Code']) }}
            {!! $errors->first('about_detail_code', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('service_title') }}
            {{ Form::text('service_title', $cmsContent->service_title, ['class' => 'form-control' . ($errors->has('service_title') ? ' is-invalid' : ''), 'placeholder' => 'Service Title']) }}
            {!! $errors->first('service_title', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('service_content') }}
            {{ Form::text('service_content', $cmsContent->service_content, ['class' => 'form-control' . ($errors->has('service_content') ? ' is-invalid' : ''), 'placeholder' => 'Service Content']) }}
            {!! $errors->first('service_content', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('service_detail_code') }}
            {{ Form::text('service_detail_code', $cmsContent->service_detail_code, ['class' => 'form-control' . ($errors->has('service_detail_code') ? ' is-invalid' : ''), 'placeholder' => 'Service Detail Code']) }}
            {!! $errors->first('service_detail_code', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('element_title') }}
            {{ Form::text('element_title', $cmsContent->element_title, ['class' => 'form-control' . ($errors->has('element_title') ? ' is-invalid' : ''), 'placeholder' => 'Element Title']) }}
            {!! $errors->first('element_title', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('element_content') }}
            {{ Form::text('element_content', $cmsContent->element_content, ['class' => 'form-control' . ($errors->has('element_content') ? ' is-invalid' : ''), 'placeholder' => 'Element Content']) }}
            {!! $errors->first('element_content', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('element_detail_code') }}
            {{ Form::text('element_detail_code', $cmsContent->element_detail_code, ['class' => 'form-control' . ($errors->has('element_detail_code') ? ' is-invalid' : ''), 'placeholder' => 'Element Detail Code']) }}
            {!! $errors->first('element_detail_code', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('excellence_title') }}
            {{ Form::text('excellence_title', $cmsContent->excellence_title, ['class' => 'form-control' . ($errors->has('excellence_title') ? ' is-invalid' : ''), 'placeholder' => 'Excellence Title']) }}
            {!! $errors->first('excellence_title', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('excellence_content') }}
            {{ Form::text('excellence_content', $cmsContent->excellence_content, ['class' => 'form-control' . ($errors->has('excellence_content') ? ' is-invalid' : ''), 'placeholder' => 'Excellence Content']) }}
            {!! $errors->first('excellence_content', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('excellence_detail_code') }}
            {{ Form::text('excellence_detail_code', $cmsContent->excellence_detail_code, ['class' => 'form-control' . ($errors->has('excellence_detail_code') ? ' is-invalid' : ''), 'placeholder' => 'Excellence Detail Code']) }}
            {!! $errors->first('excellence_detail_code', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('cta_title') }}
            {{ Form::text('cta_title', $cmsContent->cta_title, ['class' => 'form-control' . ($errors->has('cta_title') ? ' is-invalid' : ''), 'placeholder' => 'Cta Title']) }}
            {!! $errors->first('cta_title', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('cta_content') }}
            {{ Form::text('cta_content', $cmsContent->cta_content, ['class' => 'form-control' . ($errors->has('cta_content') ? ' is-invalid' : ''), 'placeholder' => 'Cta Content']) }}
            {!! $errors->first('cta_content', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('portfolio_title') }}
            {{ Form::text('portfolio_title', $cmsContent->portfolio_title, ['class' => 'form-control' . ($errors->has('portfolio_title') ? ' is-invalid' : ''), 'placeholder' => 'Portfolio Title']) }}
            {!! $errors->first('portfolio_title', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('portfolio_content') }}
            {{ Form::text('portfolio_content', $cmsContent->portfolio_content, ['class' => 'form-control' . ($errors->has('portfolio_content') ? ' is-invalid' : ''), 'placeholder' => 'Portfolio Content']) }}
            {!! $errors->first('portfolio_content', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('portfolio_detail_code') }}
            {{ Form::text('portfolio_detail_code', $cmsContent->portfolio_detail_code, ['class' => 'form-control' . ($errors->has('portfolio_detail_code') ? ' is-invalid' : ''), 'placeholder' => 'Portfolio Detail Code']) }}
            {!! $errors->first('portfolio_detail_code', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('team_title') }}
            {{ Form::text('team_title', $cmsContent->team_title, ['class' => 'form-control' . ($errors->has('team_title') ? ' is-invalid' : ''), 'placeholder' => 'Team Title']) }}
            {!! $errors->first('team_title', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('team_content') }}
            {{ Form::text('team_content', $cmsContent->team_content, ['class' => 'form-control' . ($errors->has('team_content') ? ' is-invalid' : ''), 'placeholder' => 'Team Content']) }}
            {!! $errors->first('team_content', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('team_detail_code') }}
            {{ Form::text('team_detail_code', $cmsContent->team_detail_code, ['class' => 'form-control' . ($errors->has('team_detail_code') ? ' is-invalid' : ''), 'placeholder' => 'Team Detail Code']) }}
            {!! $errors->first('team_detail_code', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('faq_title') }}
            {{ Form::text('faq_title', $cmsContent->faq_title, ['class' => 'form-control' . ($errors->has('faq_title') ? ' is-invalid' : ''), 'placeholder' => 'Faq Title']) }}
            {!! $errors->first('faq_title', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('faq_content') }}
            {{ Form::text('faq_content', $cmsContent->faq_content, ['class' => 'form-control' . ($errors->has('faq_content') ? ' is-invalid' : ''), 'placeholder' => 'Faq Content']) }}
            {!! $errors->first('faq_content', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('faq_detail_code') }}
            {{ Form::text('faq_detail_code', $cmsContent->faq_detail_code, ['class' => 'form-control' . ($errors->has('faq_detail_code') ? ' is-invalid' : ''), 'placeholder' => 'Faq Detail Code']) }}
            {!! $errors->first('faq_detail_code', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('contact_title') }}
            {{ Form::text('contact_title', $cmsContent->contact_title, ['class' => 'form-control' . ($errors->has('contact_title') ? ' is-invalid' : ''), 'placeholder' => 'Contact Title']) }}
            {!! $errors->first('contact_title', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('contact_content') }}
            {{ Form::text('contact_content', $cmsContent->contact_content, ['class' => 'form-control' . ($errors->has('contact_content') ? ' is-invalid' : ''), 'placeholder' => 'Contact Content']) }}
            {!! $errors->first('contact_content', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('contact_detail_code') }}
            {{ Form::text('contact_detail_code', $cmsContent->contact_detail_code, ['class' => 'form-control' . ($errors->has('contact_detail_code') ? ' is-invalid' : ''), 'placeholder' => 'Contact Detail Code']) }}
            {!! $errors->first('contact_detail_code', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('socmed_title') }}
            {{ Form::text('socmed_title', $cmsContent->socmed_title, ['class' => 'form-control' . ($errors->has('socmed_title') ? ' is-invalid' : ''), 'placeholder' => 'Socmed Title']) }}
            {!! $errors->first('socmed_title', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('socmed_content') }}
            {{ Form::text('socmed_content', $cmsContent->socmed_content, ['class' => 'form-control' . ($errors->has('socmed_content') ? ' is-invalid' : ''), 'placeholder' => 'Socmed Content']) }}
            {!! $errors->first('socmed_content', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('socmed_detail_code') }}
            {{ Form::text('socmed_detail_code', $cmsContent->socmed_detail_code, ['class' => 'form-control' . ($errors->has('socmed_detail_code') ? ' is-invalid' : ''), 'placeholder' => 'Socmed Detail Code']) }}
            {!! $errors->first('socmed_detail_code', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>