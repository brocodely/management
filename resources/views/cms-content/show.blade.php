@extends('layouts.main')
@section('title', __('Cms Content'))
@section('content')
<!--begin::Main-->
	<div class="d-flex flex-column flex-column-fluid">
		<!--begin::toolbar-->
		<div class="toolbar" id="kt_toolbar">
			<div class="container d-flex flex-stack flex-wrap flex-sm-nowrap">
				<!--begin::Info-->
				<div class="d-flex flex-column align-items-start justify-content-center flex-wrap me-1">
					
					<!--begin::Breadcrumb-->
					<ul class="breadcrumb breadcrumb-line bg-transparent text-muted fw-bold p-0 my-1 fs-7">
						<li class="breadcrumb-item">
							<a href="{{ route('cms-contents.index') }}" class="text-muted text-hover-primary">{{ __('Cms Content') }}</a>
						</li>
						<li class="breadcrumb-item text-dark">{{ __('Cms Content') }}</li>							
					</ul>
					<!--end::Breadcrumb-->
				</div>
				<!--end::Info-->
				
				
			</div>
		</div>
		<!--end::toolbar-->
		<!--begin::Content-->
		<div class="content fs-6 d-flex flex-column-fluid mt-5" id="kt_content">
			<!--begin::Container-->
			<div class="container">
				<!--begin::Profile Account-->
                <div class="card">
                    <div class="card-header">
                        <div class="float-left mt-4">
                            <span class="card-title">Show Cms Content</span>
                        </div>
                        <div class="float-right mt-4">
                            <a class="btn btn-primary" href="{{ route('cms-contents.index') }}"> Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Logo:</strong>
                            {{ $cmsContent->logo }}
                        </div>
                        <div class="form-group">
                            <strong>Headline:</strong>
                            {{ $cmsContent->headline }}
                        </div>
                        <div class="form-group">
                            <strong>Tagline:</strong>
                            {{ $cmsContent->tagline }}
                        </div>
                        <div class="form-group">
                            <strong>About Title:</strong>
                            {{ $cmsContent->about_title }}
                        </div>
                        <div class="form-group">
                            <strong>About Content:</strong>
                            {{ $cmsContent->about_content }}
                        </div>
                        <div class="form-group">
                            <strong>About Detail Code:</strong>
                            {{ $cmsContent->about_detail_code }}
                        </div>
                        <div class="form-group">
                            <strong>Service Title:</strong>
                            {{ $cmsContent->service_title }}
                        </div>
                        <div class="form-group">
                            <strong>Service Content:</strong>
                            {{ $cmsContent->service_content }}
                        </div>
                        <div class="form-group">
                            <strong>Service Detail Code:</strong>
                            {{ $cmsContent->service_detail_code }}
                        </div>
                        <div class="form-group">
                            <strong>Element Title:</strong>
                            {{ $cmsContent->element_title }}
                        </div>
                        <div class="form-group">
                            <strong>Element Content:</strong>
                            {{ $cmsContent->element_content }}
                        </div>
                        <div class="form-group">
                            <strong>Element Detail Code:</strong>
                            {{ $cmsContent->element_detail_code }}
                        </div>
                        <div class="form-group">
                            <strong>Excellence Title:</strong>
                            {{ $cmsContent->excellence_title }}
                        </div>
                        <div class="form-group">
                            <strong>Excellence Content:</strong>
                            {{ $cmsContent->excellence_content }}
                        </div>
                        <div class="form-group">
                            <strong>Excellence Detail Code:</strong>
                            {{ $cmsContent->excellence_detail_code }}
                        </div>
                        <div class="form-group">
                            <strong>Cta Title:</strong>
                            {{ $cmsContent->cta_title }}
                        </div>
                        <div class="form-group">
                            <strong>Cta Content:</strong>
                            {{ $cmsContent->cta_content }}
                        </div>
                        <div class="form-group">
                            <strong>Portfolio Title:</strong>
                            {{ $cmsContent->portfolio_title }}
                        </div>
                        <div class="form-group">
                            <strong>Portfolio Content:</strong>
                            {{ $cmsContent->portfolio_content }}
                        </div>
                        <div class="form-group">
                            <strong>Portfolio Detail Code:</strong>
                            {{ $cmsContent->portfolio_detail_code }}
                        </div>
                        <div class="form-group">
                            <strong>Team Title:</strong>
                            {{ $cmsContent->team_title }}
                        </div>
                        <div class="form-group">
                            <strong>Team Content:</strong>
                            {{ $cmsContent->team_content }}
                        </div>
                        <div class="form-group">
                            <strong>Team Detail Code:</strong>
                            {{ $cmsContent->team_detail_code }}
                        </div>
                        <div class="form-group">
                            <strong>Faq Title:</strong>
                            {{ $cmsContent->faq_title }}
                        </div>
                        <div class="form-group">
                            <strong>Faq Content:</strong>
                            {{ $cmsContent->faq_content }}
                        </div>
                        <div class="form-group">
                            <strong>Faq Detail Code:</strong>
                            {{ $cmsContent->faq_detail_code }}
                        </div>
                        <div class="form-group">
                            <strong>Contact Title:</strong>
                            {{ $cmsContent->contact_title }}
                        </div>
                        <div class="form-group">
                            <strong>Contact Content:</strong>
                            {{ $cmsContent->contact_content }}
                        </div>
                        <div class="form-group">
                            <strong>Contact Detail Code:</strong>
                            {{ $cmsContent->contact_detail_code }}
                        </div>
                        <div class="form-group">
                            <strong>Socmed Title:</strong>
                            {{ $cmsContent->socmed_title }}
                        </div>
                        <div class="form-group">
                            <strong>Socmed Content:</strong>
                            {{ $cmsContent->socmed_content }}
                        </div>
                        <div class="form-group">
                            <strong>Socmed Detail Code:</strong>
                            {{ $cmsContent->socmed_detail_code }}
                        </div>

                    </div>
                </div>
            <!--end::Profile Account-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Content-->
	</div>
	<!--end::Main-->
@endsection
