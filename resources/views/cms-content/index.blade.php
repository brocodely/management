@extends('layouts.main-cms')
@section('title', __('Cms Content'))
@section('css')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link href="{{ URL::asset('assets/css/editor.css') }}" type="text/css" rel="stylesheet" />
@endsection
@section('content')
<!--begin::Main-->
<div class="d-flex flex-column flex-column-fluid">
    <!--begin::toolbar-->
    <div class="toolbar" id="kt_toolbar">
        <div class="container d-flex flex-stack flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex flex-column align-items-start justify-content-center flex-wrap me-1">

                <!--begin::Breadcrumb-->
                <ul class="breadcrumb breadcrumb-line bg-transparent text-muted fw-bold p-0 my-1 fs-7">
                    <li class="breadcrumb-item">
                        <a href="{{ url('home') }}" class="text-muted text-hover-primary">{{ __('Home') }}</a>
                    </li>
                    <li class="breadcrumb-item text-dark">{{ __('Cms Content') }}</li>
                </ul>
                <!--end::Breadcrumb-->
            </div>
            <!--end::Info-->


        </div>
    </div>
    <!--end::toolbar-->
    <!--begin::Content-->
    <div class="content fs-6 d-flex flex-column-fluid mt-5" id="kt_content">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Profile Account-->
            <div class="card">
                <div class="card-body">

                    <div class="container-fluid">
                        <div style="margin-bottom:25px">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h3>HOME</h3>
                                </div>
                            </div>
                            <div class="row mb-5">
                                <div class="col-lg-6 ">
                                    <h4 style="font-style: italic;">Headline</h4>
                                    <textarea id="home-headline"></textarea>
                                    <small id="home-headline-wait" style="display:none;">Saving....</small>
                                </div>
                                <div class="col-lg-6 ">
                                    <h4 style="font-style: italic;">Tagline</h4>
                                    <textarea id="home-tagline"></textarea>
                                    <small id="home-tagline-wait" style="display:none;">Saving....</small>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div style="margin-bottom:25px">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h3>ABOUT</h3>
                                </div>
                            </div>
                            <div class="row mb-5">
                                <div class="col-lg-6 ">
                                    <h4 style="font-style: italic;">Title</h4>
                                    <textarea id="about-title"></textarea>
                                    <small id="about-title-wait" style="display:none;">Saving....</small>
                                </div>
                                <div class="col-lg-6 ">
                                    <h4 style="font-style: italic;">Content</h4>
                                    <textarea id="about-content"></textarea>
                                    <small id="about-content-wait" style="display:none;">Saving....</small>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </div>

                </div>
            </div>
            <!--end::Profile Account-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Content-->
</div>
<!--end::Main-->
@endsection

@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="{{ URL::asset('assets/js/editor.js') }}"></script>

<script>
    $(document).ready(function() {

        //var str = $( '#editor2 .Editor-editor' ).text();
        //$('#txtEditor').val(str);

        $("#home-headline").Editor();
        $("#home-tagline").Editor();
        
        $("#about-title").Editor();
        $("#about-content").Editor();

    });

    $.ajax({
        url: `{{ route('api.getCms') }}`
        , success: function(response) {

            $("#home-headline").val(response.headline);
            $('.Editor-editor')[0].innerHTML = response.headline;
            
            $("#home-tagline").val(response.tagline);
            $('.Editor-editor')[1].innerHTML = response.tagline;
            
            $("#about-title").val(response.about_title);
            $('.Editor-editor')[2].innerHTML = response.about_title;
        }
    });

</script>
@endsection
