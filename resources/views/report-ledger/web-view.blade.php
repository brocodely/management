@if(!empty($data))
    <table class="fixed table-neraca-saldo" cellspacing=0 cellpadding=0 style="display: table; margin-top:0px;" width="100%">
        <thead>
            <tr style="margin-top:0px">
                <td colspan="7">
                    <h1 style="text-align:center; font-size:20px;margin-right:100px">Brocodely IT Solutions</h1>
                    <p></p>
                    <h4 style="text-align:center; font-size:15px; margin:0; margin-right:100px">PERINCIAN BUKU BESAR</h4>
                    <p style="text-align:center; font-size:15px; margin:0; margin-right:100px">{{ $periode1 }} - {{ $periode2 }}</p>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;">Tanggal</th>
                <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;">Sumber</th>
                <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;" width="20%">No. Sumber</th>
                <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;" width="20%">Keterangan</th>
                <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;">Debit</th>
                <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;">Kredit</th>
                <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;">Balance</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $key => $dta)
                @if ($key == 'ASET')
                    <tr>
                        <td></td>
                        <td style="text-align: left; "><h3 style="margin-top:10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $key }}</h3></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    @php
                        $total = 0;
                    @endphp
                    @foreach ($dta as $key => $dt)
                        <tr>
                            <td></td>
                            <td> <p style="font-style: italic; margin-top:10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>

                        @php
                            $subTotal = 0;
                        @endphp

                        @foreach ($dt as $key => $d) 

                            @php
                                if(!empty($d->saldo_awal_sisa)){
                                    $jumlah = $d->saldo_awal_sisa;
                                }elseif(!empty($d->saldo_akhir_sisa)){
                                    $jumlah = $d->saldo_akhir_sisa;
                                }else{
                                    $jumlah = $d->saldo_awal;
                                }
                            @endphp   
                            

                            <tr>
                                <td style="text-align: right;"><b>{{ $d->nomor_perkiraan }}</b></td>
                                <td style="text-align: right;"><b>{{ $d->nama_akun }}</b></td>
                                <td style="text-align: right;"><b>Rp. {{ number_format($jumlah, 2) }}</b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            
                            @php
                                $dtail = false;
                                $dbit = 0;
                                $kredit = 0;
                            @endphp
                            @if(!empty($d->jurnal))
                                @foreach ($d->jurnal as $key => $jurnal)
                                    @php
                                        $count = count($d->jurnal);
                                    @endphp
                                    
                                    @if($jurnal->nomor_perkiraan == $d->nomor_perkiraan)
                                        
                                        
                                        @if($jurnal->id)
                                            
                                            {{-- biar ga offset --}}
                                            <tr>
                                                <td>{{ $jurnal->tanggal }}</td>
                                                <td>Bukti Jurnal</td>
                                                <td>
                                                    <a href="{{ url('finance-transactions/'. $jurnal->main_id .'/print') }}" target="_blank">{{ $jurnal->nomor_voucher }}</a>
                                                </td>
                                                <td>{{ $jurnal->keterangan }}</td>
                                                <td>Rp. {{ number_format($jurnal->debit, 2) }}</td>
                                                <td>Rp. {{ number_format($jurnal->kredit, 2) }}</td>
                                                @php
                                                    //debit
                                                    $dbit += $jurnal->debit; 
                                                    
                                                    //kredit
                                                    $kredit += $jurnal->kredit; 

                                                    //sisa
                                                    if(!isset($sisa)){
                                                        
                                                        if($jurnal->bagian == 'ASET'){
                
                                                            $sisa = $jumlah + $jurnal->debit - $jurnal->kredit;
                                                            
                                                        }elseif($jurnal->bagian == 'LIABILITAS'){
                                                            
                                                            $sisa = $jumlah - $jurnal->debit + $jurnal->kredit;
                                                            
                                                        }elseif($jurnal->bagian == 'BIAYA' || $jurnal->bagian == 'BEBAN'){
                                                            
                                                            $sisa = $jumlah - $jurnal->debit + $jurnal->kredit;

                                                        }elseif($jurnal->bagian == 'PENDAPATAN' || $jurnal->bagian == 'PENDAPATAN LAINNYA'){
                                                            
                                                            $sisa = $jumlah - $jurnal->debit + $jurnal->kredit;

                                                        }

                                                    }else{

                                                        if($jurnal->bagian == 'ASET'){
                
                                                            $sisa = $sisa + $jurnal->debit - $jurnal->kredit;
                                                            
                                                        }elseif($jurnal->bagian == 'LIABILITAS'){
                                                            
                                                            $sisa = $sisa - $jurnal->debit + $jurnal->kredit;
                                                            
                                                        }elseif($jurnal->bagian == 'BIAYA' || $jurnal->bagian == 'BEBAN'){
                                                            
                                                            $sisa = $sisa - $jurnal->debit + $jurnal->kredit;

                                                        }elseif($jurnal->bagian == 'PENDAPATAN' || $jurnal->bagian == 'PENDAPATAN LAINNYA'){

                                                                $sisa = $sisa - $jurnal->debit + $jurnal->kredit;

                                                        }
                                                    }
                                                @endphp
                                                <td>Rp. {{ number_format($sisa, 2) }}</td>
                                            </tr>
                                        @endif

                                    @endif
                                @endforeach
                                @if($dtail == true)
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td style="border-top:1px solid #333">Rp. {{ number_format($dbit , 2)}}</td>
                                        <td style="border-top:1px solid #333">Rp. {{ number_format($kredit, 2) }}</td>
                                        <td></td>
                                    </tr>
                                @endif
                            @endif
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @php
                                $jml = count($dt) -1;
                                $subTotal += $jumlah;
                            @endphp
                            @if ($key == $jml)
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            @endif
                            @php
                                $sisa = NULL;
                            @endphp
                        @endforeach
                        @php
                            $total += $subTotal;
                        @endphp
                    @endforeach
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                @endif
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tfoot>
        
    </table>
        @if(!empty($data2))
        <div class="page_break">
    @endif
@endif
@if(!empty($data2))
        <table class="fixed table-neraca-saldo" cellspacing=0 cellpadding=0 style="display: table; margin-top:0px" width="100%">
            <thead>
                <tr style="margin-top:0px">
                    <td colspan="7">
                        <h1 style="text-align:center; font-size:20px;margin-right:100px">Brocodely IT Solutions</h1>
                        <p></p>
                        <h4 style="text-align:center; font-size:15px; margin:0; margin-right:100px">PERINCIAN BUKU BESAR</h4>
                        <p style="text-align:center; font-size:15px; margin:0; margin-right:100px">{{ $periode1 }} - {{ $periode2 }}</p>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;">Tanggal</th>
                    <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;">Sumber</th>
                    <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;" width="20%">No. Sumber</th>
                    <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;" width="20%">Keterangan</th>
                    <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;">Debit</th>
                    <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;">Kredit</th>
                    <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;">Balance</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data2 as $key => $dta)
                    @if ($key != 'ASET' && $key != 'LIABILITAS')
                        <tr>
                            <td></td>
                            <td style="text-align: left; "><h3 style="margin-top:10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $key }}</h3></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        @php
                            $total = 0;
                        @endphp
                        @foreach ($dta as $key => $dt)
                            <tr>
                                <td></td>
                                <td> <p style="font-style: italic; margin-top:10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>

                            @php
                                $subTotal = 0;
                            @endphp

                            @foreach ($dt as $key => $d)   
                                
                                @php
                                    if(!empty($d->saldo_awal_sisa)){
                                        $jumlah = $d->saldo_awal_sisa;
                                    }elseif(!empty($d->saldo_akhir_sisa)){
                                        $jumlah = $d->saldo_akhir_sisa;
                                    }else{
                                        $jumlah = $d->saldo_awal;
                                    }
                                @endphp    

                                <tr>
                                    <td><b>{{ $d->nomor_perkiraan }}</b></td>
                                    <td><b>{{ $d->nama_akun }}</b></td>
                                    <td style="text-align: right;"><b>Rp. {{ number_format($jumlah, 2) }}</b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>

                                @php
                                    $dtail = false;
                                    $dbit = 0;
                                    $kredit = 0;
                                @endphp

                                @if(!empty($d->jurnal))
                                @foreach ($d->jurnal as $key => $jurnal)
                                    @php
                                        $count = count($d->jurnal);
                                    @endphp
                                    
                                    @if($jurnal->nomor_perkiraan == $d->nomor_perkiraan)
                                        
                                        
                                        @if($jurnal->id)
                                            
                                            {{-- biar ga offset --}}
                                            <tr>
                                                <td>{{ $jurnal->tanggal }}</td>
                                                <td>Bukti Jurnal</td>
                                                <td>
                                                    <a href="{{ url('finance-transactions/'. $jurnal->main_id .'/print') }}" target="_blank">{{ $jurnal->nomor_voucher }}</a>
                                                </td>
                                                <td>{{ $jurnal->keterangan }}</td>
                                                <td>Rp. {{ number_format($jurnal->debit, 2) }}</td>
                                                <td>Rp. {{ number_format($jurnal->kredit, 2) }}</td>
                                                @php
                                                    //debit
                                                    $dbit += $jurnal->debit; 
                                                    
                                                    //kredit
                                                    $kredit += $jurnal->kredit; 

                                                    //sisa
                                                    if(!isset($sisa)){
                                                        
                                                        if($jurnal->bagian == 'ASET'){
                
                                                            $sisa = $jumlah + $jurnal->debit - $jurnal->kredit;
                                                            
                                                        }elseif($jurnal->bagian == 'LIABILITAS'){
                                                            
                                                            $sisa = $jumlah - $jurnal->debit + $jurnal->kredit;
                                                            
                                                        }elseif($jurnal->bagian == 'BIAYA' || $jurnal->bagian == 'BEBAN'){
                                                            
                                                            $sisa = $jumlah - $jurnal->debit + $jurnal->kredit;

                                                        }elseif($jurnal->bagian == 'PENDAPATAN' || $jurnal->bagian == 'PENDAPATAN LAINNYA'){
                                                            
                                                            $sisa = $jumlah - $jurnal->debit + $jurnal->kredit;

                                                        }

                                                    }else{

                                                        if($jurnal->bagian == 'ASET'){
                
                                                            $sisa = $sisa + $jurnal->debit - $jurnal->kredit;
                                                            
                                                        }elseif($jurnal->bagian == 'LIABILITAS'){
                                                            
                                                            $sisa = $sisa - $jurnal->debit + $jurnal->kredit;
                                                            
                                                        }elseif($jurnal->bagian == 'BIAYA' || $jurnal->bagian == 'BEBAN'){
                                                            
                                                            $sisa = $sisa - $jurnal->debit + $jurnal->kredit;

                                                        }elseif($jurnal->bagian == 'PENDAPATAN' || $jurnal->bagian == 'PENDAPATAN LAINNYA'){

                                                                $sisa = $sisa - $jurnal->debit + $jurnal->kredit;

                                                        }
                                                    }
                                                @endphp
                                                <td>Rp. {{ number_format($sisa, 2) }}</td>
                                            </tr>
                                        @endif

                                    @endif
                                @endforeach
                                @if($dtail == true)
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td style="border-top:1px solid #333">Rp. {{ number_format($dbit , 2)}}</td>
                                        <td style="border-top:1px solid #333">Rp. {{ number_format($kredit, 2) }}</td>
                                        <td></td>
                                    </tr>
                                @endif
                            @endif
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @php
                                $jml = count($dt) -1;
                                $subTotal += $jumlah;
                            @endphp
                            @if ($key == $jml)
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            @endif
                            @php
                                $sisa = NULL;
                            @endphp
                        @endforeach
                        @php
                            $total += $subTotal;
                        @endphp
                    @endforeach
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                @endif
            @endforeach
        </tbody>
            <tfoot>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tfoot>
            
        </table>
    </div>
@endif