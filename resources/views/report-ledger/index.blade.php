@extends('layouts.main')
@section('title', 'Laporan Buku Besar')
@section('content')
    <!--begin::Main-->
	<div class="d-flex flex-column flex-column-fluid">
		<!--begin::toolbar-->
		<div class="toolbar" id="kt_toolbar">
			<div class="container d-flex flex-stack flex-wrap flex-sm-nowrap">
				<!--begin::Info-->
				<div class="d-flex flex-column align-items-start justify-content-center flex-wrap me-1">
					<!--begin::Breadcrumb-->
					<ul class="breadcrumb breadcrumb-line bg-transparent text-muted fw-bold p-0 my-1 fs-7">
						<li class="breadcrumb-item">
							<a href="{{ url('/report-ledgers') }}" class="text-muted text-hover-primary">Laporan Buku Besar</a>
						</li>
						<li class="breadcrumb-item text-dark">Laporan Buku Besar</li>							
					</ul>
					<!--end::Breadcrumb-->
				</div>
				<!--end::Info-->
			</div>
		</div>
		<!--end::toolbar-->
		<!--begin::Content-->
		<div class="content fs-6 d-flex flex-column-fluid mt-5" id="kt_content">
			<!--begin::Container-->
			<div class="container">
				<!--begin::Profile Account-->
				<div class="card">
                    <div class="card-header py-5">
                        <div class="d-flex justify-content-between align-items-center">
                            <span id="card_title">
                                <h4 class="m-0">Laporan Buku Besar</h4>
                            </span>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg">
                                <input type="text" class="form-control" id="daterange" required name="periode">
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-lg-4 print">
                                <button class="btn btn-danger preview">PDF</button>
                                <button class="btn btn-success web">Web View</button>
                                <button class="btn btn-primary excel">Excel</button>
                            </div>
                            <div class="loader-spin">
                            </div>
                        </div>
                    </div>
                </div>
				<!--end::Profile Account-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Content-->
	</div>
    
@endsection
@section('scripts')
    <script>

        $('.preview').on('click', function(){
            var date = $("input[name='periode']").val();
            window.open('/report-ledgers/generate?type=pdf&periode=' + date, '_blank');
        });
        $('.web').on('click', function(){
            var date = $("input[name='periode']").val();
            window.open('/report-ledgers/generate?type=web&periode=' + date, '_blank');
        });
        $('.excel').on('click', function(){
            var date = $("input[name='periode']").val();
            window.open('/report-ledgers/export?type=web&periode=' + date, '_blank');
        });
        
        $("#daterange").daterangepicker();
    </script>
@endsection
