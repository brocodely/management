
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>BUKU BESAR</title>
        <style>
            html,body{
                font-size: 11px;
                font-family: "Arial";
            }

            h1{
                font-size: 18px;
                margin: 0px;
                padding: 0px;
            }
            h3{
                font-size: 13px;
                margin: 0px;
                padding: 0px;
            }

            table.top{
                border:1px solid #333;
                width: 100%;
            }

            table.top.no-border{
                border:none;
                width: 100%;
            }

            table.top td{
                padding: 15px;
            }
            table.top td.content{
                padding-top: 5px;
            }

            table.table-content{
                border: 0px solid #333;
                padding: 0px;
                width: 100%;
            }

            table.table-content.bordered{
                border: 1px solid #333;
                padding: 0px;
                width: 100%;
            }

            table.table-content > thead > tr > th{
                padding: 5px;
                border-right: 0px solid #333;
                border-bottom: 0px solid #333;
                margin: 0px;
                vertical-align: top;
                font-weight: bold;
            }

            table.table-content.bordered > thead > tr > th{
                padding: 5px;
                border-right: 1px solid #333;
                border-bottom: 1px solid #333;
                margin: 0px;
                vertical-align: top;
                font-weight: bold;
            }
            table.table-content.bordered > tfoot > tr > th{
                padding: 5px;
                border-right: 1px solid #333;
                border-bottom: 1px solid #333;
                margin: 0px;
                vertical-align: top;
                font-weight: bold;
            }

            table.table-content > thead > tr > th:last-child{
                border-right: none;
            }
            /*table.table-content > thead > tr:last-child > th{
                border-bottom: none;
            }*/


            table.table-content > tbody > tr > td{
                padding: 5px;
                border-right: 0px solid #333;
                border-bottom: 0px solid #333;
                margin: 0px;
                vertical-align: top;
            }

            table.table-content.bordered > tbody > tr > td{
                padding: 5px;
                border-right: 1px solid #333;
                border-bottom: 1px solid #333;
                margin: 0px;
                vertical-align: top;
            }


            .detail-content{
                padding: 0px !important;
            }

            table.table-content > tbody > tr > td:last-child{
                border-right: none;
            }
            table.table-content > tbody > tr:last-child > td{
                border-bottom: none;
            }


            table.table-content > tfoot > tr > th{
                padding: 5px;
                border-right: 0px solid #333;
                border-bottom: 0px solid #333;
                margin: 0px;
                vertical-align: top;
            }

            table.table-content > tfoot > tr > th:last-child{
                border-right: none;
            }
            table.table-content > tfoot > tr:last-child > th{
                border-bottom: none;
            }
            table.table-content > tfoot > tr:first-child > th{
                border-top:  0px solid #333;
            }



            table.table-detail{
                padding: 0px;
                width: 100%;
                border:1px solid #333;
                margin:15px 0px;
            }


            table.table-detail > thead > tr > th{
                padding: 5px;
                border-right: 1px solid #333;
                border-bottom: 1px solid #333;
                margin: 0px;
                vertical-align: top;
            }


            table.table-detail > thead > tr > th:last-child{
                border-right: none;
            }

            table.table-detail > tbody > tr > td,table.table-detail > tbody > tr > th{
                padding: 5px;
                border-right: 1px solid #333;
                border-bottom: 1px solid #333;
                margin: 0px;
                vertical-align: top;
            }

            table.table-detail > tbody > tr > td:last-child,table.table-detail > tbody > tr > th:last-child{
                border-right: none;
            }
            table.table-detail > tbody > tr:last-child > td,table.table-detail > tbody > tr:last-child > th{
                border-bottom: none;
            }

            .text-left{
                text-align: left;
            }
            .text-center{
                text-align: center;
            }

            .text-right{
                text-align: right;
            }

            .label-dokumen{
                padding: 15px;
                border:1px solid #333;
                display: inline-block;
                text-align: center;
            }

            .logo{
                width: 150px;
                max-width: 100%;
            }

            .bg-grey td{
                background: #ddd;
            }

            .table-buku-besar{
                border-top:3px double #333 !important;
                border-bottom:3px double #333 !important;
                margin-bottom:25px;
                width: 100% !important;
            }

            .table-buku-besar thead tr:last-child th{
                border-bottom:1px double #333;
            }
            .table-buku-besar tfoot tr:first-child th{
                border-top:1px double #333 !important;
            }

            .table-neraca-saldo{
                margin-bottom:25px;
                
            }

            .table-neraca-saldo thead tr:first-child th{
                border-top:3px double #333 !important;
                padding:10px !important;
            }
            .table-neraca-saldo thead tr th{
                vertical-align: middle !important;
            }
            .table-neraca-saldo thead tr th.right-border{
                border-right:1px solid #333 !important;
            }

            .table-neraca-saldo thead tr th{
                border-bottom:1px solid #333 !important;
                border-right:1px solid #333 !important;
                border-left:1px solid #333 !important;
                border-top:1px solid #333 !important;
                padding:3px;
            }
            .table-neraca-saldo tbody td{
                border-right:1px solid #333 !important;
                border-left:1px solid #333 !important;
                padding:3px;
            }
            .table-buku-besar tfoot tr:first-child th{
                border-top:1px double #333 !important;
            }
            .table-neraca-saldo tr.row-header td{
                padding-top:10px !important;
                padding-bottom:10px !important;
                font-weight: bold;
            }
            .table-neraca-saldo tr.row-footer td{
                border-top:1px solid #333 !important;
                border-bottom:1px solid #333 !important;
                font-weight: bold;
            }
            .table-neraca-saldo tfoot tr td{
                border-top:3px double #333 !important;
                font-weight: bold;
                padding:10px 5px;
            }

            .table-voucher{
                width:100%;
                border:3px double #333;
            }
            .table-voucher > thead th{
                text-align:left;
                padding: 5px;
            }
            .table-signature{
                width:75%;
                margin-left:auto; 
                margin-right:auto;
                margin-bottom:15px;
            }
            .page_break { page-break-before: always; }
            table.fixed {table-layout:fixed; width:100%;word-break:break-all;}/*Setting the table width is important!*/
            table.fixed td {overflow:hidden;word-wrap:break-word;}/*Hide text outside the cell.*/
            table.fixed td:nth-of-type(1) {width:200px;}/*Setting the width of column 1.*/
            table.fixed td:nth-of-type(2) {width:300px;}/*Setting the width of column 2.*/
            table.fixed td:nth-of-type(3) {width:400px;}/*Setting the width of column 3.*/

            a:link {
            color:#333333;
            text-decoration:none;
            }

            a:hover {
            color:#333333;
            text-decoration : none;
            }

            a:visited {
            color:#333333;
            text-decoration : none;
            }
        </style>
    </head>
    <body>
        <br>
        @if(!empty($data))
            <table class="fixed table-neraca-saldo" cellspacing=0 cellpadding=0 style="display: table; margin-top:0px;" width="100%">
                <thead>
                    <tr style="margin-top:0px">
                        <td colspan="7">
                            <h1 style="text-align:center; font-size:20px;margin-right:100px">Brocodely IT Solutions</h1>
                            <p></p>
                            <h4 style="text-align:center; font-size:15px; margin:0; margin-right:100px">PERINCIAN BUKU BESAR</h4>
                            <p style="text-align:center; font-size:15px; margin:0; margin-right:100px">{{ $periode1 }} - {{ $periode2 }}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;">Tanggal</th>
                        <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;">Sumber</th>
                        <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;" width="20%">No. Sumber</th>
                        <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;" width="20%">Keterangan</th>
                        <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;">Debit</th>
                        <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;">Kredit</th>
                        <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;">Balance</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $key => $dta)
                        @if ($key == 'ASET')
                            <tr>
                                <td></td>
                                <td style="text-align: left; "><h3 style="margin-top:10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $key }}</h3></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @php
                                $total = 0;
                            @endphp
                            @foreach ($dta as $key => $dt)
                                <tr>
                                    <td></td>
                                    <td> <p style="font-style: italic; margin-top:10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>

                                @php
                                    $subTotal = 0;
                                @endphp

                                @foreach ($dt as $key => $d) 

                                    @php
                                        if(!empty($d->saldo_awal_sisa)){
                                            $jumlah = $d->saldo_awal_sisa;
                                        }elseif(!empty($d->saldo_akhir_sisa)){
                                            $jumlah = $d->saldo_akhir_sisa;
                                        }else{
                                            $jumlah = $d->saldo_awal;
                                        }
                                    @endphp   
                                    

                                    <tr>
                                        <td style="text-align: right;"><b>{{ $d->nomor_perkiraan }}</b></td>
                                        <td style="text-align: right;"><b>{{ $d->nama_akun }}</b></td>
                                        <td style="text-align: right;"><b>Rp. {{ number_format($jumlah, 2) }}</b></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    
                                    @php
                                        $dtail = false;
                                        $dbit = 0;
                                        $kredit = 0;
                                    @endphp
                                    @if(!empty($d->jurnal))
                                        @foreach ($d->jurnal as $key => $jurnal)
                                            @php
                                                $count = count($d->jurnal);
                                            @endphp
                                            
                                            @if($jurnal->nomor_perkiraan == $d->nomor_perkiraan)
                                                
                                                
                                                @if($jurnal->id)
                                                    
                                                    {{-- biar ga offset --}}
                                                    <tr>
                                                        <td>{{ $jurnal->tanggal }}</td>
                                                        <td>Bukti Jurnal</td>
                                                        <td>
                                                            <a href="{{ url('finance-transactions/'. $jurnal->main_id .'/print') }}" target="_blank">{{ $jurnal->nomor_voucher }}</a>
                                                        </td>
                                                        <td>{{ $jurnal->keterangan }}</td>
                                                        <td>Rp. {{ number_format($jurnal->debit, 2) }}</td>
                                                        <td>Rp. {{ number_format($jurnal->kredit, 2) }}</td>
                                                        @php
                                                            //debit
                                                            $dbit += $jurnal->debit; 
                                                            
                                                            //kredit
                                                            $kredit += $jurnal->kredit; 

                                                            //sisa
                                                            if(!isset($sisa)){
                                                                
                                                                if($jurnal->bagian == 'ASET'){
                        
                                                                    $sisa = $jumlah + $jurnal->debit - $jurnal->kredit;
                                                                    
                                                                }elseif($jurnal->bagian == 'LIABILITAS'){
                                                                    
                                                                    $sisa = $jumlah - $jurnal->debit + $jurnal->kredit;
                                                                    
                                                                }elseif($jurnal->bagian == 'BIAYA' || $jurnal->bagian == 'BEBAN'){
                                                                    
                                                                    $sisa = $jumlah - $jurnal->debit + $jurnal->kredit;

                                                                }elseif($jurnal->bagian == 'PENDAPATAN' || $jurnal->bagian == 'PENDAPATAN LAINNYA'){
                                                                    
                                                                    $sisa = $jumlah - $jurnal->debit + $jurnal->kredit;

                                                                }

                                                            }else{

                                                                if($jurnal->bagian == 'ASET'){
                        
                                                                    $sisa = $sisa + $jurnal->debit - $jurnal->kredit;
                                                                    
                                                                }elseif($jurnal->bagian == 'LIABILITAS'){
                                                                    
                                                                    $sisa = $sisa - $jurnal->debit + $jurnal->kredit;
                                                                    
                                                                }elseif($jurnal->bagian == 'BIAYA' || $jurnal->bagian == 'BEBAN'){
                                                                    
                                                                    $sisa = $sisa - $jurnal->debit + $jurnal->kredit;

                                                                }elseif($jurnal->bagian == 'PENDAPATAN' || $jurnal->bagian == 'PENDAPATAN LAINNYA'){

                                                                     $sisa = $sisa - $jurnal->debit + $jurnal->kredit;

                                                                }
                                                            }
                                                        @endphp
                                                        <td>Rp. {{ number_format($sisa, 2) }}</td>
                                                    </tr>
                                                @endif

                                            @endif
                                        @endforeach
                                        @if($dtail == true)
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td style="border-top:1px solid #333">Rp. {{ number_format($dbit , 2)}}</td>
                                                <td style="border-top:1px solid #333">Rp. {{ number_format($kredit, 2) }}</td>
                                                <td></td>
                                            </tr>
                                        @endif
                                    @endif
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    @php
                                        $jml = count($dt) -1;
                                        $subTotal += $jumlah;
                                    @endphp
                                    @if ($key == $jml)
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                    @php
                                        $sisa = NULL;
                                    @endphp
                                @endforeach
                                @php
                                    $total += $subTotal;
                                @endphp
                            @endforeach
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tfoot>
                
            </table>
             @if(!empty($data2))
                <div class="page_break">
            @endif
        @endif
        @if(!empty($data2))
                <table class="fixed table-neraca-saldo" cellspacing=0 cellpadding=0 style="display: table; margin-top:0px" width="100%">
                    <thead>
                        <tr style="margin-top:0px">
                            <td colspan="7">
                                <h1 style="text-align:center; font-size:20px;margin-right:100px">Brocodely IT Solutions</h1>
                                <p></p>
                                <h4 style="text-align:center; font-size:15px; margin:0; margin-right:100px">PERINCIAN BUKU BESAR</h4>
                                <p style="text-align:center; font-size:15px; margin:0; margin-right:100px">{{ $periode1 }} - {{ $periode2 }}</p>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;">Tanggal</th>
                            <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;">Sumber</th>
                            <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;" width="20%">No. Sumber</th>
                            <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;" width="20%">Keterangan</th>
                            <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;">Debit</th>
                            <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;">Kredit</th>
                            <th height="25px;" style="display: table-cell;vertical-align: middle;background-color:#c7c7c7;">Balance</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data2 as $key => $dta)
                            @if ($key != 'ASET' && $key != 'LIABILITAS')
                                <tr>
                                    <td></td>
                                    <td style="text-align: left; "><h3 style="margin-top:10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $key }}</h3></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                @php
                                    $total = 0;
                                @endphp
                                @foreach ($dta as $key => $dt)
                                    <tr>
                                        <td></td>
                                        <td> <p style="font-style: italic; margin-top:10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>

                                    @php
                                        $subTotal = 0;
                                    @endphp

                                    @foreach ($dt as $key => $d)   
                                        
                                        @php
                                            if(!empty($d->saldo_awal_sisa)){
                                                $jumlah = $d->saldo_awal_sisa;
                                            }elseif(!empty($d->saldo_akhir_sisa)){
                                                $jumlah = $d->saldo_akhir_sisa;
                                            }else{
                                                $jumlah = $d->saldo_awal;
                                            }
                                        @endphp    

                                        <tr>
                                            <td><b>{{ $d->nomor_perkiraan }}</b></td>
                                            <td><b>{{ $d->nama_akun }}</b></td>
                                            <td style="text-align: right;"><b>Rp. {{ number_format($jumlah, 2) }}</b></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>

                                        @php
                                            $dtail = false;
                                            $dbit = 0;
                                            $kredit = 0;
                                        @endphp

                                        @if(!empty($d->jurnal))
                                        @foreach ($d->jurnal as $key => $jurnal)
                                            @php
                                                $count = count($d->jurnal);
                                            @endphp
                                            
                                            @if($jurnal->nomor_perkiraan == $d->nomor_perkiraan)
                                                
                                                
                                                @if($jurnal->id)
                                                    
                                                    {{-- biar ga offset --}}
                                                    <tr>
                                                        <td>{{ $jurnal->tanggal }}</td>
                                                        <td>Bukti Jurnal</td>
                                                        <td>
                                                            <a href="{{ url('finance-transactions/'. $jurnal->main_id .'/print') }}" target="_blank">{{ $jurnal->nomor_voucher }}</a>
                                                        </td>
                                                        <td>{{ $jurnal->keterangan }}</td>
                                                        <td>Rp. {{ number_format($jurnal->debit, 2) }}</td>
                                                        <td>Rp. {{ number_format($jurnal->kredit, 2) }}</td>
                                                        @php
                                                            //debit
                                                            $dbit += $jurnal->debit; 
                                                            
                                                            //kredit
                                                            $kredit += $jurnal->kredit; 

                                                            //sisa
                                                            if(!isset($sisa)){
                                                                
                                                                if($jurnal->bagian == 'ASET'){
                        
                                                                    $sisa = $jumlah + $jurnal->debit - $jurnal->kredit;
                                                                    
                                                                }elseif($jurnal->bagian == 'LIABILITAS'){
                                                                    
                                                                    $sisa = $jumlah - $jurnal->debit + $jurnal->kredit;
                                                                    
                                                                }elseif($jurnal->bagian == 'BIAYA' || $jurnal->bagian == 'BEBAN'){
                                                                    
                                                                    $sisa = $jumlah - $jurnal->debit + $jurnal->kredit;

                                                                }elseif($jurnal->bagian == 'PENDAPATAN' || $jurnal->bagian == 'PENDAPATAN LAINNYA'){
                                                                    
                                                                    $sisa = $jumlah - $jurnal->debit + $jurnal->kredit;

                                                                }

                                                            }else{

                                                                if($jurnal->bagian == 'ASET'){
                        
                                                                    $sisa = $sisa + $jurnal->debit - $jurnal->kredit;
                                                                    
                                                                }elseif($jurnal->bagian == 'LIABILITAS'){
                                                                    
                                                                    $sisa = $sisa - $jurnal->debit + $jurnal->kredit;
                                                                    
                                                                }elseif($jurnal->bagian == 'BIAYA' || $jurnal->bagian == 'BEBAN'){
                                                                    
                                                                    $sisa = $sisa - $jurnal->debit + $jurnal->kredit;

                                                                }elseif($jurnal->bagian == 'PENDAPATAN' || $jurnal->bagian == 'PENDAPATAN LAINNYA'){

                                                                     $sisa = $sisa - $jurnal->debit + $jurnal->kredit;

                                                                }
                                                            }
                                                        @endphp
                                                        <td>Rp. {{ number_format($sisa, 2) }}</td>
                                                    </tr>
                                                @endif

                                            @endif
                                        @endforeach
                                        @if($dtail == true)
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td style="border-top:1px solid #333">Rp. {{ number_format($dbit , 2)}}</td>
                                                <td style="border-top:1px solid #333">Rp. {{ number_format($kredit, 2) }}</td>
                                                <td></td>
                                            </tr>
                                        @endif
                                    @endif
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    @php
                                        $jml = count($dt) -1;
                                        $subTotal += $jumlah;
                                    @endphp
                                    @if ($key == $jml)
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                    @php
                                        $sisa = NULL;
                                    @endphp
                                @endforeach
                                @php
                                    $total += $subTotal;
                                @endphp
                            @endforeach
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tfoot>
                    
                </table>
            </div>
        @endif
    </body>
</html>
