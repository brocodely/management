@extends('layouts.main')
@section('title', __('Client'))
@section('content')
<!--begin::Main-->
	<div class="d-flex flex-column flex-column-fluid">
		<!--begin::toolbar-->
		<div class="toolbar" id="kt_toolbar">
			<div class="container d-flex flex-stack flex-wrap flex-sm-nowrap">
				<!--begin::Info-->
				<div class="d-flex flex-column align-items-start justify-content-center flex-wrap me-1">
					
					<!--begin::Breadcrumb-->
					<ul class="breadcrumb breadcrumb-line bg-transparent text-muted fw-bold p-0 my-1 fs-7">
						<li class="breadcrumb-item">
							<a href="{{ url('') }}" class="text-muted text-hover-primary">Home</a>
						</li>
						<li class="breadcrumb-item text-dark">{{ __('Client') }}</li>							
					</ul>
					<!--end::Breadcrumb-->
				</div>
				<!--end::Info-->
				
				
			</div>
		</div>
		<!--end::toolbar-->
		<!--begin::Content-->
		<div class="content fs-6 d-flex flex-column-fluid mt-5" id="kt_content">
			<!--begin::Container-->
			<div class="container">
				<!--begin::Profile Account-->
                <div class="card">
                    <div class="card-header">
                        <div class="float-left mt-4">
                            <span class="card-title">Show Client</span>
                        </div>
                        <div class="float-right mt-4">
                            <a class="btn btn-primary" href="{{ route('clients.index') }}"> Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <table class="table border px-5  table-rounded table-flush align-middle">
                            <tbody>
                                <tr class="py-5 border-bottom border-gray-300 fs-6">
                                    <td class="fw-bolder text-capitalize">image</td>
                                    <td class="fw-bolder">:</td>
                                    <td><img src="{{ URL::asset('storage') }}/{{ $client->image }}" alt="{{ $client->image }}" width="100px" class="rounded"></td>
                                </tr>
                                <tr class="py-5 border-bottom border-gray-300 fs-6">
                                    <td class="fw-bolder text-capitalize">email</td>
                                    <td class="fw-bolder">:</td>
                                    <td>{{ $client->email }}</td>
                                </tr>
                                <tr class="py-5 border-bottom border-gray-300 fs-6">
                                    <td class="fw-bolder text-capitalize">nama</td>
                                    <td class="fw-bolder">:</td>
                                    <td class="text-uppercase">{{ $client->name }}</td>
                                </tr>
                                <tr class="py-5 border-bottom border-gray-300 fs-6">
                                    <td class="fw-bolder text-capitalize">telp</td>
                                    <td class="fw-bolder">:</td>
                                    <td class="text-uppercase">{{ $client->phone }}</td>
                                </tr>
                                <tr class="py-5 border-bottom border-gray-300 fs-6">
                                    <td class="fw-bolder text-capitalize">source</td>
                                    <td class="fw-bolder">:</td>
                                    <td class="text-uppercase">{{ $client->source }}</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            <!--end::Profile Account-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Content-->
	</div>
	<!--end::Main-->
@endsection
