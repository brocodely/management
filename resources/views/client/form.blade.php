<div class="box box-info padding-1">
    <div class="box-body">
        {{ Form::hidden('oldImage', $client->image) }}
        <div class="form-group mb-5">
            {{ Form::label('image', '', ['class' => 'd-block']) }}
            <div class="row m-0">
                <div class="col-md-4 py-3">
                    <div class="d-flex align-items-center">
                        @if ($client->image)
                            <img src="{{ URL::asset('storage') }}/{{ $client->image }}" alt="{{ $client->image }}" id="image-profile" width="100px">
                        @else
                            <img src="" alt="" id="image-profile" width="100px">
                        @endif
                        <div class="d-none fw-bolder text-dark ms-7" id="text-preview">
                            <span class="d-block small">*Recommended ratio: 1:1</span>
                            <span class="d-block small">*Max image size: 2Mb</span>
                            <span class="d-block small">*Type of file must an image</span>
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::file('image', ['class' => 'form-control' . ($errors->has('image') ? ' is-invalid' : ''), 'onchange' => 'previewImage()']) }}
            {!! $errors->first('image', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-5">
            {{ Form::label('email', '', ['class' => 'mb-4']) }}
            {{ Form::text('email', $client->email, ['class' => 'form-control' . ($errors->has('email') ? ' is-invalid' : ''), 'placeholder' => 'Email']) }}
            {!! $errors->first('email', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-5">
            {{ Form::label('name', '', ['class' => 'mb-4']) }}
            {{ Form::text('name', $client->name, ['class' => 'form-control' . ($errors->has('name') ? ' is-invalid' : ''), 'placeholder' => 'Name']) }}
            {!! $errors->first('name', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-5">
            {{ Form::label('phone', '', ['class' => 'mb-4']) }}
            {{ Form::text('phone', $client->phone, ['class' => 'form-control' . ($errors->has('phone') ? ' is-invalid' : ''), 'placeholder' => 'Phone']) }}
            {!! $errors->first('phone', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-5">
            {{ Form::label('source', '', ['class' => 'mb-4']) }}
            <select name="source[]" id="source" class="form-select form-select-lg {{ $errors->has('source') ? 'is-invalid' : '' }}" data-control="select2" data-placeholder="Select an option" data-allow-clear="true" multiple="multiple">
                @if ($client->source)
                    <?php $sources = explode(", ", $client->source) ?>
                    @foreach ($sources as $source)
                        <option value="{{ $source }}" selected>{{ $source }}</option>
                    @endforeach
                    <option value="Website">Website</option>
                    <option value="Social Media">Social Media</option>
                    <option value="Friends">Friends</option>
                    <option value="Other">Other</option>
                @else
                    <option value="Website">Website</option>
                    <option value="Social Media">Social Media</option>
                    <option value="Friends">Friends</option>
                    <option value="Other">Other</option>
                @endif
            </select>
            {!! $errors->first('source', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>