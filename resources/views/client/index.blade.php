@extends('layouts.main')
@section('title', __('Client'))
@section('content')
    <!--begin::Main-->
	<div class="d-flex flex-column flex-column-fluid">
		<!--begin::toolbar-->
		<div class="toolbar" id="kt_toolbar">
			<div class="container d-flex flex-stack flex-wrap flex-sm-nowrap">
				<!--begin::Info-->
				<div class="d-flex flex-column align-items-start justify-content-center flex-wrap me-1">
					
					<!--begin::Breadcrumb-->
					<ul class="breadcrumb breadcrumb-line bg-transparent text-muted fw-bold p-0 my-1 fs-7">
						<li class="breadcrumb-item">
							<a href="{{ url('') }}" class="text-muted text-hover-primary">Home</a>
						</li>
						<li class="breadcrumb-item text-dark">{{ __('Client') }}</li>							
					</ul>
					<!--end::Breadcrumb-->
				</div>
				<!--end::Info-->
				
				
			</div>
		</div>
		<!--end::toolbar-->
		<!--begin::Content-->
		<div class="content fs-6 d-flex flex-column-fluid mt-5" id="kt_content">
			<!--begin::Container-->
			<div class="container">
				<!--begin::Profile Account-->
				<div class="card" >
					<div class="card-body">
						<a href="{{ route('clients.create') }}" class="btn btn-primary mb-3" >Tambah {{ __('Client') }} Baru</a>
						<!--begin::Alert-->                                        
						@if ($message = Session::get('success'))
							<div class="alert alert-success">
								<p>{{ $message }}</p>
							</div>
						@endif
						@if ($message = Session::get('danger'))
							<div class="alert alert-danger">
								<p>{{ $message }}</p>
							</div>
						@endif
						<!--end::Alert-->
						<div class="table-responsive">
							{{-- <table class="table table-striped table-hover gy-7 gs-10 text-center align-middle">
								<thead>
									<tr class="fw-bold fs-6 text-gray-800 border-bottom-2 border-gray-200">
										<th>No</th>
										<th>Image</th>
										<th>Email</th>
										<th>Name</th>
										<th>Phone</th>
										<th>Source</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>                                                 
                                    @foreach ($clients as $client)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            
											<td><img src="{{ URL::asset('storage') }}/{{ $client->image }}" alt="{{ $client->image }}" width="100px"></td>
											<td>{{ $client->email }}</td>
											<td>{{ $client->name }}</td>
											<td>{{ $client->phone }}</td>
											<td>{{ $client->source }}</td>

                                            <td>
												<a class="btn btn-sm btn-primary " href="{{ route('clients.show',$client->id) }}"><i class="fa fa-fw fa-eye"></i> Show</a>
												<a class="btn btn-sm btn-success" href="{{ route('clients.edit',$client->id) }}"><i class="fa fa-fw fa-edit"></i> Edit</a>
												<button type="button" class="btn btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#delete_client_{{ $client->id }}"><i class="fa fa-fw fa-trash"></i> Delete</button>
                                            </td>
                                        </tr>
                                    @endforeach
								</tbody>
							</table> --}}

							<table id="dataTable" class="table table-row-bordered gy-5">
							</table>
						</div>
					</div>
                </div>
				<!--end::Profile Account-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Content-->
	</div>
	<!--end::Main-->

	<!--begin::Delete Client Modal-->
	{{-- @foreach ($clients as $client)
	<div class="modal fade" tabindex="-1" id="delete_client_{{ $client->id }}" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg">
			<div class="modal-content bg-light-danger">
				<form action="{{ route('clients.destroy',$client->id) }}" method="POST">
					@csrf
					@method('DELETE')
					<div class="modal-body p-0">
						<!--begin::Alert-->
						<div class="alert alert-dismissible bg-light-danger d-flex flex-center flex-column pe-4">
							<!--begin::Close-->
							<button type="button" class="position-absolute top-0 end-0 m-2 btn btn-icon btn-icon-danger" data-bs-dismiss="modal" aria-label="Close">
								<span class="svg-icon svg-icon-1">
									<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
										<path opacity="0.25" fill-rule="evenodd" clip-rule="evenodd" d="M2.36899 6.54184C2.65912 4.34504 4.34504 2.65912 6.54184 2.36899C8.05208 2.16953 9.94127 2 12 2C14.0587 2 15.9479 2.16953 17.4582 2.36899C19.655 2.65912 21.3409 4.34504 21.631 6.54184C21.8305 8.05208 22 9.94127 22 12C22 14.0587 21.8305 15.9479 21.631 17.4582C21.3409 19.655 19.655 21.3409 17.4582 21.631C15.9479 21.8305 14.0587 22 12 22C9.94127 22 8.05208 21.8305 6.54184 21.631C4.34504 21.3409 2.65912 19.655 2.36899 17.4582C2.16953 15.9479 2 14.0587 2 12C2 9.94127 2.16953 8.05208 2.36899 6.54184Z" fill="#12131A"></path>
										<path fill-rule="evenodd" clip-rule="evenodd" d="M8.29289 8.29289C8.68342 7.90237 9.31658 7.90237 9.70711 8.29289L12 10.5858L14.2929 8.29289C14.6834 7.90237 15.3166 7.90237 15.7071 8.29289C16.0976 8.68342 16.0976 9.31658 15.7071 9.70711L13.4142 12L15.7071 14.2929C16.0976 14.6834 16.0976 15.3166 15.7071 15.7071C15.3166 16.0976 14.6834 16.0976 14.2929 15.7071L12 13.4142L9.70711 15.7071C9.31658 16.0976 8.68342 16.0976 8.29289 15.7071C7.90237 15.3166 7.90237 14.6834 8.29289 14.2929L10.5858 12L8.29289 9.70711C7.90237 9.31658 7.90237 8.68342 8.29289 8.29289Z" fill="#12131A"></path>
									</svg>
								</span>
							</button>
							<!--end::Close-->

							<!--begin::Icon-->
							<span class="svg-icon svg-icon-5tx svg-icon-danger mb-5">
								<svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><path d="M11.1669899,4.49941818 L2.82535718,19.5143571 C2.557144,19.9971408 2.7310878,20.6059441 3.21387153,20.8741573 C3.36242953,20.9566895 3.52957021,21 3.69951446,21 L21.2169432,21 C21.7692279,21 22.2169432,20.5522847 22.2169432,20 C22.2169432,19.8159952 22.1661743,19.6355579 22.070225,19.47855 L12.894429,4.4636111 C12.6064401,3.99235656 11.9909517,3.84379039 11.5196972,4.13177928 C11.3723594,4.22181902 11.2508468,4.34847583 11.1669899,4.49941818 Z" fill="#000000" opacity="0.3"></path>
									<rect fill="#000000" x="11" y="9" width="2" height="7" rx="1"></rect>
									<rect fill="#000000" x="11" y="17" width="2" height="2" rx="1"></rect>
								</svg>
							</span>
							<!--end::Icon-->

							<!--begin::Wrapper-->
							<div class="text-center">
								<!--begin::Title-->
								<h5 class="fw-bolder fs-1 mb-5">Peringatan</h5>
								<!--end::Title-->

								<!--begin::Separator-->
								<div class="separator separator-dashed border-danger opacity-25 mb-5"></div>
								<!--end::Separator-->

								<!--begin::Content-->
								<div class="mb-9">
									Tindakan ini akan <span class="text-danger fw-boldest">menghapus</span> data <strong>Client</strong> sekaligus data <strong>Product</strong> client yang bersangkutan.<br/>
									Jika anda benar-benar ingin menghapus <strong>Client</strong> ini, silahkan ketik <span class="text-danger fw-boldest">"KONFIRMASI"</span> dibawah.
									<input type="text" class="form-control mt-10 text-danger" name="delete_confirm">
								</div>
								<!--end::Content-->
							</div>
							<!--end::Wrapper-->
						</div>
						<!--end::Alert-->
					</div>
					<div class="modal-footer flex-center">
						<!--begin::Buttons-->
						<div class="d-flex flex-wrap">
							<button type="button" class="btn btn-outline btn-outline-danger btn-active-danger m-2" data-bs-dismiss="modal">Batal</button>
							<button type="submit" class="btn btn-danger m-2">Hapus Client</button>
						</div>
						<!--end::Buttons-->
					</div>
				</form>
			</div>
		</div>
	</div>
	@endforeach --}}
	<!--end::Delete Client Modal-->
	
@endsection

@section('scripts')
	<script type="text/javascript">


		$(document).ready(function(){
            var table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
				ajax: {
					url: "{{ route('clients.index') }}", 
					data: function ( d ) {
						d.test = 1;
					}
				},
                columns: 
				[
					{ 
						title: 'No',
						class: 'text-center',
                        data: 'id',
                        name: 'id'
                    },
					{
                        title: 'Image',
                        class: 'text-center',
                        data: 'image',
                        render: function(image, display, data) {
						    return `
                                <td><img src="{{ URL::asset('storage') }}/${image}" alt="" width="100px"></td>
                            `;
                        }
                    },
					{ 
						title: 'Email',
						class: 'text-center',
                        data: 'email',
                        name: 'email'
                    },
					{ 
						title: 'Nama Client',
						class: 'text-center',
                        data: 'name',
                        name: 'name'
                    },
					{ 
						title: 'Telp',
						class: 'text-center',
                        data: 'phone',
                        name: 'phone'
                    },
					{ 
						title: 'Source',
						class: 'text-center',
                        data: 'source',
                        name: 'source'
                    },
					{
                        title: 'Action',
                        class: 'text-center',
                        data: 'id',
                        render: function(id, display, data) {
                            return `
								<div class="modal fade" tabindex="-1" id="delete_client_`+id+`" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered modal-lg">
									<div class="modal-content bg-light-danger">
										<form action="{{ route('clients.destroy','') }}`+'/'+id+`" method="POST">
											@csrf
											@method('DELETE')
											<div class="modal-body p-0">
												<!--begin::Alert-->
												<div class="alert alert-dismissible bg-light-danger d-flex flex-center flex-column pe-4">
													<!--begin::Close-->
													<button type="button" class="position-absolute top-0 end-0 m-2 btn btn-icon btn-icon-danger" data-bs-dismiss="modal" aria-label="Close">
														<span class="svg-icon svg-icon-1">
															<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																<path opacity="0.25" fill-rule="evenodd" clip-rule="evenodd" d="M2.36899 6.54184C2.65912 4.34504 4.34504 2.65912 6.54184 2.36899C8.05208 2.16953 9.94127 2 12 2C14.0587 2 15.9479 2.16953 17.4582 2.36899C19.655 2.65912 21.3409 4.34504 21.631 6.54184C21.8305 8.05208 22 9.94127 22 12C22 14.0587 21.8305 15.9479 21.631 17.4582C21.3409 19.655 19.655 21.3409 17.4582 21.631C15.9479 21.8305 14.0587 22 12 22C9.94127 22 8.05208 21.8305 6.54184 21.631C4.34504 21.3409 2.65912 19.655 2.36899 17.4582C2.16953 15.9479 2 14.0587 2 12C2 9.94127 2.16953 8.05208 2.36899 6.54184Z" fill="#12131A"></path>
																<path fill-rule="evenodd" clip-rule="evenodd" d="M8.29289 8.29289C8.68342 7.90237 9.31658 7.90237 9.70711 8.29289L12 10.5858L14.2929 8.29289C14.6834 7.90237 15.3166 7.90237 15.7071 8.29289C16.0976 8.68342 16.0976 9.31658 15.7071 9.70711L13.4142 12L15.7071 14.2929C16.0976 14.6834 16.0976 15.3166 15.7071 15.7071C15.3166 16.0976 14.6834 16.0976 14.2929 15.7071L12 13.4142L9.70711 15.7071C9.31658 16.0976 8.68342 16.0976 8.29289 15.7071C7.90237 15.3166 7.90237 14.6834 8.29289 14.2929L10.5858 12L8.29289 9.70711C7.90237 9.31658 7.90237 8.68342 8.29289 8.29289Z" fill="#12131A"></path>
															</svg>
														</span>
													</button>
													<!--end::Close-->

													<!--begin::Icon-->
													<span class="svg-icon svg-icon-5tx svg-icon-danger mb-5">
														<svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><path d="M11.1669899,4.49941818 L2.82535718,19.5143571 C2.557144,19.9971408 2.7310878,20.6059441 3.21387153,20.8741573 C3.36242953,20.9566895 3.52957021,21 3.69951446,21 L21.2169432,21 C21.7692279,21 22.2169432,20.5522847 22.2169432,20 C22.2169432,19.8159952 22.1661743,19.6355579 22.070225,19.47855 L12.894429,4.4636111 C12.6064401,3.99235656 11.9909517,3.84379039 11.5196972,4.13177928 C11.3723594,4.22181902 11.2508468,4.34847583 11.1669899,4.49941818 Z" fill="#000000" opacity="0.3"></path>
															<rect fill="#000000" x="11" y="9" width="2" height="7" rx="1"></rect>
															<rect fill="#000000" x="11" y="17" width="2" height="2" rx="1"></rect>
														</svg>
													</span>
													<!--end::Icon-->

													<!--begin::Wrapper-->
													<div class="text-center">
														<!--begin::Title-->
														<h5 class="fw-bolder fs-1 mb-5">Peringatan</h5>
														<!--end::Title-->

														<!--begin::Separator-->
														<div class="separator separator-dashed border-danger opacity-25 mb-5"></div>
														<!--end::Separator-->

														<!--begin::Content-->
														<div class="mb-9">
															Tindakan ini akan <span class="text-danger fw-boldest">menghapus</span> data <strong>Client</strong> sekaligus data <strong>Product</strong> client yang bersangkutan.<br/>
															Jika anda benar-benar ingin menghapus <strong>Client</strong> ini, silahkan ketik <span class="text-danger fw-boldest">"KONFIRMASI"</span> dibawah.
															<input type="text" class="form-control mt-10 text-danger" name="delete_confirm">
														</div>
														<!--end::Content-->
													</div>
													<!--end::Wrapper-->
												</div>
												<!--end::Alert-->
											</div>
											<div class="modal-footer flex-center">
												<!--begin::Buttons-->
												<div class="d-flex flex-wrap">
													<button type="button" class="btn btn-outline btn-outline-danger btn-active-danger m-2" data-bs-dismiss="modal">Batal</button>
													<button type="submit" class="btn btn-danger m-2">Hapus Client</button>
												</div>
												<!--end::Buttons-->
											</div>
										</form>
									</div>
								</div>
							</div>
							<form action="{{ route('clients.destroy','') }}`+'/'+id+`" method="POST">
								<a class="btn btn-sm btn-primary " href="{{ route('clients.index') }}`+'/'+id+`"><i class="fa fa-fw fa-eye"></i> Show</a>
								<a class="btn btn-sm btn-success" href="{{ route('clients.index') }}`+'/'+id+ '/edit'+`"><i class="fa fa-fw fa-edit"></i> Edit</a>
								@csrf 
								@method('DELETE')
								<button type="button" class="btn btn-danger btn-sm" data-name="${data.name}" data-bs-toggle="modal" data-bs-target="#delete_client_`+id+`"><i class="fa fa-fw fa-trash"></i> Delete</button>
							</form>
                            `;
                        }
                    },
                ]
            });

			table.on( 'draw.dt', function () {
                var info = table.page.info();
                var i = 0;
                for (let x = (info.start + 1); x <= info.end; x++) {
                    table.column(0).nodes()[i].innerHTML = x;
                    i++;
                }
            } ).draw();

			$('body').on('click', '.btn-delete', function(event) {
			event.preventDefault();

				var form = $(this).closest('form');

				var data = $(this).data('name');

				Swal.fire({
					title: 'Are you sure?',
					text: data + " will be deleted permanently!" ,
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, delete it!'
				}).then((result) => {
					if (result.isConfirmed) {

						$.ajax({
							type: form.attr('method'),
							url: form.attr('action'),
							data: form.serialize(),
							beforeSend: function(){
								Swal.fire({
									icon: 'info',
									title: 'Please wait....',
								});
							},
							success: function (r) {
								Swal.fire({
									icon: 'success',
									title: r.message
								});
								
								table.ajax.reload();
							},
							error: function (e) {
								Swal.fire({
									icon: 'error',
									title: 'An error occurred.'
								});
							},
						});
					}else{

						Swal.fire({
							icon: 'error',
							title: 'Operation canceled!'
						});
					}
				})


			});

        });

		

	</script>
@endsection