<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group mb-3">
            {{ Form::label('nomor_perkiraan') }}
            {{ Form::text('nomor_perkiraan', $financeBudget->nomor_perkiraan, ['class' => 'form-control' . ($errors->has('nomor_perkiraan') ? ' is-invalid' : ''), 'placeholder' => 'Nomor Perkiraan']) }}
            {!! $errors->first('nomor_perkiraan', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('nama_akun') }}
            {{ Form::text('nama_akun', $financeBudget->nama_akun, ['class' => 'form-control' . ($errors->has('nama_akun') ? ' is-invalid' : ''), 'placeholder' => 'Nama Akun']) }}
            {!! $errors->first('nama_akun', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('saldo') }}
            {{ Form::text('saldo', $financeBudget->saldo, ['class' => 'form-control' . ($errors->has('saldo') ? ' is-invalid' : ''), 'placeholder' => 'Saldo']) }}
            {!! $errors->first('saldo', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('saldo_sisa') }}
            {{ Form::text('saldo_sisa', $financeBudget->saldo_sisa, ['class' => 'form-control' . ($errors->has('saldo_sisa') ? ' is-invalid' : ''), 'placeholder' => 'Saldo Sisa']) }}
            {!! $errors->first('saldo_sisa', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('tahun') }}
            {{ Form::text('tahun', $financeBudget->tahun, ['class' => 'form-control' . ($errors->has('tahun') ? ' is-invalid' : ''), 'placeholder' => 'Tahun']) }}
            {!! $errors->first('tahun', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>