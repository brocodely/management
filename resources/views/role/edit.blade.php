@extends('layouts.main')
@section('title', 'Role Management')
@section('content')
<!--begin::Main-->
	<div class="d-flex flex-column flex-column-fluid">
		<!--begin::toolbar-->
		<div class="toolbar" id="kt_toolbar">
			<div class="container d-flex flex-stack flex-wrap flex-sm-nowrap">
				<!--begin::Info-->
				<div class="d-flex flex-column align-items-start justify-content-center flex-wrap me-1">
					<!--begin::Title-->
					<h3 class="text-dark fw-bolder my-1">Role</h3>
					<!--end::Title-->
					<!--begin::Breadcrumb-->
					<ul class="breadcrumb breadcrumb-line bg-transparent text-muted fw-bold p-0 my-1 fs-7">
						<li class="breadcrumb-item">
							<a href="{{ url('') }}" class="text-muted text-hover-primary">Home</a>
						</li>
						<li class="breadcrumb-item">
							<a href="{{ url('roles') }}" class="text-muted text-hover-primary">Role</a>
						</li>
						<li class="breadcrumb-item text-dark">Edit</li>
					</ul>
					<!--end::Breadcrumb-->
				</div>
				<!--end::Info-->
			</div>
		</div>
		<!--end::toolbar-->
		<!--begin::Content-->
		<div class="content fs-6 d-flex flex-column-fluid mt-5" id="kt_content">
			<!--begin::Container-->
			<div class="container">
				<!--begin::Profile Account-->
				<div class="card" >
					<div class="card-body">
						<form action="{{ url('roles/'. $role->id) }}" method="post" class="form-group">
							@csrf
							@method('patch')
							<div class="form-group mb-5">
								<label for="name">Role</label>
								<input type="text" class="form-control" id="name" aria-describedby="name" placeholder="Masukan Role" name="name" value="{{ $role->name }}">
								<small id="name" class="form-text text-muted">Gunakan nama yang spesifik dan membantu</small>
							</div>
							<button type="submit" class="btn btn-primary">Submit</button>
							<button type="button" class="btn btn-warning" onclick="history.back()">Back</button>
						</form>
					</div>
					</div>
				<!--end::Profile Account-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Content-->
	</div>
<!--end::Main-->
@endsection