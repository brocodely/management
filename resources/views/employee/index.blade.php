@extends('layouts.main')
@section('title', __('Employee'))
@section('content')
    <!--begin::Main-->
	<div class="d-flex flex-column flex-column-fluid">
		<!--begin::toolbar-->
		<div class="toolbar" id="kt_toolbar">
			<div class="container d-flex flex-stack flex-wrap flex-sm-nowrap">
				<!--begin::Info-->
				<div class="d-flex flex-column align-items-start justify-content-center flex-wrap me-1">
					
					<!--begin::Breadcrumb-->
					<ul class="breadcrumb breadcrumb-line bg-transparent text-muted fw-bold p-0 my-1 fs-7">
						<li class="breadcrumb-item">
							<a href="{{ url('') }}" class="text-muted text-hover-primary">Home</a>
						</li>
						<li class="breadcrumb-item text-dark">{{ __('Employee') }}</li>							
					</ul>
					<!--end::Breadcrumb-->
				</div>
				<!--end::Info-->
				
				
			</div>
		</div>
		<!--end::toolbar-->
		<!--begin::Content-->
		<div class="content fs-6 d-flex flex-column-fluid mt-5" id="kt_content">
			<!--begin::Container-->
			<div class="container">
				<!--begin::Profile Account-->
				<div class="card" >
					<div class="card-body">
						<a href="{{ route('employees.create') }}" class="btn btn-primary mb-3" >Tambah {{ __('Employee') }} Baru</a>
						<!--begin::Alert-->
						@if ($message = Session::get('success'))
						<div class="alert alert-success">
							<p>{{ $message }}</p>
						</div>
						@endif
						<!--end::Alert-->
						<div class="table-responsive">
							{{-- <table class="table table-striped table-hover gy-7 gx-10 text-center align-middle">
								<thead>
									<tr class="fw-bold fs-6 text-gray-800 border-bottom-2 border-gray-200">
										<th>No</th>
										<th>Image</th>
										<th>Email</th>
										<th>Position</th>
										<th>Name</th>
										<th>Place Of Birth</th>
										<th>Date Of Birth</th>
										<th>Phone</th>
										<th>Address</th>
										<th>Join Date</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>                                                 
									@foreach ($employees as $employee)
										<tr>
											<td>{{ ++$i }}</td>
											
											<td><img src="{{ URL::asset('storage') }}/{{ $employee->image }}" alt="{{ $employee->image }}" width="100px"></td>
											<td>{{ $employee->email }}</td>
											<td>{{ $employee->position }}</td>
											<td>{{ $employee->name }}</td>
											<td>{{ $employee->place_of_birth }}</td>
											<td>{{ $employee->date_of_birth }}</td>
											<td>{{ $employee->phone }}</td>
											<td>{{ $employee->address }}</td>
											<td>{{ $employee->join_date }}</td>

											<td>
												<form action="{{ route('employees.destroy',$employee->id) }}" method="POST">
													<a class="btn btn-sm btn-primary " href="{{ route('employees.show',$employee->id) }}"><i class="fa fa-fw fa-eye"></i> Show</a>
													<a class="btn btn-sm btn-success" href="{{ route('employees.edit',$employee->id) }}"><i class="fa fa-fw fa-edit"></i> Edit</a>
													@csrf
													@method('DELETE')
													<button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="fa fa-fw fa-trash"></i> Delete</button>
												</form>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table> --}}

							<table id="dataTable" class="table table-row-bordered gy-5">
							</table>
						</div>
					</div>
                </div>
				<!--end::Profile Account-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Content-->
	</div>
	<!--end::Main-->
@endsection

@section('scripts')
	<script type="text/javascript">


		$(document).ready(function(){
            var table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
				ajax: {
					url: "{{ route('employees.index') }}", 
					data: function ( d ) {
						d.test = 1;
					}
				},
                columns: 
				[
					{ 
						title: 'No',
						class: 'text-center',
                        data: 'id',
                        name: 'id'
                    },
					{
                        title: 'Image',
                        class: 'text-center',
                        data: 'image',
                        render: function(image, display, data) {
						    return `
                                <td><img src="{{ URL::asset('storage') }}/${image}" alt="" width="100px"></td>
                            `;
                        }
                    },
					{ 
						title: 'Email',
						class: 'text-center',
                        data: 'email',
                        name: 'email'
                    },
					{ 
						title: 'Position',
						class: 'text-center',
                        data: 'position',
                        name: 'position'
                    },
					{ 
						title: 'Nama',
						class: 'text-center',
                        data: 'name',
                        name: 'name'
                    },
					{ 
						title: 'Tempat Lahir',
						class: 'text-center',
                        data: 'place_of_birth',
                        name: 'place_of_birth'
                    },
					{ 
						title: 'Tanggal Lahir',
						class: 'text-center',
                        data: 'date_of_birth',
                        name: 'date_of_birth'
                    },
					{ 
						title: 'Telp',
						class: 'text-center',
                        data: 'phone',
                        name: 'phone'
                    },
					{ 
						title: 'Alamat',
						class: 'text-center',
                        data: 'address',
                        name: 'address'
                    },
					{ 
						title: 'Tanggal Bergabung',
						class: 'text-center',
                        data: 'join_date',
                        name: 'join_date'
                    },
					{
                        title: 'Action',
                        class: 'text-center',
                        data: 'id',
                        render: function(id, display, data) {
                            return `
                                <form action="{{ route('employees.destroy','') }}`+'/'+id+`" method="POST">
									<a class="btn btn-sm btn-primary " href="{{ route('employees.index') }}`+'/'+id+`"><i class="fa fa-fw fa-eye"></i> Show</a>
									<a class="btn btn-sm btn-success" href="{{ route('employees.index') }}`+'/'+id+ '/edit'+`"><i class="fa fa-fw fa-edit"></i> Edit</a>
									@csrf 
									@method('DELETE')
									<button type="submit" class="btn btn-danger btn-sm btn-delete" data-name="${data.name}"><i class="fa fa-fw fa-trash"></i> Delete</button>
								</form>
                            `;
                        }
                    },
                ]
            });

			table.on( 'draw.dt', function () {
                var info = table.page.info();
                var i = 0;
                for (let x = (info.start + 1); x <= info.end; x++) {
                    table.column(0).nodes()[i].innerHTML = x;
                    i++;
                }
            } ).draw();

			$('body').on('click', '.btn-delete', function(event) {
			event.preventDefault();

				var form = $(this).closest('form');

				var data = $(this).data('name');

				Swal.fire({
					title: 'Are you sure?',
					text: data + " will be deleted permanently!" ,
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, delete it!'
				}).then((result) => {
					if (result.isConfirmed) {

						$.ajax({
							type: form.attr('method'),
							url: form.attr('action'),
							data: form.serialize(),
							beforeSend: function(){
								Swal.fire({
									icon: 'info',
									title: 'Please wait....',
								});
							},
							success: function (r) {
								Swal.fire({
									icon: 'success',
									title: r.message
								});
								
								table.ajax.reload();
							},
							error: function (e) {
								Swal.fire({
									icon: 'error',
									title: 'An error occurred.'
								});
							},
						});
					}else{

						Swal.fire({
							icon: 'error',
							title: 'Operation canceled!'
						});
					}
				})


			});

        });

		

	</script>
@endsection