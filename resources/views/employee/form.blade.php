<div class="box box-info padding-1">
    <div class="box-body">
        {{ Form::hidden('oldImage', $employee->image) }}
        <div class="form-group mb-5">
            {{ Form::label('image', '', ['class' => 'd-block']) }}
            <div class="row m-0">
                <div class="col-md-4 py-3">
                    <div class="d-flex align-items-center">
                        @if ($employee->image)
                            <img src="{{ URL::asset('storage') }}/{{ $employee->image }}" alt="{{ $employee->image }}" id="image-profile" width="100px">
                        @else
                            <img src="" alt="" id="image-profile" width="100px">
                        @endif
                        <div class="d-none fw-bolder text-dark ms-7" id="text-preview">
                            <span class="d-block small">*Recommended ratio: 1:1</span>
                            <span class="d-block small">*Max image size: 2Mb</span>
                            <span class="d-block small">*Type of file must an image</span>
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::file('image', ['class' => 'form-control' . ($errors->has('image') ? ' is-invalid' : ''), 'onchange' => 'previewImage()']) }}
            {!! $errors->first('image', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-5">
            {{ Form::label('email', '', ['class' => 'mb-4']) }}
            {{ Form::text('email', $employee->email, ['class' => 'form-control' . ($errors->has('email') ? ' is-invalid' : ''), 'placeholder' => 'Email']) }}
            {!! $errors->first('email', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-5">
            {{ Form::label('position', '', ['class' => 'mb-4']) }}
            {{ Form::text('position', $employee->position, ['class' => 'form-control' . ($errors->has('position') ? ' is-invalid' : ''), 'placeholder' => 'Position']) }}
            {!! $errors->first('position', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-5">
            {{ Form::label('name', '', ['class' => 'mb-4']) }}
            {{ Form::text('name', $employee->name, ['class' => 'form-control' . ($errors->has('name') ? ' is-invalid' : ''), 'placeholder' => 'Name']) }}
            {!! $errors->first('name', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-5">
            {{ Form::label('place_of_birth', '', ['class' => 'mb-4']) }}
            {{ Form::text('place_of_birth', $employee->place_of_birth, ['class' => 'form-control' . ($errors->has('place_of_birth') ? ' is-invalid' : ''), 'placeholder' => 'Place Of Birth']) }}
            {!! $errors->first('place_of_birth', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-5">
            {{ Form::label('date_of_birth', '', ['class' => 'mb-4']) }}
            {{ Form::text('date_of_birth', $employee->date_of_birth, ['class' => 'form-control' . ($errors->has('date_of_birth') ? ' is-invalid' : ''), 'placeholder' => 'Date Of Birth', 'id' => 'date_of_birth']) }}
            {!! $errors->first('date_of_birth', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-5">
            {{ Form::label('phone', '', ['class' => 'mb-4']) }}
            {{ Form::text('phone', $employee->phone, ['class' => 'form-control' . ($errors->has('phone') ? ' is-invalid' : ''), 'placeholder' => 'Phone']) }}
            {!! $errors->first('phone', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-5">
            {{ Form::label('address', '', ['class' => 'mb-4']) }}
            {{ Form::textarea('address', $employee->address, ['class' => 'form-control' . ($errors->has('address') ? ' is-invalid' : ''), 'placeholder' => 'Address', 'rows' => '5']) }}
            {!! $errors->first('address', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-5">
            {{ Form::label('join_date', '', ['class' => 'mb-4']) }}
            {{ Form::text('join_date', $employee->join_date, ['class' => 'form-control' . ($errors->has('join_date') ? ' is-invalid' : ''), 'placeholder' => 'Join Date', 'id' => 'join_date']) }}
            {!! $errors->first('join_date', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>