<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="row">
            <div class="col-lg-4">
                <div class="form-group mb-3">
                    {{ Form::label('tanggal') }}
                    {{ Form::text('tanggal', date('Y-m-d'), ['class' => 'form-control datepicker' . ($errors->has('tanggal') ? ' is-invalid' : ''), 'placeholder' => 'Tanggal', 'autocomplete' => 'off', 'required' => 'required']) }}
                    {!! $errors->first('tanggal', '<div class="invalid-feedback">:message</div>') !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    {{ Form::label('jenis_transaksi') }}
                    {{ Form::select('jenis_transaksi', ['Pemasukan' => 'Pemasukan', 'Pengeluaran' => 'Pengeluaran'], '', ['class' => 'form-control' . ($errors->has('jenis_transaksi ') ? ' is-invalid' : ''), 'placeholder' => 'Masukan Jenis Transaksi', 'data-control' => 'select2']) }}
                    {!! $errors->first('jenis_transaksi ', '<div class="invalid-feedback">:message</p></div>') !!}
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    {{ Form::label('tipe_transaksi') }}
                    {{ Form::select('tipe_transaksi', ['Bank' => 'Bank', 'Kas' => 'Kas', 'E-money' => 'E-money'], '', ['class' => 'form-control' . ($errors->has('tipe_transaksi ') ? ' is-invalid' : ''), 'placeholder' => 'Masukan Tipe Transaksi', 'data-control' => 'select2']) }}
                    {!! $errors->first('tipe_transaksi ', '<div class="invalid-feedback">:message</p></div>') !!}
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    {{ Form::label('akun_gantung') }}
                    {{ Form::select('akun_gantung', [''], '', ['class' => 'form-control akunGantung' . ($errors->has('akun_gantung ') ? ' is-invalid' : ''), 'placeholder' => 'Akun gantung', 'data-control' => 'select2']) }}
                    {!! $errors->first('akun_gantung ', '<div class="invalid-feedback">:message</p></div>') !!}
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group mb-3">
                    {{ Form::label('keterangan') }}
                    {{ Form::textarea('keterangan', $financeTransaction->keterangan, ['class' => 'form-control' . ($errors->has('keterangan') ? ' is-invalid' : ''), 'placeholder' => 'Keterangan', 'rows' => '3']) }}
                    {!! $errors->first('keterangan', '<div class="invalid-feedback">:message</div>') !!}
                </div>
            </div>
        </div>
        <table class="table w-100 text-center  table-bordered">
            <thead>
            <tr class="bg-light">
                <th class="font-weight-bold">Akun</th>
                <th class="font-weight-bold">Remark</th>
                <th class="font-weight-bold">Debit</th>
                <th class="font-weight-bold">Kredit</th>
                <th></th>
            </tr>
            </thead>
            <tbody class="append">
                <tr class="bg-light">
                    <td class="td1">
                        <div class="form-group">
                            {{ Form::select('nomor_perkiraan', [$financeTransaction->nomor_perkiraan], $financeTransaction->nomor_perkiraan, ['class' => 'form-control getCoa' . ($errors->has('nomor_perkiraan') ? ' is-invalid' : ''), 'placeholder' => 'Masukan Nomor Perkiraan']) }}
                            {!! $errors->first('nomor_perkiraan', '<div class="invalid-feedback">:message</p></div>') !!}
                        </div>
                    </td>
                    <td class="td2">
                        <div class="form-group">
                            <input type="text" name="" class="form-control remark" value="" placeholder="">
                        </div>
                    </td>
                    <td class="td3">
                        <div class="form-group">
                            <input type="text" name="" class=" rupiah form-control debit rupiah" value="" placeholder="">
                        </div>
                    </td>
                    <td class="td4">
                        <div class="form-group">
                            <input type="text" name="" class=" rupiah form-control kredit rupiah" value="" placeholder="">
                        </div>
                    </td>
                    <td>
                        <button type="button" class="btn btn-success btn-add"><i class="fa fa-fw fa-plus p-0 pl-1"></i></button>
                        <br>
                    </td>
                </tr>
            </tbody>
            <tfoot>
            <tr>
                <td class="font-weight-bold" colspan="2">Total</td>
                <td>
                    <input type="text" name="total_debit" class="form-control total-debit text-center rupiah" value="" placeholder="" readonly >
                </td>
                <td>
                    <input type="text" name="total_kredit" class="form-control total-kredit text-center rupiah" value="" placeholder="" readonly >
                </td>
            </tr>
            </tfoot>
            
        </table>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary btn-submit">Submit</button>
    </div>
</div>

@section('scripts')
    <script type="text/javascript">

        $(document).ready(function() {
            getCoa();
            rupiahClass();
            akunGantung($('input[name="tanggal"]').val());
        });

        $('.btn-add').on('click', function () {

            var noPerk = $('.getCoa option:selected')[0].innerText;
            var remark = $('.remark').val();
            var debit = $('.debit').val();
            var kredit = $('.kredit').val();

            $('.append').append(`
                <tr>
                    <td>`
                        + noPerk +`
                        <input type="hidden" name="nomor_perkiraan[]" value="`+noPerk+`">
                    </td>
                    <td class="remark-val">
                        `+remark+`
                        <input type="hidden" name="remark[]" value="`+remark+`">
                    </td>
                    <td class="debit-val">
                        `+debit+`
                        <input type="hidden" name="debit[]" value="`+debit+`">
                    </td>
                    <td class="kredit-val">
                        `+kredit+`
                        <input type="hidden" name="kredit[]" value="`+kredit+`">
                    </td>
                    <td>
                        <button type="button" class="btn btn-danger btn-remove"><i class="fa fa-fw fa-times p-0 pl-1"></i></button>
                    </td>
                </tr>
            `);

            $('.debit').val('');
            $('.kredit').val('');

            getDebitData($('.debit-val'));
            getKreditData($('.kredit-val'));

            rupiahClass();
            
        });

        $('table').on('click', '.btn-remove', function(){
            var hapus = $(this).closest('tr');
            hapus.remove();

            getDebitData($('.debit-val'));
            getKreditData($('.kredit-val'));
        });

        $('.btn-submit').on('click',function(e){
            e.preventDefault();

            var form = $(this).closest('form');

            var debit = $(".total-debit")[0].value;
            var kredit = $(".total-kredit")[0].value;

            if(debit != kredit){
                Swal.fire(
                    'Warning!',
                    'Hasil tidak balance!',
                    'warning'
                );
            }else{
                if(debit == 0 && kredit == 0){
                    Swal.fire(
                        'Warning!',
                        'Data tidak boleh kosong!',
                        'warning'
                    );
                }else{
                    Swal.fire(
                        'Success!',
                        'Hasil balance!',
                        'success'
                    );

                    form.submit();
                    
                }

            }

        });

        $('select[name="akun_gantung"]').on('change', function(){

            var tanggal = $('input[name="tanggal"]').val();

            $.ajax({
                url: `{{route('api.akunGantungValues')}}`,
                data: {tanggal: tanggal},
                dataType: 'json',
                success: function(response){

                    var $newOption = $("<option selected='selected'></option>").val(14).text('320.01.01 - BIAYA GAJI');
                    $('select[name="nomor_perkiraan"]').append($newOption).trigger('change');

                    $('.debit').val(rupiah2(response));

                }
            });

        });

        function getDebitData(debitData){
            var total = 0;
            var data = [];

            for(var i=0;i<debitData.length;i++){
                data[i] = debitData[i].innerText.replace(/,/g, '');
                total += Number(data[i]);
            }
            
            $('.total-debit').val(rupiah2(total.toFixed(2)));
            rupiahClass();
        }

        function getKreditData(kreditData){
            var total = 0;
            var data = [];

            for(var i=0;i<kreditData.length;i++){
                data[i] = kreditData[i].innerText.replace(/,/g, '');
                total += Number(data[i]);
            }
            
            $('.total-kredit').val(rupiah2(total.toFixed(2)));

            rupiahClass();
        }

        function getCoa(){

            $(".getCoa").select2({
                width: '100%',
                ajax: {
                    url: `{{route('api.getCoa')}}`,
                    dataType: 'json'
                }
            });

        }
        
        function akunGantung(tanggal){

            $(".akunGantung").select2({
                width: '100%',
                ajax: {
                    url: `{{route('api.akunGantung')}}`,
                    data: {tanggal: tanggal},
                    dataType: 'json'
                }
            });

        }

        getCoa();
        akunGantung($('input[name="tanggal"]').val());
        rupiahClass();
    </script>
@endsection