<html>
    <head>
        <title>Jurnal Transaksi</title>
        <style type="text/css">

            @page {
                size: 21cm 29.7cm;
                margin: 12px;
                margin-top: 30px;
            }

            html,body{
                font-size: 11px;
                font-family: "Arial";
            }

            h1{
                font-size: 18px;
                margin: 0px;
                padding: 0px;
            }
            h3{
                font-size: 13px;
                margin: 0px;
                padding: 0px;
            }

            table.top{
                border:1px solid #333;
                width: 100%;
            }

            table.top.no-border{
                border:none;
                width: 100%;
            }

            table.top td{
                padding: 0px;
            }
            table.top td.content{
            }

            table.table-content{
                border: 0px solid #333;
                padding: 0px;
                width: 100%;
            }

            table.table-content.bordered{
                border: 1px solid #333;
                padding: 0px;
                width: 100%;
            }

            table.table-content > thead > tr > th{
                padding: 5px;
                border-right: 0px solid #333;
                border-bottom: 0px solid #333;
                margin: 0px;
                vertical-align: top;
                font-weight: bold;
            }

            table.table-content.bordered > thead > tr > th{
                padding: 0px;
                border-right: 1px solid #333;
                border-bottom: 1px solid #333;
                margin: 0px;
                vertical-align: top;
                font-weight: bold;
            }
            table.table-content.bordered > tfoot > tr > th{
                padding: 0px;
                border-right: 1px solid #333;
                border-bottom: 1px solid #333;
                margin: 0px;
                vertical-align: top;
                font-weight: bold;
            }

            table.table-content > thead > tr > th:last-child{
                border-right: none;
            }
            /*table.table-content > thead > tr:last-child > th{
                border-bottom: none;
            }*/


            table.table-content > tbody > tr > td{
                padding: 0px;
                border-right: 0px solid #333;
                border-bottom: 0px solid #333;
                margin: 0px;
                vertical-align: top;
            }

            table.table-content.bordered > tbody > tr > td{
                padding: 0px;
                border-right: 1px solid #333;
                border-bottom: 1px solid #333;
                margin: 0px;
                vertical-align: top;
            }


            .detail-content{
                padding: 0px !important;
            }

            table.table-content > tbody > tr > td:last-child{
                border-right: none;
            }
            table.table-content > tbody > tr:last-child > td{
                border-bottom: none;
            }


            table.table-content > tfoot > tr > th{
                padding: 0px;
                border-right: 0px solid #333;
                border-bottom: 0px solid #333;
                margin: 0px;
                vertical-align: top;
            }

            table.table-content > tfoot > tr > th:last-child{
                border-right: none;
            }
            table.table-content > tfoot > tr:last-child > th{
                border-bottom: none;
            }
            table.table-content > tfoot > tr:first-child > th{
                border-top:  0px solid #333;
            }



            table.table-detail{
                padding: 0px;
                width: 100%;
                border:1px solid #333;
                margin:15px 0px;
            }


            table.table-detail > thead > tr > th{
                border-right: 1px solid #333;
                border-bottom: 1px solid #333;
                margin: 0px;
                vertical-align: top;
            }


            table.table-detail > thead > tr > th:last-child{
                border-right: none;
            }

            table.table-detail > tbody > tr > td,table.table-detail > tbody > tr > th{
                padding: 0px;
                border-right: 1px solid #333;
                border-bottom: 1px solid #333;
                margin: 0px;
                vertical-align: top;
            }

            table.table-detail > tbody > tr > td:last-child,table.table-detail > tbody > tr > th:last-child{
                border-right: none;
            }
            table.table-detail > tbody > tr:last-child > td,table.table-detail > tbody > tr:last-child > th{
                border-bottom: none;
            }

            .text-left{
                text-align: left;
            }
            .text-center{
                text-align: center;
            }

            .text-right{
                text-align: right;
            }

            .label-dokumen{
                padding: 0px;
                border:1px solid #333;
                display: inline-block;
                text-align: center;
            }

            .logo{
                width: 150px;
                max-width: 100%;
            }

            .bg-grey td{
                background: #ddd;
            }

            .table-buku-besar{
                border-top:3px double #333 !important;
                border-bottom:3px double #333 !important;
                margin-bottom:25px;
            }

            .table-buku-besar thead tr:last-child th{
                border-bottom:1px double #333;
            }
            .table-buku-besar tfoot tr:first-child th{
                border-top:1px double #333 !important;
            }

            .table-neraca-saldo{
                margin-bottom:25px;
            }

            .table-neraca-saldo thead tr:first-child th{
                border-top:3px double #333 !important;
            }
            .table-neraca-saldo thead tr th{
            vertical-align: middle !important;
            }
            .table-neraca-saldo thead tr th.right-border{
                border-right:1px solid #333 !important;
            }

            .table-neraca-saldo thead tr th{
                border-bottom:1px solid #333 !important;
            }
            .table-buku-besar tfoot tr:first-child th{
                border-top:1px double #333 !important;
            }
            .table-neraca-saldo tr.row-header td{
                padding-top:0px !important;
                padding-bottom:10px !important;
                font-weight: bold;
            }
            .table-neraca-saldo tr.row-footer td{
                border-top:1px solid #333 !important;
                border-bottom:1px solid #333 !important;
                font-weight: bold;
            }
            .table-neraca-saldo tfoot tr td{
                border-top:1px solid #333 !important;
                border-bottom:3px double #333 !important;
                font-weight: bold;
                padding:0px 5px;
            }

            .table-voucher{
                width:100%;
            }
            .table-voucher-copy{
                width:100%;
                border:3px double #333;
            }
            .table-voucher > thead th{
                text-align:left;
                padding-top: 5px;
                padding-left: 5px;
            }
            .table-signature{
                width:75%;
                margin-left:auto; 
                margin-right:auto;
                margin-bottom:15px;
            }

            .fixed td, .fixed th{
                padding:2px !important;
            }
            table.fixed {table-layout:fixed; width:100%;word-break:break-all;}/*Setting the table width is important!*/
            table.fixed td {overflow:hidden;word-wrap:break-word; padding:1px;}/*Hide text outside the cell.*/
            table.fixed td:nth-of-type(1) {width:200px;}/*Setting the width of column 1.*/
            table.fixed td:nth-of-type(2) {width:300px;}/*Setting the width of column 2.*/
            table.fixed td:nth-of-type(3) {width:400px;}/*Setting the width of column 3.*/
        </style>
    </head>
    <body>
    <table width="100%">
        <thead>
            <tr style="margin-top:0px">
                <td colspan="2">
                    <h1 style="text-align:center; font-size:18px;margin-right:100px">Brocodely IT Solutions</h1>
                    <p></p>
                    <p style="text-align:center; font-size:15px; margin:0;margin-right:100px">Laporan Jurnal Transaksi</p>
                    <p style="text-align:center; font-size:15px; margin:0;margin-right:100px">{{ $data->tanggal }}</p>
                </td>
            </tr>
        </thead>
    </table>
    <br>
    <table class="table-voucher" cellspacing=0 cellpadding=0>
        <thead>
            <tr>
                <th colspan="3">BROCODELY</th>
            </tr>
            <tr>
                <th width="15%">No. Voucher</th>
                <th width="2%">:</th>
                <th>{{ $data->nomor_voucher }}</th>
            </tr>
            <tr>
                <th>Tanggal</th>
                <th>:</th>
                <th>{{ $data->tanggal }}</th>
            </tr>
            <tr>
                <th>Keterangan</th>
                <th>:</th>
                <th>{{ $data->keterangan }}</th>
            </tr>
            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="3">
                    <table class="table-detail fixed" cellspacing=0 cellpadding=0 style="margin-top:0px">
                        <thead>
                            <tr>
                                <th width="3%">No.</th>
                                <th>Akun</th>
                                <th width="23%">Remark</th>
                                <th width="15%">Debet</th>
                                <th width="15%">Kredit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($detail as $dtl)
                            @if(!empty($detail[$loop->index]->nomor_perkiraan))
                                <tr>
                                    <td class="text-center">{{ $loop->iteration }}</td>
                                    <td class="text-left">{{ $dtl->nomor_perkiraan }} - {{ $dtl->nama_akun }}</td>
                                    <td class="text-left">{{ $dtl->remark }}</td>
                                    <td>Rp. <span style="float:right">{{ number_format($dtl->debit, 2) }}</span></td>
                                    <td>Rp. <span style="float:right">{{ number_format($dtl->kredit, 2) }}</span></td>
                                </tr>
                            @endif
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td style="text-align:center;border-top:1px solid #333" colspan="3">TOTAL BALANCE</td>
                                <td style="border:1px solid #333">Rp. <span style="float:right">{{ number_format($data->total_debit, 2) }}</span></td>
                                <td style="border:1px solid #333">Rp. <span style="float:right">{{ number_format($data->total_kredit, 2) }}</span></td>
                            </tr>
                        </tfoot>
                    </table>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3" class="text-center">
                    <br>
                    <table class="table-signature" cellspacing=0 cellpadding=0>
                        <tbody>
                            <tr>
                                <td width="33.3%" style="text-align: center;">Pembuat</td>
                                <td></td>
                                <td width="33.3%" style="text-align: center;">Mengetahui</td>
                            </tr>
                            <tr>
                                <td style="text-align: center;"><br><br><br><br><br><br>{{ $data->pembuat }}</td>
                                <td><br><br><br><br><br><br></td>
                                <td style="text-align: center;"><br><br><br><br><br><br>{{ 'GANDEN DYNASTINA ICHSAN' }}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tfoot>
    </table>

    </body>
</html>