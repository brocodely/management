@extends('layouts.main')
@section('title', __('Product'))
@section('content')
<!--begin::Main-->
	<div class="d-flex flex-column flex-column-fluid">
		<!--begin::toolbar-->
		<div class="toolbar" id="kt_toolbar">
			<div class="container d-flex flex-stack flex-wrap flex-sm-nowrap">
				<!--begin::Info-->
				<div class="d-flex flex-column align-items-start justify-content-center flex-wrap me-1">
					
					<!--begin::Breadcrumb-->
					<ul class="breadcrumb breadcrumb-line bg-transparent text-muted fw-bold p-0 my-1 fs-7">
						<li class="breadcrumb-item">
							<a href="{{ url('') }}" class="text-muted text-hover-primary">Home</a>
						</li>
						<li class="breadcrumb-item text-dark">{{ __('Product') }}</li>							
					</ul>
					<!--end::Breadcrumb-->
				</div>
				<!--end::Info-->
				
				
			</div>
		</div>
		<!--end::toolbar-->
		<!--begin::Content-->
		<div class="content fs-6 d-flex flex-column-fluid mt-5" id="kt_content">
			<!--begin::Container-->
			<div class="container">
				<!--begin::Profile Account-->
                <div class="card">
                    <div class="card-header align-items-center">
                            <span class="card-title">Show Product</span>
                            <a class="btn btn-primary" href="{{ route('products.index') }}"> Back</a>
                    </div>

                    <div class="card-body">
                        
                        <table class="table border px-5  table-rounded table-flush align-middle">
                            <tbody>
                                <tr class="py-5 border-bottom border-gray-300 fs-6">
                                    <td class="fw-bolder text-capitalize">client</td>
                                    <td class="fw-bolder">:</td>
                                    <td>{{ $product->client->name }}</td>
                                </tr>
                                <tr class="py-5 border-bottom border-gray-300 fs-6">
                                    <td class="fw-bolder text-capitalize">image</td>
                                    <td class="fw-bolder">:</td>
                                    <td><img src="{{ URL::asset('storage') }}/{{ $product->image }}" alt="{{ $product->image }}" width="100px" class="rounded"></td>

                                </tr>
                                <tr class="py-5 border-bottom border-gray-300 fs-6">
                                    <td class="fw-bolder text-capitalize">nama</td>
                                    <td class="fw-bolder">:</td>
                                    <td class="text-uppercase">{{ $product->name }}</td>
                                </tr>
                                <tr class="py-5 border-bottom border-gray-300 fs-6">
                                    <td class="fw-bolder text-capitalize" id="desc_label">description</td> <!-- w-200px -->
                                    <td class="fw-bolder" id="desc_colon">:</td> <!-- w-25px -->
                                    <td class="text-uppercase">
                                        @if (strlen($product->description) > 50)

                                        <!-- JGN DIBUKA DI SMALL DEVICE xD ------------------------------------------------------------>
                                            <div class="d-flex align-items-center justify-content-between">
                                                <span id="collapse_desc">{{ Str::limit($product->description, 20) }}</span>
                                                <span id="expand_desc" class="d-none w-700px">{{ $product->description }}</span>
                                                <button class="btn btn-sm bg-light-primary border border-hover border-primary" id="expand_toggle_desc">Expand</button>
                                                <button class="btn btn-sm bg-light-danger border border-hover border-danger active" id="collapse_toggle_desc">Collapse</button>
                                            </div>
                                        <!-- JGN DIBUKA DI SMALL DEVICE xD ------------------------------------------------------------>
                                            
                                        @else
                                            <span>{{ $product->description }}</span>
                                        @endif
                                    </td>
                                </tr>
                                <tr class="py-5 border-bottom border-gray-300 fs-6">
                                    <td class="fw-bolder text-capitalize">order date</td>
                                    <td class="fw-bolder">:</td>
                                    <td class="text-uppercase">{{ $product->order_date }}</td>
                                </tr>
                                <tr class="py-5 border-bottom border-gray-300 fs-6">
                                    <td class="fw-bolder text-capitalize">order finish date</td>
                                    <td class="fw-bolder">:</td>
                                    <td class="text-uppercase">{{ $product->order_finish_date }}</td>
                                </tr>
                                <tr class="py-5 border-bottom border-gray-300 fs-6">
                                    <td class="fw-bolder text-capitalize">status</td>
                                    <td class="fw-bolder">:</td>
                                    @if ($product->status == "P")
                                        <td class="text-uppercase bg-light-primary text-primary fw-bolder shadow-sm bg-progress rounded"><span class="bullet bullet-dot bg-primary h-10px w-10px mx-5"></span>on progress</td>
                                    @endif
                                    @if ($product->status == "D")
                                        <td class="text-uppercase bg-light-success text-success fw-bolder d-flex shadow-sm align-items-center rounded"><i class="bi bi-check2-all mx-5 fs-2 text-success"></i>done</td>
                                    @endif
                                    @if ($product->status == "M")
                                        <td class="text-uppercase bg-light-warning text-warning fw-bolder shadow-sm bg-progress rounded"><span class="bullet bullet-dot bg-warning h-10px w-10px mx-5"></span>maintenance</td>
                                    @endif
                                    @if ($product->status == "R")
                                        <td class="text-uppercase bg-light-danger text-danger fw-bolder shadow-sm bg-progress rounded d-flex align-items-center"><i class="fas fa-times fs-2 mx-5 text-danger"></i>rejected</td>
                                    @endif
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            <!--end::Profile Account-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Content-->
	</div>
	<!--end::Main-->
@endsection
