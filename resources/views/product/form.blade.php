<div class="box box-info padding-1">
    <div class="box-body">
        {{ Form::hidden('oldImage', $product->image) }}
        <div class="form-group mb-5">
            {{ Form::label('client', '', ['class' => 'mb-4']) }}
            <select name="client_id" id="client" class="form-select form-select-solid {{ $errors->has('client_id') ? ' is-invalid' : '' }}" data-control="select2" data-placeholder="Select an option" data-hide-search="true" data-allow-clear="true">
                <option value=""></option>
                @if ($product->client_id)
                    <option value="{{ $product->client_id }}" selected>{{ $product->client->name }}</option>
                @endif
                @foreach ($clients as $client)
                    <option value="{{ $client->id }}">{{ $client->name }}</option>
                @endforeach
            </select>
            {!! $errors->first('client_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-5">
            {{ Form::label('image', '', ['class' => 'd-block']) }}
            <div class="row m-0">
                <div class="col-md-4 py-3">
                    <div class="d-flex align-items-center">
                        @if ($product->image)
                            <img src="{{ URL::asset('storage') }}/{{ $product->image }}" alt="{{ $product->image }}" id="image-profile" width="100px">
                        @else
                            <img src="" alt="" id="image-profile" width="100px">
                        @endif
                        <div class="d-none fw-bolder text-dark ms-7" id="text-preview">
                            <span class="d-block small">*Recommended ratio: 1:1</span>
                            <span class="d-block small">*Max image size: 2Mb</span>
                            <span class="d-block small">*Type of file must an image</span>
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::file('image', ['class' => 'form-control' . ($errors->has('image') ? ' is-invalid' : ''), 'onchange' => 'previewImage()']) }}
            {!! $errors->first('image', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-5">
            {{ Form::label('name', '', ['class' => 'mb-4']) }}
            {{ Form::text('name', $product->name, ['class' => 'form-control' . ($errors->has('name') ? ' is-invalid' : ''), 'placeholder' => 'Name']) }}
            {!! $errors->first('name', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-5">
            {{ Form::label('description', '', ['class' => 'mb-4']) }}
            {{ Form::textarea('description', $product->description, ['class' => 'form-control' . ($errors->has('description') ? ' is-invalid' : ''), 'placeholder' => 'Description', 'rows' => '5']) }}
            {!! $errors->first('description', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-5">
            {{ Form::label('order_date', '', ['class' => 'mb-4']) }}
            {{ Form::text('order_date', $product->order_date, ['class' => 'form-control' . ($errors->has('order_date') ? ' is-invalid' : ''), 'placeholder' => 'Order Date']) }}
            {!! $errors->first('order_date', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-5">
            {{ Form::label('order_finish_date', '', ['class' => 'mb-4']) }}
            {{ Form::text('order_finish_date', $product->order_finish_date, ['class' => 'form-control' . ($errors->has('order_finish_date') ? ' is-invalid' : ''), 'placeholder' => 'Order Finish Date']) }}
            {!! $errors->first('order_finish_date', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-5">
            {{ Form::label('status', '', ['class' => 'mb-4']) }}
            {{ Form::select('status', [($product->status) ? $product->status : '' => '', 'P' => 'On Progress', 'D' => 'Done', 'M' => 'Maintenance', 'R' => 'Rejected'], '', ['class' => 'form-select form-select-solid' . ($errors->has('status') ? ' is-invalid' : ''), 'data-control' => 'select2', 'data-placeholder' => 'Select an option', 'data-allow-clear' => 'true', 'data-hide-search' => 'true']) }}
            {!! $errors->first('status', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>