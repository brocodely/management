@extends('layouts.main')
@section('title', __('Product'))
@section('content')
    <!--begin::Main-->
	<div class="d-flex flex-column flex-column-fluid">
		<!--begin::toolbar-->
		<div class="toolbar" id="kt_toolbar">
			<div class="container d-flex flex-stack flex-wrap flex-sm-nowrap">
				<!--begin::Info-->
				<div class="d-flex flex-column align-items-start justify-content-center flex-wrap me-1">
					
					<!--begin::Breadcrumb-->
					<ul class="breadcrumb breadcrumb-line bg-transparent text-muted fw-bold p-0 my-1 fs-7">
						<li class="breadcrumb-item">
							<a href="{{ url('') }}" class="text-muted text-hover-primary">Home</a>
						</li>
						<li class="breadcrumb-item text-dark">{{ __('Product') }}</li>							
					</ul>
					<!--end::Breadcrumb-->
				</div>
				<!--end::Info-->
				
				
			</div>
		</div>
		<!--end::toolbar-->
		<!--begin::Content-->
		<div class="content fs-6 d-flex flex-column-fluid mt-5" id="kt_content">
			<!--begin::Container-->
			<div class="container">
				<!--begin::Profile Account-->
				<div class="card" >
					<div class="card-body">
						<a href="{{ route('products.create') }}" class="btn btn-primary mb-3" >Tambah {{ __('Product') }} Baru</a>
						<!--begin::Alert-->                                        
						@if ($message = Session::get('success'))
							<div class="alert alert-success">
								<p>{{ $message }}</p>
							</div>
						@endif
						<!--end::Alert-->
						<div class="table-responsive">
							{{-- <table class="table table-striped table-hover gy-7 gs-10 text-center align-middle">
								<thead>
									<tr class="fw-bold fs-6 text-gray-800 border-bottom-2 border-gray-200">
										<th>No</th>
										
										<th>Client</th>
										<th>Image</th>
										<th>Name</th>
										<th>Description</th>
										<th>Order Date</th>
										<th>Order Finish Date</th>
										<th>Status</th>

										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>                                                 
                                    @foreach ($products as $product)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            
											<td>{{ $product->client->name }}</td>
											<td><img src="{{ URL::asset('storage') }}/{{ $product->image }}" alt="{{ $product->image }}" width="100px"></td>
											<td>{{ $product->name }}</td>
											<td>{{ Str::limit($product->description, 20) }}</td>
											<td>{{ $product->order_date }}</td>
											<td>{{ $product->order_finish_date }}</td>
											<td>{!! ($product->status == "P") ? '<span class="bullet bullet-dot bg-primary h-15px w-15px"></span>' : (($product->status == "D") ? '<i class="bi bi-check2-all fs-2 text-success"></i>' : (($product->status == "M") ? '<span class="bullet bullet-dot bg-warning h-15px w-15px"></span>' : (($product->status == "R") ? '<i class="fas fa-times fs-2 text-danger"></i>' : ''))) !!}</td>

                                            <td>
                                                <form action="{{ route('products.destroy',$product->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('products.show',$product->id) }}"><i class="fa fa-fw fa-eye"></i> Show</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('products.edit',$product->id) }}"><i class="fa fa-fw fa-edit"></i> Edit</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="fa fa-fw fa-trash"></i> Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
								</tbody>
							</table> --}}

							<table id="dataTable" class="table table-row-bordered gy-5">
							</table>
						</div>
					</div>
                </div>
				<!--end::Profile Account-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Content-->
	</div>
	<!--end::Main-->
@endsection

@section('scripts')
	<script type="text/javascript">


		$(document).ready(function(){
            var table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
				ajax: {
					url: "{{ route('products.index') }}", 
					data: function ( d ) {
						d.test = 1;
					}
				},
                columns: 
				[
					{ 
						title: 'No',
						class: 'text-center',
                        data: 'id',
                        name: 'id'
                    },
					{
                        title: 'Image',
                        class: 'text-center',
                        data: 'image',
                        render: function(image, display, data) {
						    return `
                                <td><img src="{{ URL::asset('storage') }}/${image}" alt="" width="100px"></td>
                            `;
                        }
                    },
					{ 
						title: 'Nama Produk',
						class: 'text-center',
                        data: 'name',
                        name: 'name'
                    },
					{
                        title: 'Deskripsi',
                        class: 'text-center',
                        data: 'description',
                        render: function(description, display, data) {
						    return `
                                <td>{{ Str::limit(` + description + `, 20) }}</td>
                            `;
                        }
                    },
					{ 
						title: 'Tanggal Order',
						class: 'text-center',
                        data: 'order_date',
                        name: 'order_date'
                    },
					{ 
						title: 'Tanggal Selesai Order',
						class: 'text-center',
                        data: 'order_finish_date',
                        name: 'order_finish_date'
                    },
					{
                        title: 'Status',
                        class: 'text-center',
                        data: 'status',
                        render: function(status, display, data) {
						    return `
								<td>{!! (` + status + ` == "P") ? '<span class="bullet bullet-dot bg-primary h-15px w-15px"></span>' : ((` + status + ` == "D") ? '<i class="bi bi-check2-all fs-2 text-success"></i>' : ((` + status + ` == "M") ? '<span class="bullet bullet-dot bg-warning h-15px w-15px"></span>' : ((` + status + ` == "R") ? '<i class="fas fa-times fs-2 text-danger"></i>' : ''))) !!}</td>
                            `;
                        }
                    },
					{
                        title: 'Action',
                        class: 'text-center',
                        data: 'id',
                        render: function(id, display, data) {
                            return `
                                <form action="{{ route('products.destroy','') }}`+'/'+id+`" method="POST">
									<a class="btn btn-sm btn-primary " href="{{ route('products.index') }}`+'/'+id+`"><i class="fa fa-fw fa-eye"></i> Show</a>
									<a class="btn btn-sm btn-success" href="{{ route('products.index') }}`+'/'+id+ '/edit'+`"><i class="fa fa-fw fa-edit"></i> Edit</a>
									@csrf 
									@method('DELETE')
									<button type="submit" class="btn btn-danger btn-sm btn-delete" data-name="${data.name}"><i class="fa fa-fw fa-trash"></i> Delete</button>
								</form>
                            `;
                        }
                    },
                ]
            });

			table.on( 'draw.dt', function () {
                var info = table.page.info();
                var i = 0;
                for (let x = (info.start + 1); x <= info.end; x++) {
                    table.column(0).nodes()[i].innerHTML = x;
                    i++;
                }
            } ).draw();

			$('body').on('click', '.btn-delete', function(event) {
			event.preventDefault();

				var form = $(this).closest('form');

				var data = $(this).data('name');

				Swal.fire({
					title: 'Are you sure?',
					text: data + " will be deleted permanently!" ,
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, delete it!'
				}).then((result) => {
					if (result.isConfirmed) {

						$.ajax({
							type: form.attr('method'),
							url: form.attr('action'),
							data: form.serialize(),
							beforeSend: function(){
								Swal.fire({
									icon: 'info',
									title: 'Please wait....',
								});
							},
							success: function (r) {
								Swal.fire({
									icon: 'success',
									title: r.message
								});
								
								table.ajax.reload();
							},
							error: function (e) {
								Swal.fire({
									icon: 'error',
									title: 'An error occurred.'
								});
							},
						});
					}else{

						Swal.fire({
							icon: 'error',
							title: 'Operation canceled!'
						});
					}
				})


			});

        });

		

	</script>
@endsection