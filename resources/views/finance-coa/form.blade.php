<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group mb-3">
            {{ Form::label('nomor_perkiraan') }}
            {{ Form::text('nomor_perkiraan', $financeCoa->nomor_perkiraan, ['class' => 'form-control' . ($errors->has('nomor_perkiraan') ? ' is-invalid' : ''), 'placeholder' => 'Nomor Perkiraan']) }}
            {!! $errors->first('nomor_perkiraan', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('nama_akun') }}
            {{ Form::text('nama_akun', $financeCoa->nama_akun, ['class' => 'form-control' . ($errors->has('nama_akun') ? ' is-invalid' : ''), 'placeholder' => 'Nama Akun']) }}
            {!! $errors->first('nama_akun', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('header') }}
            {{ Form::text('header', $financeCoa->header, ['class' => 'form-control' . ($errors->has('header') ? ' is-invalid' : ''), 'placeholder' => 'Header']) }}
            {!! $errors->first('header', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('bagian') }}
            {{ Form::text('bagian', $financeCoa->bagian, ['class' => 'form-control' . ($errors->has('bagian') ? ' is-invalid' : ''), 'placeholder' => 'Bagian']) }}
            {!! $errors->first('bagian', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('status') }}
            {{ Form::text('status', $financeCoa->status, ['class' => 'form-control' . ($errors->has('status') ? ' is-invalid' : ''), 'placeholder' => 'Status']) }}
            {!! $errors->first('status', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>