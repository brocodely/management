@extends('layouts.main')
@section('title', __('Balance Beginning'))
@section('content')
<!--begin::Main-->
	<div class="d-flex flex-column flex-column-fluid">
		<!--begin::toolbar-->
		<div class="toolbar" id="kt_toolbar">
			<div class="container d-flex flex-stack flex-wrap flex-sm-nowrap">
				<!--begin::Info-->
				<div class="d-flex flex-column align-items-start justify-content-center flex-wrap me-1">
					
					<!--begin::Breadcrumb-->
					<ul class="breadcrumb breadcrumb-line bg-transparent text-muted fw-bold p-0 my-1 fs-7">
						<li class="breadcrumb-item">
							<a href="{{ route('balance-beginnings.index') }}" class="text-muted text-hover-primary">{{ __('Balance Beginning') }}</a>
						</li>
						<li class="breadcrumb-item text-dark">{{ __('Balance Beginning') }}</li>							
					</ul>
					<!--end::Breadcrumb-->
				</div>
				<!--end::Info-->
				
				
			</div>
		</div>
		<!--end::toolbar-->
		<!--begin::Content-->
		<div class="content fs-6 d-flex flex-column-fluid mt-5" id="kt_content">
			<!--begin::Container-->
			<div class="container">
				<!--begin::Profile Account-->
                <div class="card">
                    <div class="card-header">
                        <div class="float-left mt-4">
                            <span class="card-title">Show Balance Beginning</span>
                        </div>
                        <div class="float-right mt-4">
                            <a class="btn btn-primary" href="{{ route('balance-beginnings.index') }}"> Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Nomor Perkiraan:</strong>
                            {{ $balanceBeginning->nomor_perkiraan }}
                        </div>
                        <div class="form-group">
                            <strong>Nama Akun:</strong>
                            {{ $balanceBeginning->nama_akun }}
                        </div>
                        <div class="form-group">
                            <strong>Debit:</strong>
                            {{ $balanceBeginning->debit }}
                        </div>
                        <div class="form-group">
                            <strong>Kredit:</strong>
                            {{ $balanceBeginning->kredit }}
                        </div>
                        <div class="form-group">
                            <strong>Tanggal:</strong>
                            {{ $balanceBeginning->tanggal }}
                        </div>
                        <div class="form-group">
                            <strong>Status:</strong>
                            {{ $balanceBeginning->status }}
                        </div>

                    </div>
                </div>
            <!--end::Profile Account-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Content-->
	</div>
	<!--end::Main-->
@endsection
