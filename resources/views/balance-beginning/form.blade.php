<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group mb-3">
            {{ Form::label('nomor_perkiraan') }}
            {{ Form::text('nomor_perkiraan', $balanceBeginning->nomor_perkiraan, ['class' => 'form-control' . ($errors->has('nomor_perkiraan') ? ' is-invalid' : ''), 'placeholder' => 'Nomor Perkiraan']) }}
            {!! $errors->first('nomor_perkiraan', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('nama_akun') }}
            {{ Form::text('nama_akun', $balanceBeginning->nama_akun, ['class' => 'form-control' . ($errors->has('nama_akun') ? ' is-invalid' : ''), 'placeholder' => 'Nama Akun']) }}
            {!! $errors->first('nama_akun', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('debit') }}
            {{ Form::text('debit', $balanceBeginning->debit, ['class' => 'form-control' . ($errors->has('debit') ? ' is-invalid' : ''), 'placeholder' => 'Debit']) }}
            {!! $errors->first('debit', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('kredit') }}
            {{ Form::text('kredit', $balanceBeginning->kredit, ['class' => 'form-control' . ($errors->has('kredit') ? ' is-invalid' : ''), 'placeholder' => 'Kredit']) }}
            {!! $errors->first('kredit', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('tanggal') }}
            {{ Form::text('tanggal', $balanceBeginning->tanggal, ['class' => 'form-control' . ($errors->has('tanggal') ? ' is-invalid' : ''), 'placeholder' => 'Tanggal']) }}
            {!! $errors->first('tanggal', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('status') }}
            {{ Form::text('status', $balanceBeginning->status, ['class' => 'form-control' . ($errors->has('status') ? ' is-invalid' : ''), 'placeholder' => 'Status']) }}
            {!! $errors->first('status', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>