
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>NERACA</title>
        <style>
            html,body{
                font-size: 11px;
                font-family: "Arial";
            }

            h1{
                font-size: 18px;
                margin: 0px;
                padding: 0px;
            }
            h3{
                font-size: 13px;
                margin: 0px;
                padding: 0px;
            }

            table.top{
                border:1px solid #333;
                width: 100%;
            }

            table.top.no-border{
                border:none;
                width: 100%;
            }

            table.top td{
            }
            table.top td.content{
            }

            table.table-content{
                border: 0px solid #333;
                padding: 0px;
                width: 100%;
            }

            table.table-content.bordered{
                border: 1px solid #333;
                padding: 0px;
                width: 100%;
            }

            table.table-content > thead > tr > th{
                padding: 5px;
                border-right: 0px solid #333;
                border-bottom: 0px solid #333;
                margin: 0px;
                vertical-align: top;
                font-weight: bold;
            }

            table.table-content.bordered > thead > tr > th{
                padding: 5px;
                border-right: 1px solid #333;
                border-bottom: 1px solid #333;
                margin: 0px;
                vertical-align: top;
                font-weight: bold;
            }
            table.table-content.bordered > tfoot > tr > th{
                padding: 5px;
                border-right: 1px solid #333;
                border-bottom: 1px solid #333;
                margin: 0px;
                vertical-align: top;
                font-weight: bold;
            }

            table.table-content > thead > tr > th:last-child{
                border-right: none;
            }
            /*table.table-content > thead > tr:last-child > th{
                border-bottom: none;
            }*/


            table.table-content > tbody > tr > td{
                border-right: 0px solid #333;
                border-bottom: 0px solid #333;
                margin: 0px;
            }

            table.table-content.bordered > tbody > tr > td{
                border-right: 1px solid #333;
                border-bottom: 1px solid #333;
                margin: 0px;
            }


            .detail-content{
                padding: 0px !important;
            }

            table.table-content > tbody > tr > td:last-child{
                border-right: none;
            }
            table.table-content > tbody > tr:last-child > td{
                border-bottom: none;
            }


            table.table-content > tfoot > tr > th{
                padding: 5px;
                border-right: 0px solid #333;
                border-bottom: 0px solid #333;
                margin: 0px;
                vertical-align: top;
            }

            table.table-content > tfoot > tr > th:last-child{
                border-right: none;
            }
            table.table-content > tfoot > tr:last-child > th{
                border-bottom: none;
            }
            table.table-content > tfoot > tr:first-child > th{
                border-top:  0px solid #333;
            }



            table.table-detail{
                padding: 0px;
                width: 100%;
                border:1px solid #333;
                margin:15px 0px;
            }


            table.table-detail > thead > tr > th{
                padding: 5px;
                border-right: 1px solid #333;
                border-bottom: 1px solid #333;
                margin: 0px;
                vertical-align: top;
            }


            table.table-detail > thead > tr > th:last-child{
                border-right: none;
            }

            table.table-detail > tbody > tr > td,table.table-detail > tbody > tr > th{
                border-right: 1px solid #333;
                border-bottom: 1px solid #333;
                margin: 0px;
            }
            
            table td p{
                padding: 0px;
                margin: 6px;
            }

            table.table-detail > tbody > tr > td:last-child,table.table-detail > tbody > tr > th:last-child{
                border-right: none;
            }
            table.table-detail > tbody > tr:last-child > td,table.table-detail > tbody > tr:last-child > th{
                border-bottom: none;
            }

            .text-left{
                text-align: left;
            }
            .text-center{
                text-align: center;
            }

            .text-right{
                text-align: right;
            }

            .label-dokumen{
                padding: 15px;
                border:1px solid #333;
                display: inline-block;
                text-align: center;
            }

            .logo{
                width: 150px;
                max-width: 100%;
            }

            .bg-grey td{
                background: #ddd;
            }

            .table-buku-besar{
                border-top:3px double #333 !important;
                border-bottom:3px double #333 !important;
                margin-bottom:25px;
            }

            .table-buku-besar thead tr:last-child th{
                border-bottom:1px double #333;
            }
            .table-buku-besar tfoot tr:first-child th{
                border-top:1px double #333 !important;
            }

            .table-neraca-saldo{
                margin-bottom:25px;
            }

            .table-neraca-saldo thead tr:first-child th{
                border-top:3px double #333 !important;
            }
            .table-neraca-saldo thead tr th{
            vertical-align: middle !important;
            }
            .table-neraca-saldo thead tr th.right-border{
                border-right:1px solid #333 !important;
            }

            .table-neraca-saldo thead tr th{
                border-bottom:1px solid #333 !important;
            }
            .table-buku-besar tfoot tr:first-child th{
                border-top:1px double #333 !important;
            }
            .table-neraca-saldo tr.row-header td{
                font-weight: bold;
            }
            .table-neraca-saldo tr.row-footer td{
                font-weight: bold;
            }
            .table-neraca-saldo tfoot tr td{
                border-bottom:3px double #333 !important;
                font-weight: bold;
            }

            .table-voucher{
                width:100%;
                border:3px double #333;
            }
            .table-voucher > thead th{
                text-align:left;
                padding: 5px;
            }
            .table-signature{
                width:75%;
                margin-left:auto; 
                margin-right:auto;
                margin-bottom:15px;
            }
            .page_break { page-break-before: always; }
        </style>
    </head>
    <body>
        <table class="top no-border" cellspacing=0 cellpadding=0 width="100%">
            <thead>
                <tr>
                </tr>
                <tr style="margin-top:0px">
                    <td colspan="2">
                        <h1 style="text-align:center; font-size:20px;margin-right:110px">Brocodely IT Solutions</h1>
                        <p></p>
                        <p style="text-align:center; font-size:15px; margin:0;margin-right:110px">LAPORAN NERACA</p>
                        <p style="text-align:center; font-size:15px; margin:0;margin-right:110px">PERIODE : {{ date('d-m-Y', $periode1) }} - {{ date('d-m-Y', $periode2) }}</p>
                    </td>
                </tr>
            </thead>
            <tbody>
                    <td colspan="3" class="content">
                        <table class="table-content table-neraca-saldo" cellspacing=0 cellpadding=0 width="100%">
                            <tbody>
                                <tr>
                                    <td style="padding:0px;">
                                        <table style="width:100%;margin:0px" cellspacing=0 cellpadding=0>
                                            <tr>
                                                <td style="border-top:3px double #333;border-bottom:1px solid #333;padding:3px 5px;text-align:center;font-weight:bold;" width="50px">No.</td>
                                                <td style="border-top:3px double #333;border-bottom:1px solid #333;padding:3px 5px;text-align:left;font-weight:bold;">Keterangan</td>
                                                <td style="border-top:3px double #333;border-bottom:1px solid #333;padding:3px 5px;text-align:right;font-weight:bold;" >Saldo </td>
                                            </tr>
                                            @php
                                                $totalAsset = 0;
                                            @endphp
                                            @foreach ($data as $key => $dta)
                                                <tr>
                                                    
                                                    <td style="padding:5px; " colspan="2"><b style="margin-top:5px;">{{ $key }}</b></td>
                                                    <td style="padding:5px; "><b style="margin-top:5px;"></b></td>
                                                </tr>
                                                
                                                @foreach ($dta as $key => $dt)
                                                    <tr>
                                                        <td style="padding:5px; " colspan="2"><b style="margin-top:5px;">{{ $key }}</b></td>
                                                        <td style="padding:5px; "><b style="margin-top:5px;"></b></td>
                                                    </tr>
                                                    @php
                                                        $total = 0;
                                                    @endphp
                                                    
                                                    @foreach ($dt as $key => $d)
                                                        @php
                                                            $subTotal = 0;
                                                        @endphp
                                                        @foreach ($d as $s => $val)
                                                            @php
                                                                // ambil dari saldo awal kalo saldo akhir atau saldo sisa nya gaada --}}
                                                                if(!empty($val->saldo_akhir_sisa)){
                                                                    $subTotal += $val->saldo_akhir_sisa;
                                                                }else{
                                                                    $subTotal += $val->saldo_akhir_awal;
                                                                }

                                                            @endphp
                                                        @endforeach
                                                        
                                                        <tr>
                                                            <td style="padding:0px;"><p></p></td>
                                                            <td style="padding:0px;"><p>{{ $key }}</p></td>
                                                            <td style="padding:0px;"><p style="text-align:right">Rp. {{ number_format($subTotal, 2) }}</p></td>
                                                        </tr>
                                                        @php
                                                            $total += $subTotal;
                                                        @endphp
                                                    @endforeach
                                                    
                                                    <tr>
                                                        <td style="padding:3px 5px;">&nbsp;</td>
                                                        <td style="padding:3px 5px;">&nbsp;</td>
                                                        <td style="padding:3px 5px;border-top:1px solid #333"class="text-right">Rp. {{ number_format($total,2) }}</td>
                                                    </tr>
                                                    @php
                                                        $totalAsset += $total;
                                                    @endphp
                                                @endforeach
                                            @endforeach
                                            <tr>
                                                <td style="padding:3px 5px;border-top:3px double #333;border-bottom:3px double #333;text-align:right" colspan="2"><b>JUMLAH ASET</b></td>
                                                <td style="padding:3px 5px;border-top:3px double #333;border-bottom:3px double #333;"class="text-right">Rp. {{ number_format($totalAsset,2) }}</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="page_break">
        <table class="top no-border" cellspacing=0 cellpadding=0 width="100%">
            <thead>
                <tr>
                </tr>
                <tr style="margin-top:0px">
                    <td colspan="2">
                        <h1 style="text-align:center; font-size:20px;margin-right:50px">Brocodely IT Solutions</h1>
                        <p></p>
                        <p style="text-align:center; font-size:15px; margin:0; margin-right:50px">LAPORAN POSISI KEUANGAN</p>
                        <p style="text-align:center; font-size:15px; margin:0; margin-right:50px">PERIODE : {{ date('d-m-Y', $periode1) }} - {{ date('d-m-Y', $periode2) }}</p>
                    </td>
                </tr>
            </thead>
            <tbody>
                    
                    <td colspan="3" class="content">
                        <table class="table-content table-neraca-saldo" cellspacing=0 cellpadding=0 width="100%">
                            <tbody>
                                <tr>
                                    <td style="padding:0px;">
                                        <table style="width:100%;margin:0px" cellspacing=0 cellpadding=0>
                                            <tr>
                                                <td style="border-top:3px double #333;border-bottom:1px solid #333;padding:3px 5px;text-align:center;font-weight:bold;" width="50px">No.</td>
                                                <td style="border-top:3px double #333;border-bottom:1px solid #333;padding:3px 5px;text-align:left;font-weight:bold;">Keterangan</td>
                                                <td style="border-top:3px double #333;border-bottom:1px solid #333;padding:3px 5px;text-align:right;font-weight:bold;" >Saldo </td>
                                            </tr>
                                            @php
                                                $totalLiabilitas = 0;
                                            @endphp
                                            @foreach ($data2 as $key => $dta)
                                                <tr>
                                                    
                                                    <td style="padding:5px; " colspan="2"><b style="margin-top:5px;">{{ $key }}</b></td>
                                                    <td style="padding:5px; "><b style="margin-top:5px;"></b></td>
                                                </tr>
                                                @foreach ($dta as $key => $dt)
                                                    <tr>
                                                        <td style="padding:5px; " colspan="2"><b style="margin-top:5px;">{{ $key }}</b></td>
                                                        <td style="padding:5px; "><b style="margin-top:5px;"></b></td>
                                                    </tr>
                                                    @php
                                                        $total = 0;
                                                    @endphp

                                                    @foreach ($dt as $key => $d)
                                                        @php
                                                            $subTotal = 0;
                                                        @endphp
                                                        @foreach ($d as $s => $val)
                                                            @php
                                                                // ambil dari saldo awal kalo saldo akhir atau saldo sisa nya gaada --}}

                                                                if(!empty($val->saldo_akhir_sisa)){
                                                                    $subTotal += $val->saldo_akhir_sisa;
                                                                }else{
                                                                    $subTotal += $val->saldo_akhir_awal;
                                                                }
                                                            @endphp
                                                        @endforeach
                                                        <tr>
                                                            <td style="padding:0px;"><p></p></td>
                                                            <td style="padding:0px;"><p>{{ $key }}</p></td>
                                                            <td style="padding:0px;"><p style="text-align:right">Rp. {{ number_format($subTotal, 2) }}</p></td>
                                                        </tr>
                                                        @php
                                                            $total += $subTotal;
                                                        @endphp
                                                    @endforeach
                                                    
                                                    
                                                    <tr>
                                                        <td style="padding:3px 5px;">&nbsp;</td>
                                                        <td style="padding:3px 5px;">&nbsp;</td>
                                                        <td style="padding:3px 5px;border-top:1px solid #333"class="text-right">Rp. {{ number_format($total,2) }}</td>
                                                    </tr>
                                                    @php
                                                        $totalLiabilitas += $total;
                                                    @endphp
                                                @endforeach
                                            @endforeach
                                            <tr>
                                                <td style="padding:3px 5px;border-top:3px double #333;border-bottom:3px double #333;text-align:right" colspan="2"><b>JUMLAH PENDAPATAN & BIAYA</b></td>
                                                <td style="padding:3px 5px;border-top:3px double #333;border-bottom:3px double #333;"class="text-right">Rp. {{ number_format($totalLiabilitas,2) }}</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                    </td>
                </tr>
            </tbody>
        </table>
        </div>
    </body>
</html>
