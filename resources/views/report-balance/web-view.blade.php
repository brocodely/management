<table class="top no-border" cellspacing=0 cellpadding=0 >
    <thead>
        <tr style="margin-top:0px">
            <td colspan="3" style="text-align:center;">
                <h1 style="text-align:center; font-size:20px;margin-right:110px">Brocodely IT Solutions</h1>
            </td>
        </tr>
        <tr style="margin-top:0px">
            <td colspan="3" style="text-align:center;">
                <p style="text-align:center; font-size:15px; margin:0;margin-right:110px">LAPORAN NERACA</p>
            </td>
        </tr>
        <tr style="margin-top:0px">
            <td colspan="3" style="text-align:center;">
                <p style="text-align:center; font-size:15px; margin:0;margin-right:110px">PERIODE : {{ date('d-m-Y', $periode1) }} - {{ date('d-m-Y', $periode2) }}</p>
            </td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="padding:3px 5px;text-align:center;font-weight:bold;">No.</td>
            <td style="padding:3px 5px;text-align:left;font-weight:bold;">Keterangan</td>
            <td style="padding:3px 5px;text-align:right;font-weight:bold;" >Saldo </td>
        </tr>
        @php
            $totalAsset = 0;
        @endphp
        @foreach ($data as $key => $dta)
            <tr>
                
                <td style="padding:5px; " colspan="2"><b style="margin-top:5px;">{{ $key }}</b></td>
                <td style="padding:5px; "><b style="margin-top:5px;"></b></td>
            </tr>
            
            @foreach ($dta as $key => $dt)
                <tr>
                    <td style="padding:5px; " colspan="2"><b style="margin-top:5px;">{{ $key }}</b></td>
                    <td style="padding:5px; "><b style="margin-top:5px;"></b></td>
                </tr>
                @php
                    $total = 0;
                @endphp
                @foreach ($dt as $key => $d)
                    @php
                        $subTotal = 0;
                    @endphp
                    @foreach ($d as $s => $val)
                        @php

                            // ambil dari saldo awal kalo saldo akhir atau saldo sisa nya gaada --}}
                            
                            if(!empty($val->saldo_akhir_sisa)){
                                $subTotal += $val->saldo_akhir_sisa;
                            }else{
                                $subTotal += $val->saldo_akhir_awal;
                            }

                        @endphp
                    @endforeach
                    <tr>
                        <td style="padding:0px;"><p></p></td>
                        <td style="padding:0px;"><p>{{ $key }}</p></td>
                        <td style="padding:0px;"><p style="text-align:right">Rp. {{ number_format($subTotal, 2) }}</p></td>
                    </tr>
                    @php
                        $total += $subTotal;
                    @endphp
                @endforeach
                <tr>
                    <td style="padding:3px 5px;">&nbsp;</td>
                    <td style="padding:3px 5px;">&nbsp;</td>
                    <td style="padding:3px 5px;border-top:1px solid #333"class="text-right">Rp. {{ number_format($total,2) }}</td>
                </tr>
                @php
                    $totalAsset += $total;
                @endphp
            @endforeach
        @endforeach
        <tr>
            <td style="padding:3px 5px;text-align:right" colspan="2"><b>JUMLAH ASET</b></td>
            <td style="padding:3px 5px;"class="text-right">Rp. {{ number_format($totalAsset,2) }}</td>
        </tr>
        <tr></tr>
        <tr>
            <td style="padding:3px 5px;text-align:center;font-weight:bold;" >No.</td>
            <td style="padding:3px 5px;text-align:left;font-weight:bold;">Keterangan</td>
            <td style="padding:3px 5px;text-align:right;font-weight:bold;" >Saldo </td>
        </tr>
        @php
            $totalLiabilitas = 0;
        @endphp
        @foreach ($data2 as $key => $dta)
            <tr>
                
                <td style="padding:5px; " colspan="2"><b style="margin-top:5px;">{{ $key }}</b></td>
                <td style="padding:5px; "><b style="margin-top:5px;"></b></td>
            </tr>
            @foreach ($dta as $key => $dt)
                <tr>
                    <td style="padding:5px; " colspan="2"><b style="margin-top:5px;">{{ $key }}</b></td>
                    <td style="padding:5px; "><b style="margin-top:5px;"></b></td>
                </tr>
                @php
                    $total = 0;
                @endphp

                @if ($key == 'ASET BERSIH')
                    @php
                        $subTotal = $totalAssetBersih;
                    @endphp
                    <tr>
                        <td style="padding:0px;"><p></p></td>
                        <td style="padding:0px;"><p>{{ $key }}</p></td>
                        <td style="padding:0px;"><p style="text-align:right">Rp. {{ number_format($subTotal, 2) }}</p></td>
                    </tr>
                    @php
                        $total = $subTotal;
                    @endphp
                @else
                    @foreach ($dt as $key => $d)
                        @php
                            $subTotal = 0;
                        @endphp
                        @foreach ($d as $s => $val)
                            @php
                                // ambil dari saldo awal kalo saldo akhir atau saldo sisa nya gaada --}}

                                if(!empty($val->saldo_akhir_sisa)){
                                    $subTotal += $val->saldo_akhir_sisa;
                                }else{
                                    $subTotal += $val->saldo_akhir_awal;
                                }
                            @endphp
                        @endforeach
                        <tr>
                            <td style="padding:0px;"><p></p></td>
                            <td style="padding:0px;"><p>{{ $key }}</p></td>
                            <td style="padding:0px;"><p style="text-align:right">Rp. {{ number_format($subTotal, 2) }}</p></td>
                        </tr>
                        @php
                            $total += $subTotal;
                        @endphp
                    @endforeach
                @endif

                <tr>
                    <td style="padding:3px 5px;">&nbsp;</td>
                    <td style="padding:3px 5px;">&nbsp;</td>
                    <td style="padding:3px 5px;border-top:1px solid #333"class="text-right">Rp. {{ number_format($total,2) }}</td>
                </tr>
                @php
                    $totalLiabilitas += $total;
                @endphp
            @endforeach
        @endforeach
        <tr>
            <td style="padding:3px 5px;text-align:right" colspan="2"><b>JUMLAH PENDAPATAN DAN BIAYA</b></td>
            <td style="padding:3px 5px;"class="text-right">Rp. {{ number_format($totalLiabilitas,2) }}</td>
        </tr>
        
    </tbody>
</table>