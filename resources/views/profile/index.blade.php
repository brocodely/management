@extends('layouts.main')
@section('title', 'Manage Profile')
@section('content')
	<!--begin::Main-->
	<div class="d-flex flex-column flex-column-fluid">
		<!--begin::toolbar-->
		<div class="toolbar" id="kt_toolbar">
			<div class="container d-flex flex-stack flex-wrap flex-sm-nowrap">
				<!--begin::Info-->
				<div class="d-flex flex-column align-items-start justify-content-center flex-wrap me-1">
					<!--begin::Breadcrumb-->
					<ul class="breadcrumb breadcrumb-line bg-transparent text-muted fw-bold p-0 my-1 fs-7">
						<li class="breadcrumb-item">
							<a href="{{ url('/profile') }}" class="text-muted text-hover-primary">Profile</a>
						</li>
						<li class="breadcrumb-item text-dark">Manage Profile</li>							
					</ul>
					<!--end::Breadcrumb-->
				</div>
				<!--end::Info-->
			</div>
		</div>
		<!--end::toolbar-->
		<!--begin::Content-->
		<div class="content fs-6 d-flex flex-column-fluid mt-5" id="kt_content">
			<!--begin::Container-->
			<div class="container">
				<!--begin::Profile Account-->
				<div class="card" >
					<div class="card-body">
                        <!--begin::Alert-->
                        @if (session('status'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="btn-close fs-6" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                        @elseif(session('error'))
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                {{ session('error') }}
                                <button type="button" class="btn-close fs-6" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                        @endif
                        <!--end::Alert-->
                        <h3 class="fs-5 fw-bold d-none mb-7" id="text-preview">Preview gambar :</h3>
                        <div class="d-lg-flex align-items-center">
                            <div class="symbol symbol-100px">
                                @if ($user->image != 'default.svg')
                                    <a href="{{ public_path('storage/user-profile/') . $user->image }}">
                                        <img alt="Profile" src="{{ URL::asset('storage') }}/{{ $user->image }}" class="mh-lg-150px mh-100px symbol-label" id="image-profile">
                                    </a>
                                    <span class="symbol-badge badge badge-circle bg-danger start-100 top-100 delete-profile" data-bs-toggle="modal" data-bs-target="#modal_delete_profile" role="button">
                                        <span data-bs-toggle="tooltip" data-bs-custom-class="tooltip-dark" data-bs-dismiss="click" data-bs-placement="top" title="Hapus Profile">
                                            <i class="far fa-trash-alt text-white"></i>
                                        </span>
                                    </span>
                                @else
                                    <img alt="Profile" src="{{ URL::asset('assets/media/svg/avatars') }}/{{ $user->image }}" class="mh-lg-150px mh-100px img-thumbnail" id="image-profile">
                                @endif
                            </div>
                            <div class="ms-lg-7 mt-lg-0 mt-5">
                                <h1 class="text-uppercase mb-lg-8">{{ $user->name }}</h1>
                                <h3 class="mt-lg-8">{{ $user->email }}</h3>
                            </div>
                        </div>
                        <div class="separator border-dark my-lg-10 my-5"></div>
                        <h3 class="mb-3">Edit Profile</h3>
                        <form action="{{ url('profile/'. $user->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('patch')
                            <input type="hidden" name="oldImage" value="{{ $user->image }}">
                            <div class="row">
                                <div class="col-lg-6 mb-5">
                                    <div class="form-group">
                                        <label for="name" class="required form-label">Nama</label>
                                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" aria-describedby="name" name="name" value="{{ $user->name }}">
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="image" class="form-label">Gambar</label>
                                        <input type="file" class="form-control @error('image') is-invalid @enderror" id="image" aria-describedby="image" name="image" onchange="previewImage()">
                                        @error('image')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        <small id="image" class="form-text @error('image') text-danger @enderror">Ukuran maksimal gambar 1MB</small>
                                        <p id="image" class="form-text @error('image') text-danger @enderror mt-0">Ratio gambar 1:1 / Square</p>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-sm btn-light-dark mt-3">Update</button>
                        </form>
					</div>
				</div>
				<!--end::Profile Account-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Content-->
	</div>
	<!--end::Main-->
@endsection