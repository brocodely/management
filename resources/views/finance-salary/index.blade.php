@extends('layouts.main')
@section('title', __('Finance Salary'))
@section('content')
    <!--begin::Main-->
	<div class="d-flex flex-column flex-column-fluid">
		<!--begin::toolbar-->
		<div class="toolbar" id="kt_toolbar">
			<div class="container d-flex flex-stack flex-wrap flex-sm-nowrap">
				<!--begin::Info-->
				<div class="d-flex flex-column align-items-start justify-content-center flex-wrap me-1">
					
					<!--begin::Breadcrumb-->
					<ul class="breadcrumb breadcrumb-line bg-transparent text-muted fw-bold p-0 my-1 fs-7">
						<li class="breadcrumb-item">
							<a href="{{ route('finance-salaries.index') }}" class="text-muted text-hover-primary">{{ __('Finance Salary') }}</a>
						</li>
						<li class="breadcrumb-item text-dark">{{ __('Finance Salary') }}</li>							
					</ul>
					<!--end::Breadcrumb-->
				</div>
				<!--end::Info-->
				
				
			</div>
		</div>
		<!--end::toolbar-->
		<!--begin::Content-->
		<div class="content fs-6 d-flex flex-column-fluid mt-5" id="kt_content">
			<!--begin::Container-->
			<div class="container">
				<!--begin::Profile Account-->
				<div class="card" >
					<div class="card-body">
						<!--begin::Alert-->                                        
					
					<!--end::Alert-->
						<div class="table-responsive">
							<a href="{{ route('finance-salaries.create') }}" class="btn btn-primary mb-3" >Tambah {{ __('Finance Salary') }} Baru</a>
							@if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
							<table id="dataTable" class="table table-row-bordered gy-5">
							</table>
						</div>
					</div>
                </div>
				<!--end::Profile Account-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Content-->
	</div>
	<!--end::Main-->
@endsection

@section('scripts')
	<script type="text/javascript">

		$(document).ready(function(){
            var table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
				ajax: {
					url: "{{ route('finance-salaries.index') }}", 
					data: function ( d ) {
						d.test = 1;
					}
				},
                columns: 
				[
					{ 
						title: 'No',
						class: 'text-center',
                        data: 'id',
                        name: 'id'
                    },
					{ 
						title: 'Nama Pegawai',
                        data: 'nama_pegawai',
                        name: 'nama_pegawai'
                    },
					{ 
						title: 'Gaji',
                        data: 'gaji',
                        name: 'gaji',
						render: function(r) {
                            var data = rupiah2(r);
                            return 'Rp. <span style="float:right;">'+ data +'</span>';
                        }
                    },
					{ 
						title: 'Insentif',
                        data: 'insentif',
                        name: 'insentif',
						render: function(r) {
                            var data = rupiah2(r);
                            return 'Rp. <span style="float:right;">'+ data +'</span>';
                        }
                    },
					{ 
						title: 'THP',
                        data: 'insentif',
                        name: 'thp',
						render: function(id, display, data) {
                            var data = rupiah2(Number(data.gaji) + Number(data.insentif));
                            return 'Rp. <span style="float:right;">'+ data +'</span>';
                        }
                    },
					{ 
						title: 'Tanggal Dibayar',
                        data: 'tanggal',
                        name: 'tanggal'
                    },
					{
                        title: 'Action',
                        class: 'text-center',
                        data: 'id',
                        render: function(id, display, data) {
                            return `
                                <form action="{{ route('finance-salaries.index') }}`+'/'+id+`" method="POST">
									<a class="btn btn-sm btn-primary " href="{{ route('finance-salaries.index') }}`+'/'+id+`"><i class="fa fa-fw fa-eye"></i> Show</a>
									<a class="btn btn-sm btn-success" href="{{ route('finance-salaries.index') }}`+'/'+id+ '/edit'+`"><i class="fa fa-fw fa-edit"></i> Edit</a>
									@csrf 
									@method('DELETE')
									<button type="submit" class="btn btn-danger btn-sm btn-delete" data-name="${data.nama_pegawai}"><i class="fa fa-fw fa-trash"></i> Delete</button>
								</form>
                            `;
                        }
                    },
                ],
				order: [1, 'asc']
            });

			table.on( 'draw.dt', function () {
                var info = table.page.info();
                var i = 0;
                for (let x = (info.start + 1); x <= info.end; x++) {
                    table.column(0).nodes()[i].innerHTML = x;
                    i++;
                }
            } ).draw();

			$('body').on('click', '.btn-delete', function(event) {
			event.preventDefault();

				var form = $(this).closest('form');

				var data = $(this).data('name');

				Swal.fire({
					title: 'Are you sure?',
					text: data + " will be deleted permanently!" ,
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, delete it!'
				}).then((result) => {
					if (result.isConfirmed) {

						$.ajax({
							type: form.attr('method'),
							url: form.attr('action'),
							data: form.serialize(),
							beforeSend: function(){
								Swal.fire({
									icon: 'info',
									title: 'Please wait....',
								});
							},
							success: function (r) {
								Swal.fire({
									icon: 'success',
									title: r.message
								});
								
								table.ajax.reload();
							},
							error: function (e) {
								Swal.fire({
									icon: 'error',
									title: 'An error occurred.'
								});
							},
						});
					}else{

						Swal.fire({
							icon: 'error',
							title: 'Operation canceled!'
						});
					}
				})


			});

        });

		

	</script>
@endsection