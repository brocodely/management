<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group mb-3">
            {{ Form::label('employee_id', 'Pegawai') }}
            {{ Form::select('employee_id', [($financeSalary->employee_id) ? $financeSalary->employee_id : '' => ($financeSalary->employee) ? $financeSalary->employee->name : 'Pegawai'], $financeSalary->employee_id, ['class' => 'form-control getEmployee' . ($errors->has('employee_id') ? ' is-invalid' : ''), 'placeholder' => 'Pegawai']) }}
            {!! $errors->first('employee_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('nama_pegawai') }}
            {{ Form::text('nama_pegawai', $financeSalary->nama_pegawai, ['class' => 'form-control' . ($errors->has('nama_pegawai') ? ' is-invalid' : ''), 'placeholder' => 'Nama Pegawai']) }}
            {!! $errors->first('nama_pegawai', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('gaji') }}
            {{ Form::text('gaji', $financeSalary->gaji, ['class' => 'form-control rupiah' . ($errors->has('gaji') ? ' is-invalid' : ''), 'placeholder' => 'Gaji']) }}
            {!! $errors->first('gaji', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('produk_dikerjakan') }}
            {{ Form::number('produk_dikerjakan', $financeSalary->produk_dikerjakan, ['class' => 'form-control rupiah' . ($errors->has('produk_dikerjakan') ? ' is-invalid' : ''), 'placeholder' => 'Produk Dikerjakan']) }}
            {!! $errors->first('produk_dikerjakan', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('insentif') }}
            {{ Form::text('insentif', $financeSalary->insentif, ['class' => 'form-control rupiah' . ($errors->has('insentif') ? ' is-invalid' : ''), 'placeholder' => 'Insentif']) }}
            {!! $errors->first('insentif', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('tanggal') }}
            {{ Form::text('tanggal', $financeSalary->tanggal, ['class' => 'form-control datepicker' . ($errors->has('tanggal') ? ' is-invalid' : ''), 'placeholder' => 'Tanggal']) }}
            {!! $errors->first('tanggal', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            getEmployee();
            rupiahClass();
        });

        rupiahClass();
    </script>

    {{-- event --}}
    <script type="text/javascript">
        $('.getEmployee').on('change', function(){

            var id = $(this).val();
            $.ajax({
                url: `{{route('api.getEmployeeValues')}}`,
                method: 'get',
                data: {id: id},
                success: function(response){
                    $('input[name="nama_pegawai"]').val(response.name);                    
                }
            });

            //gaji
            $.ajax({
                url: `{{route('api.getSalaryValues')}}`,
                method: 'get',
                success: function(response){

                    //rumus gaji
                    var gaji = (response.gaji * 40/100) / response.jumlahPegawai;
                    gaji = gaji.toFixed(2);

                    $('input[name="gaji"]').val(rupiah2(gaji));
                    
                }
            });

        });

        $('input[name="produk_dikerjakan"]').on('keyup', function(){
            $.ajax({
                url: `{{route('api.getInsentif')}}`,
                method: 'get',
                success: function(response){
                    
                    //rumus gaji
                    var produkDikerjakan = $('input[name="produk_dikerjakan"]').val();
                    var insentif = (response.pendapatan * 30/100) / 5 * produkDikerjakan;
                    insentif = insentif.toFixed(2);

                    $('input[name="insentif"]').val(rupiah2(insentif));

                }
            });
        });

    </script>

    {{-- function --}}
    <script type="text/javascript">
        function getEmployee(){
            $(".getEmployee").select2({
                width: '100%',
                ajax: {
                    url: `{{route('api.getEmployee')}}`,
                    dataType: 'json'
                }
            });
        }
    </script>
@endsection