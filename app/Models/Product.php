<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 *
 * @property $id
 * @property $client_id
 * @property $image
 * @property $name
 * @property $description
 * @property $order_date
 * @property $order_finish_date
 * @property $status
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Product extends Model
{
  protected $with = ['client'];

  static $rules = [
    'client_id' => 'required',
    // 'image' => 'required',
    'name' => 'required|min:3|max:50|regex:/^[a-zA-Z\s]+$/',
    'description' => 'required|string|min:5',
    'order_date' => 'required|date_format:Y-m-d|before_or_equal:today',
    'order_finish_date' => 'required|date_format:Y-m-d',
    'status' => 'required',
  ];

  public function client()
  {
    return $this->belongsTo(Client::class);
  }

  protected $perPage = 10;

  /**
   * Attributes that should be mass-assignable.
   *
   * @var array
   */
  protected $fillable = ['client_id', 'image', 'name', 'description', 'order_date', 'order_finish_date', 'status'];
}
