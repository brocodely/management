<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FinanceSalary
 *
 * @property $id
 * @property $employee_id
 * @property $nama_pegawai
 * @property $gaji
 * @property $insentif
 * @property $tanggal
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class FinanceSalary extends Model
{
    
    static $rules = [
		'employee_id' => 'required',
		'nama_pegawai' => 'required',
    ];

    protected $perPage = 10;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['employee_id','nama_pegawai','gaji','insentif','tanggal', 'produk_dikerjakan'];

    public function employee(){
        return $this->belongsTo('App\Models\Employee', 'employee_id', 'id');
    }



}
