<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Employee
 *
 * @property $id
 * @property $image
 * @property $email
 * @property $position
 * @property $name
 * @property $place_of_birth
 * @property $date_of_birth
 * @property $phone
 * @property $address
 * @property $join_date
 * @property $remember_token
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Employee extends Model
{

  static $rules = [
    // 'image' => 'required|image|file|max:2048',
    // 'email' => 'required|email:rfc,dns|unique:employees,email',
    'position' => 'required|regex:/^[a-zA-Z\s]+$/',
    'name' => 'required|min:3|max:50|regex:/^[a-zA-Z\s]+$/',
    'place_of_birth' => 'required|regex:/^[a-zA-Z\s]+$/',
    'date_of_birth' => 'required|date_format:Y-m-d|before_or_equal:-16 years|after_or_equal:-60 years',
    'phone' => 'required|numeric|digits_between:10,12',
    'address' => 'required|string|min:5',
    'join_date' => 'required|date_format:Y-m-d|before_or_equal:today',
  ];

  protected $perPage = 10;

  /**
   * Attributes that should be mass-assignable.
   *
   * @var array
   */
  protected $fillable = ['image', 'email', 'position', 'name', 'place_of_birth', 'date_of_birth', 'phone', 'address', 'join_date'];
}
