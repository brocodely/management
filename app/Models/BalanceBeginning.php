<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BalanceBeginning extends Model
{
    use HasFactory;

    protected $fillable = ['nomor_perkiraan','nama_akun','debit','kredit', 'tanggal', 'status'];
}
