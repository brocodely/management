<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CmsContent
 *
 * @property $id
 * @property $logo
 * @property $headline
 * @property $tagline
 * @property $about_title
 * @property $about_content
 * @property $about_detail_code
 * @property $service_title
 * @property $service_content
 * @property $service_detail_code
 * @property $element_title
 * @property $element_content
 * @property $element_detail_code
 * @property $excellence_title
 * @property $excellence_content
 * @property $excellence_detail_code
 * @property $cta_title
 * @property $cta_content
 * @property $portfolio_title
 * @property $portfolio_content
 * @property $portfolio_detail_code
 * @property $team_title
 * @property $team_content
 * @property $team_detail_code
 * @property $faq_title
 * @property $faq_content
 * @property $faq_detail_code
 * @property $contact_title
 * @property $contact_content
 * @property $contact_detail_code
 * @property $socmed_title
 * @property $socmed_content
 * @property $socmed_detail_code
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class CmsContent extends Model
{
    
    static $rules = [
		'logo' => 'required',
		'headline' => 'required',
		'tagline' => 'required',
		'about_title' => 'required',
		'about_content' => 'required',
		'about_detail_code' => 'required',
		'service_title' => 'required',
		'service_content' => 'required',
		'service_detail_code' => 'required',
		'element_title' => 'required',
		'element_content' => 'required',
		'element_detail_code' => 'required',
		'excellence_title' => 'required',
		'excellence_content' => 'required',
		'excellence_detail_code' => 'required',
		'cta_title' => 'required',
		'cta_content' => 'required',
		'portfolio_title' => 'required',
		'portfolio_content' => 'required',
		'portfolio_detail_code' => 'required',
		'team_title' => 'required',
		'team_content' => 'required',
		'team_detail_code' => 'required',
		'faq_title' => 'required',
		'faq_content' => 'required',
		'faq_detail_code' => 'required',
		'contact_title' => 'required',
		'contact_content' => 'required',
		'contact_detail_code' => 'required',
		'socmed_title' => 'required',
		'socmed_content' => 'required',
		'socmed_detail_code' => 'required',
    ];

    protected $perPage = 10;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['logo','headline','tagline','about_title','about_content','about_detail_code','service_title','service_content','service_detail_code','element_title','element_content','element_detail_code','excellence_title','excellence_content','excellence_detail_code','cta_title','cta_content','portfolio_title','portfolio_content','portfolio_detail_code','team_title','team_content','team_detail_code','faq_title','faq_content','faq_detail_code','contact_title','contact_content','contact_detail_code','socmed_title','socmed_content','socmed_detail_code'];



}
