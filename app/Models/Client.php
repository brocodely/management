<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Client
 *
 * @property $id
 * @property $image
 * @property $email
 * @property $name
 * @property $phone
 * @property $source
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Client extends Model
{

  static $rules = [
    // 'image' => 'required|image|file|max:2048',
    // 'email' => 'required|email:rfc,dns|unique:employees,email',
    'name' => 'required|min:3|max:50|regex:/^[a-zA-Z\s]+$/',
    'phone' => 'required|numeric|digits_between:10,12',
    'source' => 'required',
  ];

  public function product()
  {
    return $this->hasMany(Product::class);
  }

  protected $perPage = 10;

  /**
   * Attributes that should be mass-assignable.
   *
   * @var array
   */
  protected $fillable = ['image', 'email', 'name', 'phone', 'source'];
}
