<?php

namespace App\Http\Controllers;

use App\Models\UserMenu;
use App\Models\UserSubMenu;
use Illuminate\Http\Request;
use App\Models\UserAccessMenu;
use Yajra\DataTables\Facades\DataTables;

class UserMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return DataTables::of(UserMenu::all())->toJson();
        }
        return view('menu.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menu = UserMenu::all();
        return view('menu.create', compact('menu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $req = $request->all();

        $menu = UserMenu::orderBy('order', 'desc')->first();
        $order = $menu->order + 1;

        // assign
        $req['order'] = $order;

        UserMenu::create($req);

        return redirect(route('menus.index'))->with('status', 'Menu baru berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserMenu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(UserMenu $menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserMenu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(UserMenu $menu)
    {
        return view('menu.edit', compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserMenu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserMenu $menu)
    {
        $req = $request->all();

        $menu->update($req);

        return redirect(route('menus.index'))->with('status', 'Menu berhasil diubah!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserMenu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserMenu $menu, Request $request)
    {
        if ($request->ajax()) {

            UserMenu::destroy($menu->id);

            UserSubMenu::where('menu_id', $menu->id)->delete();
            UserAccessMenu::where('menu_id', $menu->id)->delete();
            
            return response()->json([
                'success' => true,
                'code' => 200,
                'message' => 'Menu deleted successfully'
            ], 200);            
        }

        return redirect(route('menus.index'))->with('status', 'Menu berhasil dihapus!');
    }
}
