<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\File;

class UserProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return view('profile.index', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        if ($request->name == null && $request->file('image') == null) {
            return redirect(route('profile.index'))->with('error', 'Data tidak bisa kosong total!');
        }

        $validatedData = $request->validate([
            'image' => 'image|file|max:1024',
            'name' => 'max:100'
        ], [
            'max' => 'Panjang maksimal 100 karakter!',
            'image' => 'Format file tidak benar!',
            'file' => 'File harus sukses terupload!',
            'max' => 'Ukuran file terlalu besar!'
        ]);

        // jika error
        // jalankan => composer require intervention/image

        if ($request->file('image')) {
            // Proses Resize Foto Profile
            $file = $request->file('image');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $img = Image::make($file);
            $img->fit(500);
            
            $path = public_path('storage/user-profile/');

            // jika error
            // jalankan => php artisan storage:link
            if(!Storage::exists('public/user-profile')) {
                
                //creates directory
                Storage::makeDirectory('public/user-profile', 0775, true); 
            
            }
            $img->save($path . $filename);
            // End Proses

            if ($request->oldImage != 'default.svg') {
                Storage::delete('public/'.$request->oldImage);
            }

            $validatedData['image'] = 'user-profile/' . $img->basename;
        }

        if ($request->name == null) {
            $validatedData['name'] = Auth::user()->name;
        } else {
            $validatedData['name'] = $request->name;
        }

        $user::where('id', Auth::user()->id)->update($validatedData);
        return redirect(route('profile.index'))->with('status', 'Profile berhasil diubah!');
    }

    public function resetProfile(Request $request, User $user)
    {
        $user::where('id', Auth::user()->id)->update(['image' => 'default.svg']);
        Storage::delete('public/'.Auth::user()->image);
        return redirect(route('profile.index'))->with('status', 'Profile berhasil diubah!');
    }

    public function changePassword(Request $request)
    {
        $user = Auth::user();
        return view('change-password.index', compact('user'));
    }

    public function changeProccess(Request $request, User $user)
    {
        $validatedData = $request->validate([
            'password' => 'required|current_password',
            'password_baru' => [
                'required',
                'string',
                'min:4',
                'max:20',
                'regex:/[a-z]/',
                'regex:/[0-9]/'
            ],
            'password_confirmation' => 'required|required_with:password_baru|same:password_baru'
        ], [
            'current_password' => 'Password lama anda salah!',
            'required' => 'Data :attribute harus di isi!',
            'min' => 'Panjang :attribute minimal 4 karakter!',
            'max' => 'Panjang :attribute maksimal 20 karakter!',
            'regex' => 'Format :attribute harus mengandung huruf dan angka!',
            'same' => 'Konfirmasi Password tidak cocok dengan password baru!'
        ]);

        $validatedData['password'] = Hash::make($request->password_baru);

        $user::where('id', Auth::user()->id)->update(['password' => $validatedData['password']]);
        return redirect(route('change-password.index'))->with('status', 'Password berhasil diubah!');
    }
}
