<?php

namespace App\Http\Controllers;

use App\Models\BalanceBeginning;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class BalanceBeginningController
 * @package App\Http\Controllers
 */
class BalanceBeginningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            return DataTables::of(BalanceBeginning::all())->toJson();
        }

        return view('balance-beginning.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $balanceBeginning = new BalanceBeginning();
        return view('balance-beginning.create', compact('balanceBeginning'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(BalanceBeginning::$rules);

        $balanceBeginning = BalanceBeginning::create($request->all());

        return redirect()->route('balance-beginnings.index')
            ->with('success', 'BalanceBeginning created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $balanceBeginning = BalanceBeginning::find($id);

        return view('balance-beginning.show', compact('balanceBeginning'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $balanceBeginning = BalanceBeginning::find($id);

        return view('balance-beginning.edit', compact('balanceBeginning'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  BalanceBeginning $balanceBeginning
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BalanceBeginning $balanceBeginning)
    {
        request()->validate(BalanceBeginning::$rules);

        $balanceBeginning->update($request->all());

        return redirect()->route('balance-beginnings.index')
            ->with('success', 'BalanceBeginning updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id, Request $request)
    {

        if ($request->ajax()) {
            
            $balanceBeginning = BalanceBeginning::find($id)->delete();

            return response()->json([
                'success' => true,
                'code' => 200,
                'message' => 'BalanceBeginning deleted successfully'
            ], 200);            
        }

        return redirect()->route('balance-beginnings.index')
            ->with('success', 'BalanceBeginning deleted successfully');
    }
}
