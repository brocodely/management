<?php

namespace App\Http\Controllers;

use App\Models\FinanceCoa;
use Illuminate\Http\Request;
use App\Models\FinanceSalary;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class FinanceSalaryController
 * @package App\Http\Controllers
 */
class FinanceSalaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            return DataTables::of(FinanceSalary::orderBy('nama_pegawai')->get())->toJson();
        }

        return view('finance-salary.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $financeSalary = new FinanceSalary();
        return view('finance-salary.create', compact('financeSalary'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(FinanceSalary::$rules);

        $req = $request->all();
        
        $req['gaji'] = preg_replace("/[^0-9.]/", "", $req['gaji']);
        $req['insentif'] = preg_replace("/[^0-9.]/", "", $req['insentif']);

        $financeSalary = FinanceSalary::create($req);

        return redirect()->route('finance-salaries.index')
            ->with('success', 'FinanceSalary created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $financeSalary = FinanceSalary::find($id);

        return view('finance-salary.show', compact('financeSalary'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $financeSalary = FinanceSalary::find($id);

        return view('finance-salary.edit', compact('financeSalary'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  FinanceSalary $financeSalary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FinanceSalary $financeSalary)
    {
        request()->validate(FinanceSalary::$rules);

        $req = $request->all();

        $req['gaji'] = preg_replace("/[^0-9.]/", "", $req['gaji']);
        $req['insentif'] = preg_replace("/[^0-9.]/", "", $req['insentif']);

        $financeSalary->update($req);

        return redirect()->route('finance-salaries.index')
            ->with('success', 'FinanceSalary updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id, Request $request)
    {

        if ($request->ajax()) {
            
            $financeSalary = FinanceSalary::find($id)->delete();

            return response()->json([
                'success' => true,
                'code' => 200,
                'message' => 'FinanceSalary deleted successfully'
            ], 200);            
        }

        return redirect()->route('finance-salaries.index')
            ->with('success', 'FinanceSalary deleted successfully');
    }
}
