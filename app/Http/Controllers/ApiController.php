<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\User;
use App\Models\Product;
use App\Models\Employee;
use App\Models\CmsContent;
use App\Models\FinanceCoa;
use Illuminate\Http\Request;
use App\Models\FinanceSalary;
use App\Models\UserAccessMenu;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    public function accessProcess(Request $request, UserAccessMenu $userRoleAccess)
    {
        $req = $request->all();

        $role_id = $req['role_id'];
        $menu_id = $req['menu_id'];

        if(UserAccessMenu::where('menu_id', $menu_id)->where('role_id',$role_id)->count() > 0){
            UserAccessMenu::where('menu_id', $menu_id)
            ->where('role_id',$role_id)
            ->delete();
        }else{
            UserAccessMenu::where('menu_id', $menu_id)
            ->where('role_id',$role_id)
            ->create($req);
        }
    }

    public function changeProccess(Request $request, User $user)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required',
            'password_baru' => [
                'required',
                'string',
                'min:4',
                'max:20',
                'regex:/[a-z]/',
                'regex:/[0-9]/'
            ],
            'password_confirmation' => 'required|same:password_baru'
        ], [
            'required' => 'Data :attribute harus di isi!',
            'min' => 'Panjang :attribute minimal 4 karakter!',
            'max' => 'Panjang :attribute maksimal 20 karakter!',
            'regex' => 'Format :attribute harus mengandung huruf dan angka!',
            'same' => 'Konfirmasi Password tidak cocok dengan password baru!'
        ]);

        if ($validator->passes()) {

            return response()->json(['success'=>'Added new records.']);
			
        }

        return $data = response()->json(['error'=>$validator->errors()]);

    }

    public function akunGantung(Request $request)
    {
        try {
            $req = $request->all();
            $tanggal = date('Y-m', strtotime($req['tanggal']));

            $data = FinanceSalary::select(DB::raw("CONCAT('BIAYA GAJI - 320.01.01') AS text"),'id')
            ->where('tanggal', 'LIKE', '%' . $tanggal . '%')
            ->take(1)
            ->get();

            return response()->json(['results' => $data]);
        } catch (\Throwable $th) {
            return response()->json($th->getMessage(), 500);
        }
    }

    public function getCoa(Request $request)
    {
        try {
            $req = $request->all();
            $search = '';
            if (isset($req['term'])) {
                $search = $req['term'];
            }
            $data = FinanceCoa::select(DB::raw("CONCAT(nomor_perkiraan, ' - ', nama_akun) AS text"),'id')
            ->where(function($query) use ($search) {
                $query->orWhere('nomor_perkiraan', 'LIKE', '%' . $search . '%');
                $query->orWhere('nama_akun', 'LIKE', '%' . $search . '%');
            })
            ->where('status', '1')
            ->take(5)
            ->get();
            
            return response()->json(['results' => $data]);
        } catch (\Throwable $th) {
            return response()->json($th->getMessage(), 500);
        }
    }

    public function akunGantungValues(Request $request)
    {
        try {
            $req = $request->all();
            $tanggal = date('Y-m', strtotime($req['tanggal']));

            $gaji = FinanceSalary::where('tanggal', 'LIKE', '%' . $tanggal . '%')->sum('gaji');
            $insentif = FinanceSalary::where('tanggal', 'LIKE', '%' . $tanggal . '%')->sum('insentif');

            $value = $gaji + $insentif;
            
            return $value;
        } catch (\Throwable $th) {
            return response()->json($th->getMessage(), 500);
        }
    }

    public function getEmployee(Request $request)
    {
        try {
            $req = $request->all();
            $search = '';
            if (isset($req['term'])) {
                $search = $req['term'];
            }
            $data = Employee::select(DB::raw("CONCAT(name) AS text"),'id')
            ->where(function($query) use ($search) {
                $query->orWhere('name', 'LIKE', '%' . $search . '%');
            })
            ->take(5)
            ->get();
            
            return response()->json(['results' => $data]);
        } catch (\Throwable $th) {
            return response()->json($th->getMessage(), 500);
        }
    }

    public function getEmployeeValues(Request $request)
    {
        try {
            $id = $request->id;

            $data = Employee::where('id', $id)->first();
            $value = $data;
            
            return $value;
        } catch (\Throwable $th) {
            return response()->json($th->getMessage(), 500);
        }
    }

    public function getSalaryValues(Request $request)
    {
        try {

            $grouping = [];
            $asetLancarKasAkhir = 0;
            $kodeAsetLancarKasAkhir = FinanceCoa::where('bagian','ASET')->get()->pluck('nomor_perkiraan')->toArray();

            $saldoAwalAkun = [];
            foreach ($kodeAsetLancarKasAkhir as $key => $value) {

                $queryasetLancarKasAkhir = "
        
                SELECT
                    * 
                FROM
                    balance_remainings
                    LEFT JOIN finance_coas ON finance_coas.nomor_perkiraan = balance_remainings.nomor_perkiraan 
                WHERE
                    finance_coas.nomor_perkiraan = '". $value ."' 
                    AND ( balance_remainings.tanggal <= '". date('Y-m-d') ."' OR balance_remainings.tanggal IS NULL ) 
                    AND finance_coas.`status` = 1 
                ORDER BY
                    balance_remainings.tanggal ASC,
                    balance_remainings.id ASC
                
                ";

                $hasilasetLancarKasAkhir = collect(DB::select($queryasetLancarKasAkhir));

                if(count($hasilasetLancarKasAkhir) < 1){
                    $saldoAwalAkun[] = $value;
                }

                foreach($hasilasetLancarKasAkhir as $hasil){

                    $grouping[$hasil->nomor_perkiraan] = $hasil;

                }

            }

            foreach ($grouping as $key => $grp) {
                
                $asetLancarKasAkhir += $grp->saldo_akhir ?? 0; 

            }

            $value = [];
            $value['gaji'] = $asetLancarKasAkhir;

            $value['jumlahPegawai'] = Employee::count();
            
            return $value;
        } catch (\Throwable $th) {
            return response()->json($th->getMessage(), 500);
        }
    }
    
    public function getInsentif(Request $request)
    {
        try {

            $grouping = [];
            $asetLancarKasAkhir = 0;
            $kodeAsetLancarKasAkhir = FinanceCoa::where('bagian','ASET')->get()->pluck('nomor_perkiraan')->toArray();

            $saldoAwalAkun = [];
            foreach ($kodeAsetLancarKasAkhir as $key => $value) {

                $queryasetLancarKasAkhir = "
        
                SELECT
                    * 
                FROM
                    balance_remainings
                    LEFT JOIN finance_coas ON finance_coas.nomor_perkiraan = balance_remainings.nomor_perkiraan 
                WHERE
                    finance_coas.nomor_perkiraan = '". $value ."' 
                    AND ( balance_remainings.tanggal <= '". date('Y-m-d') ."' OR balance_remainings.tanggal IS NULL ) 
                    AND finance_coas.`status` = 1 
                ORDER BY
                    balance_remainings.tanggal ASC,
                    balance_remainings.id ASC
                
                ";

                $hasilasetLancarKasAkhir = collect(DB::select($queryasetLancarKasAkhir));

                if(count($hasilasetLancarKasAkhir) < 1){
                    $saldoAwalAkun[] = $value;
                }

                foreach($hasilasetLancarKasAkhir as $hasil){

                    $grouping[$hasil->nomor_perkiraan] = $hasil;

                }

            }

            foreach ($grouping as $key => $grp) {
                
                $asetLancarKasAkhir += $grp->saldo_akhir ?? 0; 

            }

            $value = [];
            $value['pendapatan'] = $asetLancarKasAkhir;

            $value['jumlahProduk'] = Product::count();
            
            return $value;
        } catch (\Throwable $th) {
            return response()->json($th->getMessage(), 500);
        }
    }

    public function getCms(Request $request){
        try {
            $cms = CmsContent::first();

            return $cms;
        } catch (\Throwable $th) {
            return response()->json($th->getMessage(), 500);
        }
    }
    
}
