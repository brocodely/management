<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class EmployeeController
 * @package App\Http\Controllers
 */
class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return DataTables::of(Employee::all())->toJson();
        }

        $employees = Employee::paginate();


        return view('employee.index', compact('employees'))
            ->with('i', (request()->input('page', 1) - 1) * $employees->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employee = new Employee();
        return view('employee.create', compact('employee'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = Employee::$rules;
        $rules['image'] = 'required|image|file|max:2048';
        $rules['email'] = 'required|email:rfc,dns|unique:employees,email';

        request()->validate($rules);

        $validatedData['image'] = $request->file('image')->store('user-images');
        $validatedData['email'] = $request->email;
        $validatedData['position'] = $request->position;
        $validatedData['name'] = $request->name;
        $validatedData['place_of_birth'] = $request->place_of_birth;
        $validatedData['date_of_birth'] = $request->date_of_birth;
        $validatedData['phone'] = $request->phone;
        $validatedData['address'] = $request->address;
        $validatedData['join_date'] = $request->join_date;
        $employee = Employee::create($validatedData);

        return redirect()->route('employees.index')
            ->with('success', 'Employee created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Employee::find($id);

        return view('employee.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::find($id);

        return view('employee.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $rules = Employee::$rules;
        if ($request->email != $employee->email) {
            $rules['email'] = 'required|email:rfc,dns|unique:employees,email';
        } else {
            $rules['email'] = 'required|email:rfc,dns';
        }

        if ($request->file('image')) {
            $rules['image'] = 'required|image|file|max:2048';
        }

        request()->validate($rules);

        if ($request->file('image')) {
            if ($request->oldImage) {
                Storage::delete($request->oldImage);
            }
            $validatedData['image'] = $request->file('image')->store('user-images');
        }

        $validatedData['email'] = $request->email;
        $validatedData['position'] = $request->position;
        $validatedData['name'] = $request->name;
        $validatedData['place_of_birth'] = $request->place_of_birth;
        $validatedData['date_of_birth'] = $request->date_of_birth;
        $validatedData['phone'] = $request->phone;
        $validatedData['address'] = $request->address;
        $validatedData['join_date'] = $request->join_date;

        $employee->update($validatedData);

        return redirect()->route('employees.index')
            ->with('success', 'Employee updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $employee = Employee::find($id);
        Storage::delete($employee->image);
        $employee->delete();

        return redirect()->route('employees.index')
            ->with('success', 'Employee deleted successfully');
    }
}
