<?php

namespace App\Http\Controllers;

use App\Models\CmsContent;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class CmsContentController
 * @package App\Http\Controllers
 */
class CmsContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('cms-content.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cmsContent = new CmsContent();
        return view('cms-content.create', compact('cmsContent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(CmsContent::$rules);

        $cmsContent = CmsContent::create($request->all());

        return redirect()->route('cms-contents.index')
            ->with('success', 'CmsContent created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cmsContent = CmsContent::find($id);

        return view('cms-content.show', compact('cmsContent'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cmsContent = CmsContent::find($id);

        return view('cms-content.edit', compact('cmsContent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  CmsContent $cmsContent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CmsContent $cmsContent)
    {
        request()->validate(CmsContent::$rules);

        $cmsContent->update($request->all());

        return redirect()->route('cms-contents.index')
            ->with('success', 'CmsContent updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id, Request $request)
    {

        if ($request->ajax()) {
            
            $cmsContent = CmsContent::find($id)->delete();

            return response()->json([
                'success' => true,
                'code' => 200,
                'message' => 'CmsContent deleted successfully'
            ], 200);            
        }

        return redirect()->route('cms-contents.index')
            ->with('success', 'CmsContent deleted successfully');
    }
}
