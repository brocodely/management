<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;


/**
 * Class ClientController
 * @package App\Http\Controllers
 */
class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            return DataTables::of(Client::all())->toJson();
        }

        return view('client.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $client = new Client();
        return view('client.create', compact('client'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = Client::$rules;
        $rules['email'] = 'required|email:rfc,dns|unique:clients,email';
        $rules['image'] = 'required|image|file|max:2048';

        request()->validate($rules);

        $source = $request->get('source');

        $validatedData['image'] = $request->file('image')->store('user-images');
        $validatedData['email'] = $request->email;
        $validatedData['name'] = $request->name;
        $validatedData['phone'] = $request->phone;
        $validatedData['source'] = implode(", ", $source);
        $client = Client::create($validatedData);

        return redirect()->route('clients.index')
            ->with('success', 'Client created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Client::find($id);

        return view('client.show', compact('client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::find($id);

        return view('client.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Client $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $rules = Client::$rules;
        if ($request->email != $client->email) {
            $rules['email'] = 'required|email:rfc,dns|unique:clients,email';
        } else {
            $rules['email'] = 'required|email:rfc,dns';
        }

        if ($request->file('image')) {
            $rules['image'] = 'required|image|file|max:2048';
        }

        request()->validate($rules);

        if ($request->file('image')) {
            if ($request->oldImage) {
                Storage::delete($request->oldImage);
            }
            $validatedData['image'] = $request->file('image')->store('user-images');
        }

        $source = $request->get('source');

        $validatedData['email'] = $request->email;
        $validatedData['name'] = $request->name;
        $validatedData['phone'] = $request->phone;
        $validatedData['source'] = implode(", ", $source);

        $client->update($validatedData);

        return redirect()->route('clients.index')
            ->with('success', 'Client updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Request $request, $id)
    {
        if ($request->delete_confirm == "KONFIRMASI") {
            $clients = Client::where('id', $id)->first();
            if(count($clients->product) > 0) {
                foreach ($clients->product as $product) {
                    $product->delete();
                }
            }else{
                Storage::delete($clients->image);
                $clients->delete();
            }
            return redirect()->route('clients.index')
                ->with('success', 'Client deleted successfully');
        }
        return redirect()->route('clients.index')
            ->with('danger', 'Client deleted failed!');
    }
}
