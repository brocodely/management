<?php

namespace App\Http\Controllers;

use PDF;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Excel;
use Illuminate\Support\Facades\DB;
use App\Exports\ReportLedgerExport;
use App\Exports\ReportBalanceExport;
use App\Http\Controllers\Controller;

class ReportLedgerController extends Controller
{
    public function index(){

        return view('report-ledger.index');
    }

    public function generate(Request $request){

        // Neraca diambil dari saldo - pemasukan kasir atau pengeluaran kasir
        // Akun neraca dimulai dari 100.00.00 - 400.00.000

        $req = $request->all();

        $req['periode'] = str_replace(' ', '', $req['periode']);
        $range = explode('-', $req['periode']);
        $type = $req['type'];

        $range[0] = date('Y-m-d', strtotime($range[0]));
        $range[1] = date('Y-m-d', strtotime($range[1]));

        $bulan_tahun1 = date('Y-m', strtotime($range[0]));
        $bulan_tahun2 = date('Y-m', strtotime($range[1]));

        $data = [];
        $data2 = [];

        $query = "

        SELECT
            finance_coas.bagian,
            finance_coas.nomor_perkiraan,
            finance_coas.nama_akun,
            finance_coas.`status`,
            SUM(balance_beginnings.debit + balance_beginnings.kredit) as saldo_awal,
            (
            SELECT
                balance_remainings.saldo_awal 
            FROM
                balance_remainings 
            WHERE
                balance_remainings.nomor_perkiraan = finance_coas.nomor_perkiraan 
                AND balance_remainings.tanggal BETWEEN '$range[0]' 
                AND '$range[1]' 
            ORDER BY
                balance_remainings.tanggal ASC,
                balance_remainings.id ASC 
                LIMIT 1 
            ) AS saldo_awal_sisa,
            (
            SELECT
                balance_remainings.saldo_akhir 
            FROM
                balance_remainings 
            WHERE
                balance_remainings.nomor_perkiraan = finance_coas.nomor_perkiraan 
                AND balance_remainings.tanggal <= '$range[1]' 
            ORDER BY
                balance_remainings.tanggal DESC,
                balance_remainings.id DESC 
                LIMIT 1 
            ) AS saldo_akhir_sisa,
            balance_beginnings.tanggal,
            balance_remainings.tanggal 
        FROM
            finance_coas
            LEFT OUTER JOIN balance_beginnings ON finance_coas.nomor_perkiraan = balance_beginnings.nomor_perkiraan
            LEFT JOIN balance_remainings ON balance_remainings.nomor_perkiraan = balance_beginnings.nomor_perkiraan 
        WHERE
            finance_coas.`status` = 1 
        GROUP BY
            nomor_perkiraan 
        ORDER BY
            finance_coas.nomor_perkiraan ASC,
            balance_remainings.tanggal ASC,
            balance_remainings.id ASC
            
        ";

        $raw1 = DB::select($query);
        $raw1 = collect($raw1)->sortBy('nomor_perkiraan');

        foreach ($raw1 as $rw){
            $data[$rw->bagian][$rw->nomor_perkiraan][] = $rw;
            $data2[$rw->bagian][$rw->nomor_perkiraan][] = $rw;
        }

        // disini
        foreach ($raw1 as $key => $rw){

            $jurnal1 = "
            
            SELECT
                finance_coas.nama_akun,
                finance_coas.bagian,
                finance_transactions.nomor_voucher,
                finance_transactions.id as main_id,
                finance_transactions.keterangan,
                finance_transaction_details.id AS id,
                finance_transaction_details.nomor_perkiraan,
                finance_transaction_details.nama_akun,
                finance_transaction_details.debit,
                finance_transaction_details.kredit,
                finance_transaction_details.tanggal AS tanggal 
            FROM
                finance_coas
                LEFT JOIN finance_transaction_details ON finance_coas.nomor_perkiraan = finance_transaction_details.nomor_perkiraan
                LEFT JOIN finance_transactions ON finance_transactions.id = finance_transaction_details.finance_transaction_id 
            WHERE
                finance_transactions.tanggal BETWEEN '$range[0]' AND '$range[1]'
            ORDER BY
                tanggal ASC,
                nomor_voucher ASC

            ";
            
            $fetch1 = DB::select($jurnal1);

            $rw->jurnal = $fetch1;
            $rw->jurnal = collect($rw->jurnal)->sortBy('tanggal');

        }


        unset($data['PENDAPATAN']);
        unset($data['BIAYA']);
        unset($data2['ASET']);
        
        $set = false;

        foreach($data2 as $key => $value){
            if ($key != 'ASET' && $key != 'LIABILITAS'){
                $set = true;
            }
        }

        if($type == 'pdf'){
            $pdf = PDF::loadview('report-ledger.pdf', [
                'periode1' => $range[0],
                'periode2' => $range[1],
                'data' => $data,
                'data2' => $data2,
                'type' => $type,
                'set' => $set,
                ])->setpaper('A4', 'landscape');
            return $pdf->stream('Laporan Buku Besar - '. date('Y-m-d') .'.pdf');
        }elseif($type == 'web'){

            return view('report-ledger.pdf', [
                'periode1' => $range[0],
                'periode2' => $range[1],
                'data' => $data,
                'data2' => $data2,
                'type' => $type,
                'set' => $set,
                ]);
        }

        
    }

    public function exportExcel(Excel $excel, Request $request) 
    {
        $req = $request->all();
        $export = new ReportLedgerExport;

        $periode = $req['periode'];

        $export->periode = $periode;
        
        return $excel->download($export, 'Laporan Buku Besar ' . date('Y-m-d', time()) . '.xlsx', Excel::XLSX);
    }
}
