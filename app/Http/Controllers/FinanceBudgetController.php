<?php

namespace App\Http\Controllers;

use App\Models\FinanceBudget;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class FinanceBudgetController
 * @package App\Http\Controllers
 */
class FinanceBudgetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            return DataTables::of(FinanceBudget::all())->toJson();
        }

        return view('finance-budget.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $financeBudget = new FinanceBudget();
        return view('finance-budget.create', compact('financeBudget'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(FinanceBudget::$rules);

        $financeBudget = FinanceBudget::create($request->all());

        return redirect()->route('finance-budgets.index')
            ->with('success', 'FinanceBudget created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $financeBudget = FinanceBudget::find($id);

        return view('finance-budget.show', compact('financeBudget'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $financeBudget = FinanceBudget::find($id);

        return view('finance-budget.edit', compact('financeBudget'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  FinanceBudget $financeBudget
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FinanceBudget $financeBudget)
    {
        request()->validate(FinanceBudget::$rules);

        $financeBudget->update($request->all());

        return redirect()->route('finance-budgets.index')
            ->with('success', 'FinanceBudget updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id, Request $request)
    {
        
        if ($request->ajax()) {
            
            $financeBudget = FinanceBudget::find($id)->delete();
            
            return response()->json([
                'success' => true,
                'code' => 200,
                'message' => 'FinanceBudget deleted successfully'
            ], 200);            
        }

        return redirect()->route('finance-budgets.index')
            ->with('success', 'FinanceBudget deleted successfully');
    }
}
