<?php

namespace App\Http\Controllers;

use PDF;
use App\Models\FinanceCoa;
use Illuminate\Http\Request;
use App\Models\FinanceBudget;
use App\Models\BalanceBeginning;
use App\Models\BalanceRemaining;
use App\Models\FinanceTransaction;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\FinanceTransactionDetail;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class FinanceTransactionController
 * @package App\Http\Controllers
 */
class FinanceTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            return DataTables::of(FinanceTransaction::all())->toJson();
        }

        return view('finance-transaction.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $financeTransaction = new FinanceTransaction();
        return view('finance-transaction.create', compact('financeTransaction'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        $req = $request->all();
        $tahun = date('Y', strtotime($req['tanggal']));
        $backdate = false;
        if(strtotime($req['tanggal']) < strtotime(date('Y-m-d'))){
            $backdate = true;
        }

        if(
            !is_array($req['nomor_perkiraan'])
            || empty($req['tanggal'])
            || empty($req['jenis_transaksi'])
            || empty($req['tipe_transaksi'])
            || empty($req['keterangan'])
        ){
            return redirect()->route('finance-transactions.create')
            ->with('warning', 'The forms can not be EMPTY!');
        }

        // cek saldo awal
        $financeBudgetCount = FinanceBudget::where('tahun', $tahun)
        ->count();

        if($financeBudgetCount < 1){
            // harus input master anggarn tiap tahun
            return redirect()->route('finance-transactions.create')->with('warning', 'Anda belum input master anggaran tahun ini!');
        }

        foreach ($req['nomor_perkiraan'] as $key => $value) {

            $explode = explode(' - ', $value);
            $req['nomor_perkiraan'][$key] = $explode[0];
            $req['nama_akun'][$key] = $explode[1];

            // cek saldo anggaran

            // cek saldo anggaran
            $financeBudgetCount = FinanceBudget::where('tahun', $tahun)
            ->where('nomor_perkiraan', $explode[0])
            ->count();

            if($financeBudgetCount < 1){
                // harus input master anggarn tiap tahun
                return redirect()->route('finance-transactions.create')->with('warning', 'Anda belum input master anggaran pada akun '. $value .'!');
            }

            // cek saldo awal
            $balanceBeginningCount = BalanceBeginning::where('nomor_perkiraan', $explode[0])
            ->count();

            if($balanceBeginningCount < 1){
                // harus input master anggarn tiap tahun
                return redirect()->route('finance-transactions.create')->with('warning', 'Anda belum input saldo awal pada akun '. $value .'!');
            }
            
            // change format
            $req['debit'][$key] = preg_replace("/[^0-9.]/", "", $req['debit'][$key]);
            $req['kredit'][$key] = preg_replace("/[^0-9.]/", "", $req['kredit'][$key]);
        }
        
        $req['total_debit'] = preg_replace("/[^0-9.]/", "", $req['total_debit']);
        $req['total_kredit'] = preg_replace("/[^0-9.]/", "", $req['total_kredit']);

        $req['id_akun'] = 0;
        $req['tipe_akun'] = 'Umum';

        $data = FinanceTransaction::
        orderBy('id', 'desc')
        ->first();

        if(!empty($data)){
            
            $explode = explode('/', $data->nomor_voucher);
            $id = $explode[3] + 1;
            $id = sprintf('%04d',$id);

            $tahunVoucher = date('Y', strtotime($req['tanggal']));
            $dataTahun = FinanceTransaction::whereYear('tanggal', $tahunVoucher)
            ->orderBy('id', 'desc')
            ->first();

            if(!empty($dataTahun)){
                $explode = explode('/', $dataTahun->nomor_voucher);
            
                $id = $explode[3] + 1;
                $id = sprintf('%04d',$id);
            }else{
                $id = '0001';
            }
            
        }else{
            $id = '0001';
        }

        $id = sprintf('%04d',$id);

        if($req['jenis_transaksi'] == 'Pemasukan'){
            $jenisTransaksi = 'IN';
        }else{
            $jenisTransaksi = 'OUT';
        }

        if($req['tipe_transaksi'] == 'Kas'){
            $tipeTransaksi = 'CASH';
        }elseif($req['tipe_transaksi'] == 'Bank'){
            $tipeTransaksi = 'BANK';
        }else{
            $tipeTransaksi = 'EMONEY';
        }

        $voucher = $tipeTransaksi. "-". $jenisTransaksi. "/"."BCDY/".date('Y', strtotime($req['tanggal']))."/".$id;

        $req['nomor_voucher'] = $voucher;
        $req['pembuat'] = Auth::user()->name;
        
        $financeTransaction = FinanceTransaction::create($req);

        // grouping array
        // Cari akun yang sama
        // jika akun lawan sama, maka nilainya dijumlah 
        // data di merge jadi satu
        $acc = [];
            
        foreach ($req['nomor_perkiraan'] as $key => $nomor_perkiraan) {
            @$acc[$nomor_perkiraan]['nama_akun'][$req['remark'][$key]] = $req['nama_akun'][$key]; 
            @$acc[$nomor_perkiraan]['debit'][$req['remark'][$key]] += preg_replace("/[^0-9.]/", "", $req['debit'][$key]); 
            @$acc[$nomor_perkiraan]['kredit'][$req['remark'][$key]] += preg_replace("/[^0-9.]/", "", $req['kredit'][$key]); 
            @$acc[$nomor_perkiraan]['remark'][$req['remark'][$key]] = $req['remark'][$key]; 
        }

        // $key = nama_akun
        // jika remark sama disatuin, jika tidak ya tidak!
        foreach ($acc as $key => $value){
            foreach ($value['nama_akun'] as $k => $nm){
                $i = 0;
                FinanceTransactionDetail::create([
                    'finance_transaction_id' => $financeTransaction->id,
                    'nomor_perkiraan' => $key,
                    'nama_akun' => $acc[$key]['nama_akun'][$k],
                    'remark' => $acc[$key]['remark'][$k] ?? '-',
                    'debit' => $acc[$key]['debit'][$k] ?? '0',
                    'kredit' => $acc[$key]['kredit'][$k] ?? '0',
                    'jenis_transaksi' => $jenisTransaksi ?? '',
                    'tanggal' => $req['tanggal'] ?? ''
                ]);
                $i++;
            }
        }

        //===================================================================//

        if($backdate){
            
            // jika backdate maka akan generate ulang
            foreach ($req['nomor_perkiraan'] as $key => $noPerk){

                $coa = FinanceCoa::where('nomor_perkiraan', $noPerk)->first();
                $balanceRemaining = BalanceRemaining::where('nomor_perkiraan', $noPerk);
                
                // saldo anggaran disamakan
                $query = "

                    UPDATE finance_budgets 
                    SET saldo_sisa = saldo
                    WHERE nomor_perkiraan = '$noPerk'
                    AND tahun = '$tahun'

                ";

                DB::select($query);

                $balanceBeginning = BalanceBeginning::where('nomor_perkiraan', $noPerk);

                $balance = $balanceBeginning->first();

                $transaksi = BalanceRemaining::whereYear('tanggal', $tahun)
                ->where('nomor_perkiraan', '=', $noPerk)
                ->count();

                $saldo = $balance['nominal'];

                // hapus saldo sisa yang sudah masuk
                $balanceRemaining->delete();

                $kasir = FinanceTransactionDetail::where('nomor_perkiraan', $noPerk)
                ->orderBy('tanggal')
                ->get()
                ->toArray();                                   

                $dataAnggaran = FinanceTransactionDetail::where('nomor_perkiraan', $noPerk)
                ->where('jenis_transaksi', 'OUT')
                ->orderBy('tanggal')
                ->get()
                ->toArray();

                if(!empty($dataAnggaran)){
                    
                    foreach ($dataAnggaran as $key => $value){
    
                        $noPerk = $value['nomor_perkiraan'];
                        $tahunTransaksi =  date('Y', strtotime($value['tanggal']));
            
                        // budget 
            
                            // update saldo sisa di master anggaran
                            // update berdasarkan nominal yang ada masuk di kasir
                            // (prepare)
                            $nominalKasir = $value['debit'] + $value['kredit'];
            
                            // akun biasa
                            $anggaran = FinanceBudget::where('nomor_perkiraan', $noPerk)
                            ->where('tahun', $tahunTransaksi)
                            ->first();
            
                            $anggaranSisa = $anggaran['saldo_sisa'] - $nominalKasir; 
                        
                            // (update)
                            
                            // akun biasa
                            FinanceBudget::where('nomor_perkiraan', $noPerk)
                            ->where('tahun', $tahunTransaksi)
                            ->update([
                                'saldo_sisa' => $anggaranSisa
                            ]);
            
            
                        // end budget
                        
            
                    }
                    
                }

                if(!empty($kasir)){

                    foreach ($kasir as $key => $ocd){

                        $tahunTransaksi =  date('Y', strtotime($ocd['tanggal']));
    
                        if(! empty($balanceRemaining->first()) ){
    
                            $balance = $balanceRemaining->orderBy('id', 'desc')->first();
                            $saldo = $balance['saldo_akhir'];
    
                        }
    
                        // kondisi
                        if($coa['bagian'] == 'ASET'){
                            
                            $saldoAkhir = $saldo + $ocd['debit'] - $ocd['kredit'];
                            
                        }elseif($coa['bagian'] == 'LIABILITAS'){
                            
                            $saldoAkhir = $saldo - $ocd['debit'] + $ocd['kredit'];
                            
                        }elseif($coa['bagian'] == 'BIAYA' || $coa['bagian'] == 'BEBAN'){
                            
                            $saldoAkhir = $saldo - $ocd['debit'] + $ocd['kredit'];
    
                        }elseif($coa['bagian'] == 'PENDAPATAN' || $coa['bagian'] == 'PENDAPATAN LAINNYA'){
    
                            $saldoAkhir = $saldo - $ocd['debit'] + $ocd['kredit'];
    
                        }
                            
                        BalanceRemaining::create(
                            [
                                'nomor_perkiraan' => $noPerk,
                                'nama_akun' => $ocd['nama_akun'],
                                'saldo_awal' => $saldo,
                                'saldo_akhir' => $saldoAkhir,
                                'tanggal' => $ocd['tanggal'],
                                'type' => 'Umum'
                            ]
                        );
    
                    }
    
                }

                $bb = BalanceBeginning::where('nomor_perkiraan', '=', $noPerk)
                ->where(function($q){
                    $q->where('status', 0);
                    $q->orWhere('status', NULL);
                })
                ->update(
                    [
                        'status' => 1
                    ]
                );

            }


        }else{

            foreach ($req['nomor_perkiraan'] as $k => $noPerk){

                // removing null
                if(empty($req['debit'][$k])){
                    $req['debit'][$k] = 0;
                }
                if(empty($req['kredit'][$k])){
                    $req['kredit'][$k] = 0;
                }

                $req['debit'][$k] = preg_replace("/[^0-9.]/", "", $req['debit'][$k]);
                $req['kredit'][$k] = preg_replace("/[^0-9.]/", "", $req['kredit'][$k]);
                $req['total_debit'] = preg_replace("/[^0-9.]/", "", $req['total_debit']);
                $req['total_kredit'] = preg_replace("/[^0-9.]/", "", $req['total_kredit']);

                // multiuse
                $balanceRemaining = BalanceRemaining::where('nomor_perkiraan', $noPerk);
                $tahunTransaksi =  date('Y', strtotime($req['tanggal']));

                // jika akun 0 = akun backdate biasa
                // jika akun !0 = akun gantung
                // akun biasa pake tanggal backdate
                // akun gantung pake tanggal sekarang
                // cek sisa saldo atau saldo awal
                    
                $sisaSaldo = BalanceRemaining::where('nomor_perkiraan', $noPerk)
                ->where('tanggal', $req['tanggal'])
                ->first();

                $saldoAkhir = 0;

                // update saldo sisa di master anggaran
                // update berdasarkan nominal yang ada masuk di kasir
                // (prepare)
                $nominalKasir = $req['debit'][$k] + $req['kredit'][$k];
                
                // akun biasa
                $anggaran = FinanceBudget::where('nomor_perkiraan', $noPerk)
                ->where('tahun', date('Y', strtotime($req['tanggal'])))
                ->first();

                // ambil saldo awal yang awal banget
                $anggaranSisa = $anggaran['saldo_sisa'] + $nominalKasir; 

                // kurangi budget
                
                if($jenisTransaksi == 'OUT'){
                    
                    // budget 
        
                        // update saldo sisa di master anggaran
                        // update berdasarkan nominal yang ada masuk di kasir
                        // (prepare)
                        $nominalKasir = $req['debit'][$k] + $req['kredit'][$k];
        
                        // akun biasa
                        $anggaran = FinanceBudget::where('nomor_perkiraan', $noPerk)
                        ->where('tahun', $tahunTransaksi)
                        ->first();
        
                        $anggaranSisa = $anggaran['saldo_sisa'] - $nominalKasir; 
                    
                        // (update)
                        
                        // akun biasa
                        FinanceBudget::where('nomor_perkiraan', $noPerk)
                        ->where('tahun', $tahunTransaksi)
                        ->update([
                            'saldo_sisa' => $anggaranSisa
                        ]);
        
        
                    // end budget

                }
                
                // jika tidak ada sisa saldo maka akan membuat
                if(empty($sisaSaldo)){
                    
                    // jika tidak ada sisa saldo bisa jadi diambil dari saldo awal
                    // 0 belum terpakai, 1 sudah terpakai
                    $saldo = BalanceBeginning::where('nomor_perkiraan', '=', $noPerk)
                    ->where(function($q){
                        // status dipakai untuk mengetahui apakah saldo awal sudah terpakai atau belum
                        $q->where('status', 0);
                        $q->orWhere('status', NULL);
                    })
                    ->first();
                    
                    // jika sudah terpakai
                    // berganti bulan

                    if(empty($saldo)){
                        

                        $terbaruDetail = BalanceRemaining::where('nomor_perkiraan', $noPerk)
                        ->orderBy('id', 'desc')->first();

                        $saldoDetail = $terbaruDetail['saldo_akhir'];

                        $coa = FinanceCoa::where('nomor_perkiraan', $noPerk)->first();

                        if(! empty($balanceRemaining->first()) ){

                            $saldoDetail = $terbaruDetail['saldo_akhir'];
    
                        }

                        // kondisi
                        
                        if($coa['bagian'] == 'ASET'){

                            $saldoAkhirDetail = $saldoDetail + $req['debit'][$k] - $req['kredit'][$k];

                        }elseif($coa['bagian'] == 'LIABILITAS'){

                            $saldoAkhirDetail = $saldoDetail - $req['debit'][$k] + $req['kredit'][$k];

                        }elseif($coa['bagian'] == 'BIAYA' || $coa['bagian'] == 'BEBAN'){

                            $saldoAkhirDetail = $saldoDetail - $req['debit'][$k] + $req['kredit'][$k];

                        }elseif($coa['bagian'] == 'PENDAPATAN' || $coa['bagian'] == 'PENDAPATAN LAINNYA'){

                            $saldoAkhirDetail = $saldoDetail - $req['debit'][$k] + $req['kredit'][$k];
    
                        }
                        

                        BalanceRemaining::create(
                            [
                                'nomor_perkiraan' => $noPerk,
                                'nama_akun' => $req['nama_akun'][$k],
                                'saldo_awal' => $saldoDetail,
                                'saldo_akhir' => $saldoAkhirDetail,
                                'tanggal' => date('Y-m-d', strtotime($req['tanggal'])),
                                'type' => 'Umum'
                            ]
                        );
                    }else{
                        // jika belum terpakai, di remaining balance kosong
                        
                        $saldo = $saldo['debit'] + $saldo['kredit'];
                        $coa = FinanceCoa::where('nomor_perkiraan', $noPerk)->first();

                        // kondisi

                        if($coa['bagian'] == 'ASET'){

                            $saldoAkhir = $saldo + $req['debit'][$k] - $req['kredit'][$k];

                        }elseif($coa['bagian'] == 'LIABILITAS'){

                            $saldoAkhir = $saldo - $req['debit'][$k] + $req['kredit'][$k];

                        }elseif($coa['bagian'] == 'BIAYA' || $coa['bagian'] == 'BEBAN'){

                            $saldoAkhir = $saldo - $req['debit'][$k] + $req['kredit'][$k];

                        }elseif($coa['bagian'] == 'PENDAPATAN' || $coa['bagian'] == 'PENDAPATAN LAINNYA'){

                            $saldoAkhir = $saldo - $req['debit'][$k] + $req['kredit'][$k];

                        }

                        BalanceRemaining::create(
                            [
                                'nomor_perkiraan' => $noPerk,
                                'nama_akun' => $req['nama_akun'][$k],
                                'saldo_awal' => $saldo,
                                'saldo_akhir' => $saldoAkhir,
                                'tanggal' => date('Y-m-d', strtotime($req['tanggal'])),
                                'type' => 'Umum'
                            ]
                        );

                        $bb = BalanceBeginning::where('nomor_perkiraan', '=', $noPerk)
                        ->where(function($q){
                            $q->where('status', 0);
                            $q->orWhere('status', NULL);
                        })
                        ->update(
                            [
                                'status' => 1
                            ]
                        );

                    }

                }else{

                    // sisa saldo bulan ini sudah ada, maka update
                    $saldo = $sisaSaldo['saldo_akhir'];

                    $coa = FinanceCoa::where('nomor_perkiraan', $noPerk)->first();

                    // kondisi
                        if($coa['bagian'] == 'ASET'){

                            $saldoAkhir = $saldo + $req['debit'][$k] - $req['kredit'][$k];

                        }elseif($coa['bagian'] == 'LIABILITAS'){

                            $saldoAkhir = $saldo - $req['debit'][$k] + $req['kredit'][$k];

                        }elseif($coa['bagian'] == 'BIAYA' || $coa['bagian'] == 'BEBAN'){

                            $saldoAkhir = $saldo - $req['debit'][$k] + $req['kredit'][$k];

                        }elseif($coa['bagian'] == 'PENDAPATAN' || $coa['bagian'] == 'PENDAPATAN LAINNYA'){

                            $saldoAkhir = $saldo - $req['debit'][$k] + $req['kredit'][$k];

                        }

                        BalanceRemaining::create(
                            [
                                'nomor_perkiraan' => $noPerk,
                                'nama_akun' => $req['nama_akun'][$k],
                                'saldo_awal' => $saldo,
                                'saldo_akhir' => $saldoAkhir,
                                'tanggal' => date('Y-m-d', strtotime($req['tanggal'])),
                                'type' => 'Umum'
                            ]
                        );

                }
            }

        }


        return redirect()->route('finance-transactions.index')
            ->with('success', 'FinanceTransaction created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $financeTransaction = FinanceTransaction::find($id);

        return view('finance-transaction.show', compact('financeTransaction'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $financeTransaction = FinanceTransaction::find($id);

        return view('finance-transaction.edit', compact('financeTransaction'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  FinanceTransaction $financeTransaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FinanceTransaction $financeTransaction)
    {
        request()->validate(FinanceTransaction::$rules);

        $financeTransaction->update($request->all());

        return redirect()->route('finance-transactions.index')
            ->with('success', 'FinanceTransaction updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id, Request $request)
    {

        if ($request->ajax()) {

            $financeTransaction = FinanceTransaction::find($id)->delete();
            FinanceTransactionDetail::where('finance_transaction_id', $id)->delete();
            
            return response()->json([
                'success' => true,
                'code' => 200,
                'message' => 'FinanceCoa deleted successfully'
            ], 200);            
        }

        return redirect()->route('finance-transactions.index')
            ->with('success', 'FinanceTransaction deleted successfully');
    }

    public function print($id){
        $detail = FinanceTransactionDetail::where('finance_transaction_id', $id)
        ->orderBy('debit', 'desc')
        ->orderBy('kredit', 'desc')
        ->get();

        $pdf = PDF::loadview('finance-transaction.print', [
            'data' => FinanceTransaction::firstWhere('id', $id),
            'detail' => $detail,
            ])->setpaper('A4', 'portrait');
        return $pdf->stream('Jurnal Transaksi - ' .date('Y-m-d'). '.pdf');
    }
}
