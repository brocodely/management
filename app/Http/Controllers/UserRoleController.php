<?php

namespace App\Http\Controllers;

use App\Models\UserMenu;
use App\Models\UserRole;
use Illuminate\Http\Request;
use App\Models\UserAccessMenu;
use Yajra\DataTables\Facades\DataTables;

class UserRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return DataTables::of(UserRole::all())->toJson();
        }
        return view('role.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = UserRole::all();
        return view('role.create', compact('role'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $req = $request->all();

        UserRole::create($req);

        return redirect(route('roles.index'))->with('status', 'Role baru berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserRole  $role
     * @return \Illuminate\Http\Response
     */
    public function show(UserRole $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserRole  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(UserRole $role)
    {
        return view('role.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserRole  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserRole $role)
    {
        $req = $request->all();
        $role->update($req);

        return redirect(route('roles.index'))->with('status', 'UserRole berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserRole  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserRole $role, Request $request)
    {

        if ($request->ajax()) {

            UserRole::destroy($role->id);
            
            return response()->json([
                'success' => true,
                'code' => 200,
                'message' => 'Role deleted successfully'
            ], 200);            
        }

        

        return redirect(route('roles.index'))->with('status', 'UserRole berhasil dihapus!');
    }
    
    public function access(UserRole $role)
    {
        $userRoleAccess = UserAccessMenu::where('role_id', $role->id)
        ->with('menu')
        ->get();
        $menu = UserMenu::with('access')
        ->get();
        return view('role.access', compact('role', 'userRoleAccess', 'menu'));
    }
    
}
