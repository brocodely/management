<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;


/**
 * Class ProductController
 * @package App\Http\Controllers
 */
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            return DataTables::of(Product::all())->toJson();
        }

        return view('product.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product = new Product();
        $clients = Client::all();
        return view('product.create', compact('product', 'clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = Product::$rules;
        $rules['image'] = 'required|image|file|max:2048';

        request()->validate($rules);

        $validatedData['client_id'] = $request->client_id;
        $validatedData['image'] = $request->file('image')->store('user-images');
        $validatedData['name'] = $request->name;
        $validatedData['description'] = $request->description;
        $validatedData['order_date'] = $request->order_date;
        $validatedData['order_finish_date'] = $request->order_finish_date;
        $validatedData['status'] = $request->status;

        $product = Product::create($validatedData);

        return redirect()->route('products.index')
            ->with('success', 'Product created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);

        return view('product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);

        $clients = Client::where('id', '!=', $product->client_id)->get();

        return view('product.edit', compact('product', 'clients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $rules = Product::$rules;

        if ($request->file('image')) {
            $rules['image'] = 'required|image|file|max:2048';
        }

        request()->validate($rules);

        if ($request->file('image')) {
            if ($request->oldImage) {
                Storage::delete($request->oldImage);
            }
            $validatedData['image'] = $request->file('image')->store('user-images');
        }

        $validatedData['client_id'] = $request->client_id;
        $validatedData['name'] = $request->name;
        $validatedData['description'] = $request->description;
        $validatedData['order_date'] = $request->order_date;
        $validatedData['order_finish_date'] = $request->order_finish_date;
        $validatedData['status'] = $request->status;

        $product->update($validatedData);

        return redirect()->route('products.index')
            ->with('success', 'Product updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        Storage::delete($product->image);
        $product->delete();

        return redirect()->route('products.index')
            ->with('success', 'Product deleted successfully');
    }
}
