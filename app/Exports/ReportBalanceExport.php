<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ReportBalanceExport implements FromView, ShouldAutoSize
{
    use Exportable;

    public $periode = 0;

    public function view(): View
    {
        $periode = $this->periode;
        $type = 'excel';

        $range = explode(' - ', $periode);

        $range[0] = date('Y-m-d', strtotime($range[0]));
        $range[1] = date('Y-m-d', strtotime($range[1]));

        $bulan_tahun1 = date('Y-m', strtotime($range[0]));
        $bulan_tahun2 = date('Y-m', strtotime($range[1]));

        $data = [];
        $data2 = [];

        $query = "

        SELECT
            finance_coas.bagian,
            finance_coas.header,
            finance_coas.nomor_perkiraan,
            finance_coas.nama_akun,
            finance_coas.`status`,
            SUM(balance_beginnings.debit + balance_beginnings.kredit) AS saldo_akhir_awal,
            (
            SELECT
                balance_remainings.saldo_akhir 
            FROM
                balance_remainings 
            WHERE
                balance_remainings.nomor_perkiraan = finance_coas.nomor_perkiraan 
                AND balance_remainings.tanggal <= '$range[1]' 
            ORDER BY
                balance_remainings.tanggal DESC,
                balance_remainings.id DESC 
                LIMIT 1 
            ) AS saldo_akhir_sisa,
            balance_beginnings.tanggal,
            balance_remainings.tanggal 
        FROM
            finance_coas
            LEFT OUTER JOIN balance_beginnings ON finance_coas.nomor_perkiraan = balance_beginnings.nomor_perkiraan
            LEFT JOIN balance_remainings ON balance_remainings.nomor_perkiraan = balance_beginnings.nomor_perkiraan 
        WHERE
            finance_coas.`status` = 1 
        GROUP BY
            nomor_perkiraan 
        ORDER BY
            finance_coas.nomor_perkiraan ASC,
            balance_remainings.tanggal ASC,
            balance_remainings.id ASC
        
        ";

        $raw1 = DB::select($query);
        $raw1 = collect($raw1)->sortBy('nomor_perkiraan');

        foreach ($raw1 as $rw){
            $data[$rw->bagian][$rw->header][$rw->nama_akun][] = $rw;
            $data2[$rw->bagian][$rw->header][$rw->nama_akun][] = $rw;
        }

        unset($data['BIAYA']);
        unset($data['PENDAPATAN']);
        unset($data2['ASET']);

        $compact = [
            'periode1' => strtotime($range[0]),
            'periode2' => strtotime($range[1]),
            'data' => $data,
            'data2' => $data2,
            'range' => $range,
            'type' => $type,
        ];

        return view('report-balance.web-view', $compact);
    }
}
