<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ReportLedgerExport implements FromView, ShouldAutoSize
{
    use Exportable;

    public $periode = 0;
    public $noPerk = '';
    public $print = 0;

    public function view(): View
    {
        $periode = $this->periode;
        $noPerk = $this->noPerk;
        $print = $this->print;

        $type = 'excel';
        $id = $noPerk;

        $range = explode(' - ', $periode);

        $range[0] = date('Y-m-d', strtotime($range[0]));
        $range[1] = date('Y-m-d', strtotime($range[1]));

        $bulan_tahun1 = date('Y-m', strtotime($range[0]));
        $bulan_tahun2 = date('Y-m', strtotime($range[1]));

        $data = [];
        $data2 = [];

        $query = "

        SELECT
            finance_coas.bagian,
            finance_coas.nomor_perkiraan,
            finance_coas.nama_akun,
            finance_coas.`status`,
            SUM(balance_beginnings.debit + balance_beginnings.kredit) as saldo_awal,
            (
            SELECT
                balance_remainings.saldo_awal 
            FROM
                balance_remainings 
            WHERE
                balance_remainings.nomor_perkiraan = finance_coas.nomor_perkiraan 
                AND balance_remainings.tanggal BETWEEN '$range[0]' 
                AND '$range[1]' 
            ORDER BY
                balance_remainings.tanggal ASC,
                balance_remainings.id ASC 
                LIMIT 1 
            ) AS saldo_awal_sisa,
            (
            SELECT
                balance_remainings.saldo_akhir 
            FROM
                balance_remainings 
            WHERE
                balance_remainings.nomor_perkiraan = finance_coas.nomor_perkiraan 
                AND balance_remainings.tanggal <= '$range[1]' 
            ORDER BY
                balance_remainings.tanggal DESC,
                balance_remainings.id DESC 
                LIMIT 1 
            ) AS saldo_akhir_sisa,
            balance_beginnings.tanggal,
            balance_remainings.tanggal 
        FROM
            finance_coas
            LEFT OUTER JOIN balance_beginnings ON finance_coas.nomor_perkiraan = balance_beginnings.nomor_perkiraan
            LEFT JOIN balance_remainings ON balance_remainings.nomor_perkiraan = balance_beginnings.nomor_perkiraan 
        WHERE
            finance_coas.`status` = 1 
        GROUP BY
            nomor_perkiraan 
        ORDER BY
            finance_coas.nomor_perkiraan ASC,
            balance_remainings.tanggal ASC,
            balance_remainings.id ASC
            
        ";

        $raw1 = DB::select($query);
        $raw1 = collect($raw1)->sortBy('nomor_perkiraan');

        foreach ($raw1 as $rw){
            $data[$rw->bagian][$rw->nomor_perkiraan][] = $rw;
            $data2[$rw->bagian][$rw->nomor_perkiraan][] = $rw;
        }

        // disini
        foreach ($raw1 as $key => $rw){

            $jurnal1 = "
            
            SELECT
                finance_coas.nama_akun,
                finance_coas.bagian,
                finance_transactions.nomor_voucher,
                finance_transactions.id as main_id,
                finance_transactions.keterangan,
                finance_transaction_details.id AS id,
                finance_transaction_details.nomor_perkiraan,
                finance_transaction_details.nama_akun,
                finance_transaction_details.debit,
                finance_transaction_details.kredit,
                finance_transaction_details.tanggal AS tanggal 
            FROM
                finance_coas
                LEFT JOIN finance_transaction_details ON finance_coas.nomor_perkiraan = finance_transaction_details.nomor_perkiraan
                LEFT JOIN finance_transactions ON finance_transactions.id = finance_transaction_details.finance_transaction_id 
            WHERE
                finance_transactions.tanggal BETWEEN '$range[0]' AND '$range[1]'
            ORDER BY
                tanggal ASC,
                nomor_voucher ASC

            ";
            
            $fetch1 = DB::select($jurnal1);

            $rw->jurnal = $fetch1;
            $rw->jurnal = collect($rw->jurnal)->sortBy('tanggal');

        }


        unset($data['PENDAPATAN']);
        unset($data['BIAYA']);
        unset($data2['ASET']);
        
        $set = false;

        foreach($data2 as $key => $value){
            if ($key != 'ASET' && $key != 'LIABILITAS'){
                $set = true;
            }
        }

        if($type == 'excel'){

            return view('report-ledger.web-view', [
                'periode1' => $range[0],
                'periode2' => $range[1],
                'data' => $data,
                'data2' => $data2,
                'type' => $type,
                'set' => $set,
            ]);
        }



    }
}
