<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/accessProcess', [ApiController::class, 'accessProcess'])->name('api.accessProcess');
Route::get('/changeProccess', [ApiController::class, 'changeProccess'])->name('api.changeProccess');
Route::get('/getCoa', [ApiController::class, 'getCoa'])->name('api.getCoa');
Route::get('/getEmployee', [ApiController::class, 'getEmployee'])->name('api.getEmployee');
Route::get('/getEmployeeValues', [ApiController::class, 'getEmployeeValues'])->name('api.getEmployeeValues');
Route::get('/getSalaryValues', [ApiController::class, 'getSalaryValues'])->name('api.getSalaryValues');
Route::get('/getInsentif', [ApiController::class, 'getInsentif'])->name('api.getInsentif');
Route::get('/akunGantung', [ApiController::class, 'akunGantung'])->name('api.akunGantung');
Route::get('/akunGantungValues', [ApiController::class, 'akunGantungValues'])->name('api.akunGantungValues');
Route::get('/getCms', [ApiController::class, 'getCms'])->name('api.getCms');
