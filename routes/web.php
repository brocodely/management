<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SubmenuController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\UserMenuController;
use App\Http\Controllers\UserRoleController;
use App\Http\Controllers\CmsContentController;
use App\Http\Controllers\FinanceCoaController;
use App\Http\Controllers\UserProfileController;
use App\Http\Controllers\UserSubMenuController;
use App\Http\Controllers\ReportLedgerController;
use App\Http\Controllers\FinanceBudgetController;
use App\Http\Controllers\FinanceSalaryController;
use App\Http\Controllers\ReportBalanceController;
use App\Http\Controllers\BalanceBeginningController;
use App\Http\Controllers\FinanceTransactionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    
    // user
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::post('profiles/reset/{id}', [UserProfileController::class, 'resetProfile']);
    Route::get('profiles/change-password', [UserProfileController::class, 'changePassword'])->name('change-password.index');
    Route::patch('profiles/change-password/{id}', [UserProfileController::class, 'changeProccess'])->name('profile.change-password');
    Route::resource('profiles', UserProfileController::class);
    
    // admin
    Route::group(['middleware' => ['is_admin']], function () {

        // buatan
        Route::get('/roles/{role}/access', [UserRoleController::class, 'access']);
        
        Route::get('/report-balances', [ReportBalanceController::class, 'index']);
        Route::get('/report-balances/generate', [ReportBalanceController::class, 'generate']);
        Route::get('/report-balances/export', [ReportBalanceController::class, 'exportExcel'])->name('report-balances.exportExcel');
        
        Route::get('/report-ledgers', [ReportLedgerController::class, 'index']);
        Route::get('/report-ledgers/generate', [ReportLedgerController::class, 'generate']);
        Route::get('/report-ledgers/export', [ReportLedgerController::class, 'exportExcel'])->name('report-ledgers.exportExcel');
        
        Route::get('/finance-transactions/{id}/print', [FinanceTransactionController::class, 'print'])->name('finance-transactions.print');
        
        // // resource
        Route::resource('users', UserController::class);
        Route::resource('menus', UserMenuController::class);
        Route::resource('submenus', UserSubMenuController::class);
        Route::resource('roles', UserRoleController::class);
        Route::resource('employees', EmployeeController::class);
        Route::resource('products', ProductController::class);
        Route::resource('clients', ClientController::class);
        Route::resource('finance-coas', FinanceCoaController::class);
        Route::resource('finance-budgets', FinanceBudgetController::class);
        Route::resource('finance-transactions', FinanceTransactionController::class);
        Route::resource('finance-salaries', FinanceSalaryController::class);
        Route::resource('balance-beginnings', BalanceBeginningController::class);
        Route::resource('cms-contents', CmsContentController::class);
        
    });
    

});