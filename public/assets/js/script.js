$('#sidebar_toggle').on('click', function () {
    $('#kt_wrapper').toggleClass('open');
    $('#kt_aside').toggleClass('close');
    $('#kt_header').toggleClass('full-width');
});

function previewImage() {
    const image = document.querySelector('#image');
    const imgPreview = document.querySelector('#image-profile');
    const textPreview = $('#text-preview');
    const deleteProfileButton = $('.delete-profile');

    const oFileReader = new FileReader();
    oFileReader.readAsDataURL(image.files[0]);

    oFileReader.onload = function (oFREvent) {
        imgPreview.src = oFREvent.target.result;
    }

    textPreview.removeClass('d-none');
    textPreview.addClass('d-block');

    deleteProfileButton.addClass('d-none');
}

$('#expand_toggle_desc').on('click', function () {
    $('#expand_desc').toggleClass('d-none');
    $('#collapse_desc').toggleClass('d-none');
    $('#desc_label').toggleClass('w-200px');
    $('#desc_colon').toggleClass('w-25px');
    $('#collapse_toggle_desc').toggleClass('active');
    $(this).toggleClass('active');
})
$('#collapse_toggle_desc').on('click', function () {
    $('#expand_desc').toggleClass('d-none');
    $('#collapse_desc').toggleClass('d-none');
    $('#desc_label').toggleClass('w-200px');
    $('#desc_colon').toggleClass('w-25px');
    $('#expand_toggle_desc').toggleClass('active');
    $(this).toggleClass('active');
})

$("#date_of_birth, #join_date, #order_date, #order_finish_date").flatpickr();